#include "_controller.h"
#include <gwlog.h>

//const char* TAG = "ctrl";

const uint16_t CONTROLLER_PORT = 1106;

void OnClose(uv_handle_t* handle)
{
    LOGV_CALL(TAG, "start");

    if (handle)
    {
        free(handle);
    }

    LOGV_CALL(TAG, "end");
}

void OnAlloc(uv_handle_t* handle, size_t size, uv_buf_t* buf)
{
    buf->base = (char*)malloc(size);
    buf->len = size;
}