// controller.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "_controller.h"
#include "ControllerEngine.h"
#include <gwlog.h>

static const char* TAG = "controller";

int startWSA()
{
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    /* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0) {
        /* Tell the user that we could not find a usable */
        /* Winsock DLL.                                  */
        LOGE(TAG, "%s(%d) %s: WSAStartup failed with error: %d", __CALL_INFO__, err);
        return 1;
    }

    return 0;
}

int main()
{
    gwlog_init("e:/tmp/controller.txt", eLO_FILE, 3);

    startWSA();

    int ret;
    ControllerEngine engine;
    ControllerEngine::Param param;
    param.port = CONTROLLER_PORT;

    ret = engine.Open(param);
    ret = engine.Start();
    ret = engine.Stop();
    ret = engine.Close();

    WSACleanup();
    gwlog_exit();

    return 0;
}

