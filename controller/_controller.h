#pragma once
#ifndef _CONTROLLER_H
#define _CONTROLLER_H

#include <uv.h>

extern const char* TAG;
extern const uint16_t CONTROLLER_PORT;

void OnClose(uv_handle_t* handle);
void OnAlloc(uv_handle_t* handle, size_t size, uv_buf_t* buf);

#endif