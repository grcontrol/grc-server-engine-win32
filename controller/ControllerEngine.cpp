#include "ControllerEngine.h"
#include "_controller.h"
#include "KeyboardSimulator.h"
#include <gwlog.h>
#include "Executer.h"
#include <Packet.h>
#include <GRCPacket.h>

ControllerEngine::ControllerEngine() :
    m_executers(new ExecuterStorage()),
    m_packetTokenizer(new PacketTokenizer(4096)),
    m_udp(NULL)
{
    LOGD_CALL(TAG, "start");

    m_loop = uv_default_loop();
    m_loop->data = this;

    m_state.SetState(gw::eLCS_INIT);

    LOGD_CALL(TAG, "end");
}

ControllerEngine::~ControllerEngine()
{
    LOGD_CALL(TAG, "start");

    LOGD_CALL(TAG, "end");
}

int ControllerEngine::Open(const Param& param)
{
    LOGD_CALL(TAG, "start");

    m_stdin = (uv_tty_t*)malloc(sizeof(uv_tty_t));
    uv_tty_init(m_loop, m_stdin, 0, 1);
    //uv_tty_set_mode(m_stdin, UV_TTY_MODE_RAW);
    m_stdin->data = this;

    m_stdout = (uv_tty_t*)malloc(sizeof(uv_tty_t));
    uv_tty_init(m_loop, m_stdout, 1, 0);
    m_stdout->data = this;

    OpenUDP(param.port);

    m_prepare = (uv_prepare_t*)malloc(sizeof(uv_prepare_t));
    uv_prepare_init(m_loop, m_prepare);
    m_prepare->data = this;

    m_check = (uv_check_t*)malloc(sizeof(uv_check_t));
    uv_check_init(m_loop, m_check);
    m_check->data = this;

    m_state.SetState(gw::eLCS_OPEN);

    LOGD_CALL(TAG, "end");
    return 0;
}

void ControllerEngine::OpenUDP(uint16_t port)
{
    int ret;

    uv_udp_t* udp = (uv_udp_t*)malloc(sizeof(uv_udp_t));
    uv_udp_init(m_loop, udp);
    udp->data = this;

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(port);

    ret = uv_udp_bind(udp, (const sockaddr*)&addr, UV_UDP_REUSEADDR);
    if (ret < 0)
    {
        LOGE(TAG, "%s(%d) %s: error(%s) %s",
            __CALL_INFO__, uv_err_name(ret), uv_strerror(ret));

        free(udp);
    }
    else
    {
        m_udp = udp;
    }
}

int ControllerEngine::Start()
{
    LOGD_CALL(TAG, "start");

    int ret;
    ret = uv_prepare_start(m_prepare, OnPrepare);
    ret = uv_read_start((uv_stream_t*)m_stdin, ::OnAlloc, OnRead);
    ret = uv_check_start(m_check, OnCheck);
    ret = uv_udp_recv_start(m_udp, ::OnAlloc, OnUDPRecv);
    m_queue.clear();

    m_state.SetState(gw::eLCS_START);
    ret = uv_run(m_loop, UV_RUN_DEFAULT);

    LOGD_CALL(TAG, "end");
    return 0;
}

int ControllerEngine::Stop()
{
    LOGD_CALL(TAG, "start");

    if (m_state.GetState() == gw::eLCS_START)
    {
        uv_check_stop(m_check);
        uv_prepare_stop(m_prepare);
        uv_read_stop((uv_stream_t*)m_stdin);
        uv_udp_recv_stop(m_udp);

        uv_close((uv_handle_t*)m_prepare, OnClose);
        uv_close((uv_handle_t*)m_check, OnClose);
        uv_close((uv_handle_t*)m_stdin, OnClose);
        uv_close((uv_handle_t*)m_stdout, OnClose);
        uv_close((uv_handle_t*)m_udp, OnClose);
        uv_stop(m_loop);

        m_state.SetState(gw::eLCS_OPEN);
    }

    LOGD_CALL(TAG, "end");
    return 0;
}

int ControllerEngine::Close()
{
    LOGD_CALL(TAG, "start");

    if (m_stdin)
    {
        free(m_stdin);
        m_stdin = NULL;
    }
    if (m_stdout)
    {
        free(m_stdout);
        m_stdout = NULL;
    }
    if (m_check)
    {
        free(m_check);
        m_check = NULL;
    }
    if (m_prepare)
    {
        free(m_prepare);
        m_prepare = NULL;
    }
    if (m_udp)
    {
        free(m_udp);
        m_udp = NULL;
    }

    m_state.SetState(gw::eLCS_INIT);

    LOGD_CALL(TAG, "end");
    return 0;
}

void ControllerEngine::OnRead(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf)
{
    LOGD_CALL(TAG, "start");

    ControllerEngine* engine = (ControllerEngine*)stream->data;
    engine->_OnRead(stream, nread, buf);

    LOGD_CALL(TAG, "end");
}

void ControllerEngine::_OnRead(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf)
{
    LOGD_CALL(TAG, "start");

    uint32_t type = ntohl(*(uint32_t*)buf->base);
    uint32_t bodySize = ntohl(*(uint32_t*)(buf->base + 4));
    uint32_t session = ntohl(*(uint32_t*)(buf->base + 8));
    LOGI(TAG, "%s(%d) %s nread(%d) type(%d) bodySize(%d) session(%d)",
        __CALL_INFO__, nread, type, bodySize, session)

    int ret;
    Packet packet;

    m_packetTokenizer->Input(buf->base, (int)nread);

    while ((ret = m_packetTokenizer->GetNext(&packet)) == 0)
    {
        HandlePacket(&packet);
        m_packetTokenizer->Consume(packet.GetPacketSize());
    }

    free(buf->base);
    LOGD_CALL(TAG, "end");
}

void ControllerEngine::OnPrepare(uv_prepare_t* handle)
{
    LOGD_CALL(TAG, "start");
    ControllerEngine* engine = (ControllerEngine*)handle->data;
    engine->OnCommand();
    LOGD_CALL(TAG, "end");
}

void ControllerEngine::OnCheck(uv_check_t* check)
{
    LOGD_CALL(TAG, "start");
    ControllerEngine* engine = (ControllerEngine*)check->data;
    engine->OnCommand();
    LOGD_CALL(TAG, "end");
}

void ControllerEngine::OnCommand()
{
    LOGD_CALL(TAG, "start");

#if 0
    int ret;
    string response;
    auto it = m_queue.begin();
    auto itEnd = m_queue.end();

    while (it != itEnd)
    {
        Packet* packet = *it;
        response.clear();

        if (command == "exit")
        {
            Stop();
        }
        else if (command == "win")
        {
            int keyCodes[2];
            keyCodes[0] = VK_LWIN;
            ret = KeyboardSimulator::SendInputKeyCodes(1, keyCodes);
            response = "ret: ";
            response += std::to_string(ret);
            response += "\n";
        }

        if (!response.empty())
        {
            Write(response);
        }

        ++it;
    }

    m_queue.clear();
#endif

    LOGD_CALL(TAG, "end");
}

void ControllerEngine::OnClose(uv_handle_t* handle)
{
    LOGD_CALL(TAG, "start");

    ControllerEngine* engine = (ControllerEngine*)handle->data;

    LOGD_CALL(TAG, "end");
}

void ControllerEngine::Write(Json::Value& resJson)
{
#if _DEBUG
    Json::StyledWriter writer;
#else
    Json::FastWriter writer;
#endif

    const string resText = writer.write(resJson);
    GRCPacket* grcPacket =
        GRCPacket::Create(ePT_JSON, resText.c_str(), resText.length());

    uv_buf_t* buf = (uv_buf_t*)malloc(sizeof(uv_buf_t));
    buf->base = grcPacket->GetData();
    buf->len = grcPacket->GetDataSize();
    grcPacket->SetUserData(buf);

    uv_write_t* req = (uv_write_t*)malloc(sizeof(uv_write_t));
    req->data = grcPacket;

    uv_write(req, (uv_stream_t*)m_stdout, buf, 1, OnWrite);
}

void ControllerEngine::OnWrite(uv_write_t* req, int status)
{
    LOGD_CALL(TAG, "start");

    GRCPacket* grcPacket = (GRCPacket*)req->data;

    uv_buf_t* buf = (uv_buf_t*)grcPacket->GetUserData();
    free(buf);
    free(req);

    GRCPacket::Delete(grcPacket);

    LOGD_CALL(TAG, "end");
}

void ControllerEngine::UDPSend(
    const struct sockaddr* addr,
    const PACKET_TYPE packetType, const char* body, const int bodySize)
{
    LOGD_CALL(TAG, "start");

    GRCPacket* grcPacket = GRCPacket::Create(packetType, body, bodySize);

    uv_buf_t* buf = (uv_buf_t*)malloc(sizeof(uv_buf_t));
    buf->base = grcPacket->GetData();
    buf->len = grcPacket->GetDataSize();
    grcPacket->SetUserData(buf);

    uv_udp_send_t* req = (uv_udp_send_t*)malloc(sizeof(uv_udp_send_t));
    req->data = grcPacket;

    uv_udp_send(req, m_udp, buf, 1, addr, OnUDPSend);

    LOGD_CALL(TAG, "end");
}

void ControllerEngine::OnUDPSend(uv_udp_send_t* req, int status)
{
    LOGD_CALL(TAG, "start");

    GRCPacket* grcPacket = (GRCPacket*)req->data;

    uv_buf_t* buf = (uv_buf_t*)grcPacket->GetUserData();
    free(buf);
    free(req);

    GRCPacket::Delete(grcPacket);

    LOGD_CALL(TAG, "end");
}

void ControllerEngine::OnRecvPacket(Packet* packet)
{
}

int ControllerEngine::HandlePacket(Packet* packet)
{
    LOGD_CALL(TAG, "start");

    Json::Value reqJson; // request from a client
    Json::Value resJson;
    Json::Reader reader; // json parser

    int bodySize;
    const char* data = packet->GetBody(&bodySize);

    int ret;
    bool success = reader.parse(data, data + bodySize, reqJson, false);
    if (success)
    {
        ret = ExecuteRequest(reqJson, resJson);
    }
    else
    {
        ret = GRC_INVALID_JSON;
    }

    resJson["resultCode"] = ret;
    resJson["resultDesc"] = GetErrorDesc(ret);
    const struct sockaddr* addr = (const struct sockaddr*)packet->GetUserData();

    ret = SendResponse(addr, resJson);

    LOGD_CALL(TAG, "end");

    return 0;
}

int ControllerEngine::ExecuteRequest(const Json::Value& reqJson, Json::Value& resJson)
{
    LOGD(TAG, "%s(%d) %s: start\n%s", __CALL_INFO__, reqJson.toStyledString().c_str());

    if (!reqJson["type"].isInt())
    {
        return GRC_ERR;
    }

    int ret = GRC_ERR;
    const int commandType = reqJson["type"].asInt();

    auto executer = m_executers->Get(commandType);
    if (executer)
    {
        ret = executer->Run(reqJson, resJson);
    }
    else
    {
        if (commandType == eCT_LIFECYCLE)
        {
            ret = OnLifeCycleCommand(reqJson, resJson);
        }
    }

    LOGD(TAG, "%s(%d) %s: end ret(%s)", __CALL_INFO__, GetErrorName(ret));

    return ret;
}

int ControllerEngine::OnLifeCycleCommand(
    const Json::Value& reqJson, Json::Value& resJson)
{
    LOGD_CALL(TAG, "start");

    if (!reqJson["command"].isString())
    {
        return GRC_ERR;
    }

    string command = reqJson["command"].asString();
    if (command == "exit")
    {
        Stop();
    }
    else if (command == "changeDesktop")
    {
        BOOL ret = FALSE;
        HDESK hDesk = OpenInputDesktop(0, FALSE, MAXIMUM_ALLOWED);
        if (hDesk)
        {
            ret = SetThreadDesktop(hDesk);
            CloseHandle(hDesk);
        }
        LOGI(TAG, "%s(%d) %s: SetThreadDesktop() hDesk(%p) ", __CALL_INFO__, hDesk, ret);
    }

    LOGD_CALL(TAG, "end");

    return 0;
}

int ControllerEngine::SendResponse(
    const struct sockaddr* addr, Json::Value& resJson)
{
    LOGD_CALL(TAG, "start");

#if _DEBUG
    Json::StyledWriter writer;
#else
    Json::FastWriter writer;
#endif

    const string resText = writer.write(resJson);
    UDPSend(addr, ePT_JSON, resText.data(), resText.length());

    LOGD_CALL(TAG, "end");

    return 0;
}

void ControllerEngine::OnUDPRecv(
    uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
    const struct sockaddr* addr, unsigned flags)
{
    LOGD_CALL(TAG, "start");

    ControllerEngine* engine = (ControllerEngine*)handle->data;
    engine->_OnUDPRecv(handle, nread, buf, addr, flags);

    LOGD_CALL(TAG, "end");
}

void ControllerEngine::_OnUDPRecv(
    uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
    const struct sockaddr* addr, unsigned flags)
{
    LOGD_CALL(TAG, "start");

    uint32_t type = ntohl(*(uint32_t*)buf->base);
    uint32_t bodySize = ntohl(*(uint32_t*)(buf->base + 4));
    uint32_t session = ntohl(*(uint32_t*)(buf->base + 8));
    LOGI(TAG, "%s(%d) %s nread(%d) type(%d) bodySize(%d) session(%d)",
        __CALL_INFO__, nread, type, bodySize, session);

    int ret;
    Packet packet;

    m_packetTokenizer->Input(buf->base, (int)nread);

    while ((ret = m_packetTokenizer->GetNext(&packet)) == 0)
    {
        packet.SetUserData((void*)addr);
        HandlePacket(&packet);
        m_packetTokenizer->Consume(packet.GetPacketSize());
    }

    free(buf->base);

    LOGD_CALL(TAG, "end");
}
