#pragma once
#ifndef CONTROLLER_ENGINE_H
#define CONTROLLER_ENGINE_H

#include <uv.h>
#include <State.h>
#include <list>
#include <json/json.h>
#include <ExecuterStorage.h>

GRC_NAMESPACE_BEGIN

class Packet;
class PacketTokenizer;

class ControllerEngine
{
public:
    struct Param
    {
        uint16_t port;
    };

public:
    ControllerEngine();
    ~ControllerEngine();

    int Open(const Param& param);
    int Start();
    int Stop();
    int Close();

private:
    static void OnRead(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf);
    static void OnWrite(uv_write_t* req, int status);
    static void OnPrepare(uv_prepare_t* handle);
    static void OnCheck(uv_check_t* check);
    static void OnClose(uv_handle_t* handle);
    static void OnUDPRecv(
        uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
        const struct sockaddr* addr, unsigned flags);
    static void OnUDPSend(uv_udp_send_t* req, int status);

    void OnCommand();
    void _OnCheck(uv_check_t* check);
    void _OnRead(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf);
    void _OnUDPRecv(
        uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
        const struct sockaddr* addr, unsigned flags);
    void Write(Json::Value& resJson);
    void UDPSend(const struct sockaddr* addr,
        const PACKET_TYPE packetType, const char* body, const int bodySize);
    void OnRecvPacket(Packet* packet);

    void OpenUDP(uint16_t port);
    int HandlePacket(Packet* packet);
    int ExecuteRequest(const Json::Value& reqJson, Json::Value& resJson);
    int SendResponse(const struct sockaddr* addr, Json::Value& resJson);
    int OnLifeCycleCommand(const Json::Value& reqJson, Json::Value& resJson);

private:
    gw::State m_state;
    uv_loop_t* m_loop;
    uv_tty_t* m_stdin;
    uv_tty_t* m_stdout;
    uv_prepare_t* m_prepare;
    uv_check_t* m_check;
    uv_udp_t* m_udp;
    std::list<Packet*> m_queue;
    std::unique_ptr<ExecuterStorage> m_executers;
    std::unique_ptr<PacketTokenizer> m_packetTokenizer;
};

GRC_NAMESPACE_END

#endif