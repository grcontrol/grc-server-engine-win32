#include "CharUtil.h"

GW_NAMESPACE_BEGIN

inline bool isDelimiterA(const char ch, const char* delimiters)
{
    for (int i = 0; delimiters[i] != '\0'; i++)
    {
        if (ch == delimiters[i])
        {
            return true;
        }
    }

    return false;
}

size_t indexOfA(
    const char* buf, const size_t size, const char* delimiters)
{
    if (delimiters == NULL)
    {
        return -1;
    }

    for (size_t i = 0u; i < size; i++)
    {
        if (isDelimiterA(buf[i], delimiters))
        {
            return i;
        }
    }

    return -1;
}

size_t indexNotOfA(
    const char* buf, const size_t size, const char* delimiters)
{
    if (buf == NULL || delimiters == NULL)
    {
        return -1;
    }

    for (size_t i = 0; i < size; i++)
    {
        if (!isDelimiterA(buf[i], delimiters))
        {
            return i;
        }
    }

    return -1;
}

size_t indexNotOfA(
    const char* text, const char* delimiters)
{
    if (text == NULL || delimiters == NULL)
    {
        return -1;
    }

    for (size_t i = 0; text[i] != '\0'; i++)
    {
        if (!isDelimiterA(text[i], delimiters))
        {
            return i;
        }
    }

    return -1;
}

size_t rindexOfA(const char* buf, const size_t size, const char* delimiters)
{
    if (size == 0u || delimiters == NULL)
    {
        return -1;
    }

    for (int i = size - 1; i >= 0; i--)
    {
        if (isDelimiterA(buf[i], delimiters))
        {
            return i;
        }
    }

    return -1;
}

size_t rindexOfA(const char* text, const char* delimiters)
{
    if (text == NULL)
    {
        return -1;
    }

    size_t size = strlen(text);
    return rindexOfA(text, size, delimiters);
}

size_t rindexNotOfA(
    const char* text, const size_t size, const char* delimiters)
{
    if (text == NULL || size == 0u || delimiters == NULL)
    {
        return -1;
    }

    for (int i = size - 1; i >= 0; i--)
    {
        if (!isDelimiterA(text[i], delimiters))
        {
            return i;
        }
    }

    return -1;
}

size_t rindexNotOfA(const char* text, const char* delimiters)
{
    if (text == NULL || delimiters == NULL)
    {
        return -1;
    }

    size_t size = strlen(text);
    return rindexNotOfA(text, size, delimiters);
}

char* ltrimA(char* text, const char* delimiters)
{
    size_t begin = indexNotOfA(text, delimiters);

    if (begin == static_cast<size_t> (-1))
    {
        return "";
    }

    return text + begin;
}

char* rtrimA(char* text, size_t size, const char* delimiters)
{
    const size_t end = rindexNotOfA(text, size, delimiters);

    if (end == static_cast<size_t> (-1))
    {
        return "";
    }

    text[end + 1] = '\0';
    return text;
}

char* rtrimA(char* text, const char* delimiters)
{
    if (text == NULL || delimiters == NULL)
    {
        return "";
    }

    size_t size = strlen(text);
    return rtrimA(text, size, delimiters);
}

char* trimA(char* text, const char* delimiters)
{
    return rtrimA(ltrimA(text, delimiters), delimiters);
}

int parseKeyValueLine(char* line, const char delimiter, char** key, char** value)
{
    char* text;

    if (line[0] == '#')
    {
        return -1;
    }

    text = strchr(line, delimiter);
    if (text == NULL)
    {
        return -1;
    }

    text[0] = '\0';
    *key = line;

    text++;

#if 0
    while (*text == ' ' || *text == '\t')
    {
        text++;
    }
#endif

    *value = text;

    return 0;
}

// UNICODE IMPLEMENTATION -------------------------------------------------------

inline bool isDelimiterW(const wchar_t ch, const wchar_t* delimiters)
{
    for (int i = 0; delimiters[i] != '\0'; i++)
    {
        if (ch == delimiters[i])
        {
            return true;
        }
    }

    return false;
}

size_t rindexNotOfW(
    const wchar_t* buf, const size_t size, const wchar_t* delimiters)
{
    if (size == 0u || delimiters == NULL)
    {
        return -1;
    }

    for (int i = size - 1; i >= 0; i--)
    {
        if (!isDelimiterW(buf[i], delimiters))
        {
            return i;
        }
    }

    return -1;
}

size_t indexOfW(
    const wchar_t* buf, const size_t size, const wchar_t* delimiters)
{
    if (size == 0u || delimiters == NULL)
    {
        return -1;
    }

    for (size_t i = 0u; i<size; i++)
    {
        if (isDelimiterW(buf[i], delimiters))
        {
            return i;
        }
    }

    return -1;
}

size_t indexNotOfW(
    const wchar_t* buf, const size_t size, const wchar_t* delimiters)
{
    if (delimiters == NULL)
    {
        return -1;
    }

    for (size_t i = 0u; i<size; i++)
    {
        if (!isDelimiterW(buf[i], delimiters))
        {
            return i;
        }
    }

    return -1;
}

size_t rindexOfW(
    const wchar_t* buf, const size_t size, const wchar_t* delimiters)
{
    if (size == 0u || delimiters == NULL)
    {
        return -1;
    }

    for (int i = size - 1; i >= 0; i--)
    {
        if (isDelimiterW(buf[i], delimiters))
        {
            return i;
        }
    }

    return -1;
}

size_t rindexOfW(const wchar_t* text, const wchar_t* delimiters)
{
    size_t size = wcslen(text);
    return rindexOfW(text, size, delimiters);
}

size_t rindexNotOfW(const wchar_t* text, const wchar_t* delimiters)
{
    size_t size = wcslen(text);
    return rindexNotOfW(text, size, delimiters);
}

wchar_t* ltrimW(wchar_t* text, const size_t size, const wchar_t* delimiters)
{
    return nullptr;
}

wchar_t* rtrimW(wchar_t* text, const wchar_t* delimiters)
{
    return nullptr;
}

wchar_t* trimW(wchar_t* text, const wchar_t* delimiters)
{
    return nullptr;
}

GW_NAMESPACE_END