#pragma once
#ifndef GW_DYNAMIC_BUFFER_H
#define GW_DYNAMIC_BUFFER_H

#include "gw.h"

GW_NAMESPACE_BEGIN

class DynamicBuffer
{
public:
	enum MODE_TYPE
	{
		BINARY_MODE,
		TEXT_MODE
	};

	DynamicBuffer(void);
	explicit DynamicBuffer(const int nBufSize, const MODE_TYPE mode = BINARY_MODE);
	virtual ~DynamicBuffer(void);

	int Init(int nBufSize, const MODE_TYPE mode);
	int Write(const char* pBuf, const int nDataSize);
	int Read(char* pBuf, const int nBufSize);
	int Consume(const int nSize);
	void Clear();

	int GetBufSize() const { return m_nBufSize; };
	int GetDataSize() const { return m_nDataSize; };
	char* GetBuffer() const { return m_pBuffer; };
	char* ReleaseBuffer(int& nDataSize);

private:
	MODE_TYPE m_mode;
	int m_nBufSize;
	int m_nDataSize;
	char* m_pBuffer;

	int Realloc(int nBufSize);
};

GW_NAMESPACE_END

#endif
