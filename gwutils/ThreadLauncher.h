#pragma once
#ifndef GW_THREAD_LAUCNHER_H
#define GW_THREAD_LAUCNHER_H

#include <stdio.h>
#include "gw.h"

#if defined(WIN32)

#else

#include <pthread.h>

#endif

GW_NAMESPACE_BEGIN

#if defined(WIN32)

	typedef void* HTHREAD;

#else

	typedef pthread_t HTHREAD;

#endif

enum JOIN_RETURN_VALUE
{
	eJRV_OK,
	eJRV_NO_JOIN,

	eJRV_TIMEOUT = 0x80000000,
	eJRV_INVALID_HANDLE = 0x80000001,
	eJRV_FAILED = 0x80000002,
};

const HTHREAD INVALID_THREAD_HANDLE = (const HTHREAD) (-1);

class Runnable
{
friend class ThreadLauncher;

#if defined(WIN32)
friend unsigned long __stdcall ThreadFunc(void *pData);
#else
friend void *ThreadFunc(void* pData);
#endif

public:
	Runnable() : m_hThread(INVALID_THREAD_HANDLE), m_pUserData(NULL), m_exitCode(0) {}
	virtual ~Runnable() {}

	virtual int Run(void *pUserData) = 0;
	int Join(const uint32_t timeout_ms, int* exitCode = nullptr);

	void CloseThreadHandle();
	bool IsThreadHandleValid() const { return (m_hThread != INVALID_THREAD_HANDLE); }
	HTHREAD GetThreadHandle() const { return m_hThread; }

private:
	HTHREAD m_hThread;
	void *m_pUserData;
    /**
     * Run()'s return code.
     */
    int m_exitCode;

	void SetThreadHandle(HTHREAD hThread) { m_hThread = hThread; }
    void SetExitCode(const int code) { m_exitCode = code; }

	/**
	 * These methods are used only in ThreadLauncher.
	 */
	void SetUserData(void *pUserData) { m_pUserData = pUserData; }
	void *GetUserData() const { return m_pUserData; }
};

class ThreadLauncher
{
public:
	ThreadLauncher(void);
	~ThreadLauncher(void);

	static int Execute(Runnable *pRunnable, void *pUserData = NULL);

private:
};

GW_NAMESPACE_END

#endif
