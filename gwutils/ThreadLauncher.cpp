#include "ThreadLauncher.h"
#include "gw.h"

GW_NAMESPACE_BEGIN

typedef struct _SParam
{
	Runnable *pRunnable;
	void *pUserData;
} SParam;

int Runnable::Join(const uint32_t timeout_ms, int* exitCode)
{
	if( !IsThreadHandleValid() )
	{
		return eJRV_INVALID_HANDLE;
	}

	JOIN_RETURN_VALUE ret;

#if defined(WIN32)

		DWORD dwRet = ::WaitForSingleObject(m_hThread, timeout_ms);

		switch(dwRet)
		{
		case WAIT_OBJECT_0:
            if ( exitCode )
            {
                *exitCode = m_exitCode;
            }
			ret = eJRV_OK;
			break;

		case WAIT_TIMEOUT:
			ret = eJRV_TIMEOUT;
			break;

		default:
			ret = eJRV_FAILED;
		}

#else

	if ( timeout_ms > 0u )
	{
		int rc = pthread_join(m_hThread, NULL);
        if ( rc == 0 )
        {
            if ( exitCode )
            {
                *exitCode = m_exitCode;
            }

            return eJRV_OK;
        }
        else
        {
            return eJRV_FAILED;
        }
	}
	else
	{
		ret = eJRV_NO_JOIN;
	}

#endif

	CloseThreadHandle();

	return ret;
}

void Runnable::CloseThreadHandle()
{
	if( IsThreadHandleValid() )
	{
#if defined(WIN32)

		::CloseHandle(m_hThread);

#endif

		SetThreadHandle(INVALID_THREAD_HANDLE);
	}
}

// ============================================================================

#ifdef WIN32

unsigned long __stdcall ThreadFunc(void *pData)
{
	Runnable *pRunnable = reinterpret_cast<Runnable*> (pData);
	int ret = pRunnable->Run(pRunnable->GetUserData());
    pRunnable->SetExitCode(ret);
    return ret;
}

#else

void *ThreadFunc(void* pData)
{
	Runnable *pRunnable = reinterpret_cast<Runnable*> (pData);
	int ret = pRunnable->Run(pRunnable->GetUserData());
    pRunnable->SetExitCode(ret);

    pthread_exit((void*) ret);
	return (void*) ret;
}

#endif

// ============================================================================

ThreadLauncher::ThreadLauncher(void)
{
}

ThreadLauncher::~ThreadLauncher(void)
{
}

int ThreadLauncher::Execute(Runnable* pRunnable, void* pUserData)
{
	HTHREAD hThread;
	pRunnable->SetUserData(pUserData);

#ifdef WIN32

	hThread = ::CreateThread(NULL, 0, ThreadFunc, pRunnable, 0, NULL);
	if(hThread == NULL)
	{
		return -1;
	}

#else

	int nResult;
	pthread_attr_t attr;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	nResult = pthread_create(&hThread, &attr, ThreadFunc, pRunnable);
	pthread_attr_destroy(&attr);

	if(nResult)
	{
		return -1;
	}

#endif

	pRunnable->SetThreadHandle(hThread);

	return 0;
}

GW_NAMESPACE_END
