#ifndef STRING_UTIL_H
#define STRING_UTIL_H

#include "gw.h"
#include <string>
#include <vector>

using namespace std;

#if !defined(WIN32)
#if defined(_UNICODE)
#define _T(x) L ##x
#else
#define _T(x) x
#endif
#endif

#ifdef UNICODE
typedef wchar_t tchar;
typedef wstring tstring;
typedef wostringstream tostringstream;
#else
typedef char tchar;
typedef string tstring;
typedef ostringstream tostringstream;
#endif

#ifdef UNICODE
typedef wchar_t tchar;
typedef wstring tstring;
typedef wostringstream tostringstream;
#define ltrimT ltrimW
#define rtrimT rtrimW
#define trimT trimW
#define splitT splitW
#define replaceAllT replaceAllW
#define replaceCharT replaceCharW
#define isHexT isHexW
#define trimEnclosingMarksT trimEnclosedMarksW
#define parseKeyValueLineT parseKeyValueLineW
#else
typedef char tchar;
typedef string tstring;
typedef ostringstream tostringstream;
#define ltrimT ltrimA
#define rtrimT rtrimA
#define trimT trimA
#define splitT splitA
#define replaceAllT replaceAllA
#define replaceCharT replaceCharA
#define isHexT isHexA
#define trimEnclosingMarksT trimEnclosedMarksA
#define parseKeyValueLineT parseKeyValueLineA
#endif

GW_NAMESPACE_BEGIN

bool isDelimiterA(const char ch, const string& delimiters);
bool isDelimiterW(const wchar_t ch, const wstring& delimiters);

size_t indexOfA(const char* buf, const size_t size, const string& delimiters);
inline size_t indexOfA(const string& text, const string& delimiters)
{
    return indexOfA(text.c_str(), text.size(), delimiters);
}

size_t indexNotOfA(const char* buf, const size_t size, const string& delimiters);
inline size_t indexNotOfA(const string& text, const string& delimiters)
{
    return indexNotOfA(text.c_str(), text.size(), delimiters);
}

size_t indexOfW(const wchar_t* buf, const size_t size, const wstring& delimiters);
inline size_t indexOfW(const wstring& text, const wstring& delimiters)
{
    return indexOfW(text.c_str(), text.size(), delimiters);
}

size_t indexNotOfW(const wchar_t* buf, const size_t size, const wstring& delimiters);
inline size_t indexNotOfW(const wstring& text, const wstring& delimiters)
{
    return indexNotOfW(text.c_str(), text.size(), delimiters);
}

size_t rindexOfA(const string& text, const string& delimiters);
size_t rindexOfA(const char* buf, const size_t size, const string& delimiters);

size_t rindexNotOfA(const string& text, const string& delimiters);
size_t rindexNotOfA(const char* buf, const size_t size, const string& delimiters);

size_t rindexOfW(const wstring& text, const wstring& delimiters);
size_t rindexOfW(const wchar_t* buf, const size_t size, const wstring& delimiters);

size_t rindexNotOfW(const wstring& text, const wstring& delimiters);
size_t rindexNotOfW(const wchar_t* buf, const size_t size, const wstring& delimiters);

string ltrimA(const string &text, const string& delimiters=" \t\r\n");
string ltrimA(
	const char* text, const size_t size, const string& delimiters=" \t\r\n");

wstring ltrimW(const wstring &text, const wstring& delimiters=L" \t\r\n");
wstring ltrimW(const wchar_t* text, const size_t size,
	const wstring& delimiters=L" \t\r\n");

string rtrimA(const string &text, const string& delimiters=" \t\r\n");
wstring rtrimW(const wstring &text, const wstring& delimiters=L" \t\r\n");

string trimA(const string &text, const string& delimiters=" \t\r\n");
wstring trimW(const wstring &text, const wstring& delimiters=L" \t\r\n");

string replaceAllA(
	const string &str, const string &pattern, const string &replace);
wstring replaceAllW(
	const wstring &str, const wstring &pattern, const wstring &replace);

int replaceCharA(string& text, const char oldc, const char newc);
int replaceCharW(wstring& text, const wchar_t oldc, const wchar_t newc);

void splitA(const string &text, const char sep, vector<string> &tokens);
void splitW(const wstring &text, const wchar_t sep, vector<wstring> &tokens);

#if 0
int parseKeyValueLine(
	char* szLine, const char* szSeparator, char** pszKey, char** pszValue);
#endif
int parseKeyValueLineA(
    const string& line, string& key, string& value, const char delimiter);
int parseKeyValueLineW(
    const wstring& line, wstring& key, wstring& value, const wchar_t delimiter);

bool isHexA(const string& strHex);
bool isHexW(const tstring& strHex);

bool isHexA(const string& strHex, bool bLowerCase);
bool isHexW(const wstring& strHex, bool bLowerCase);

string trimEnclosingMarksA(const string& text, const char c);
wstring trimEnclosingMarksW(const wstring& text, const wchar_t c);

size_t indexOfA(
	const char* buf, const size_t dataSize, const string& delimiters);

#define HexToUpperCaseString HexToString
int HexToBin(const char* szHex, int nBufSize, unsigned char* pBuf, int nDataSize);
int HexToString(
	const unsigned char* pBuf, int nDataSize, char* szHex, int nBufSize);
int HexToLowerCaseString(
	const unsigned char* pBuf, int nDataSize, char* szHex, int nBufSize);
int StringToHex(
	const char* szHex, int nBufSize, unsigned char* pBuf, int nDataSize);
unsigned int HexStringToInt(const char* szHex);
int StringToInt(const char* szText, const int nLength);
string IntToString(const int nValue);
string FloatToString(const float value);

string toLower(const string& text);
string toUpper(const string& text);

GW_NAMESPACE_END

#endif
