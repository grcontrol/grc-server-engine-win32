#include "CharsetUtil.h"
#include "gwlog.h"

#ifdef WIN32
#include <Windows.h>
#else
#include <errno.h>
#endif

GW_NAMESPACE_BEGIN

string UnicodeToUtf8(const wstring& strUnicode)
{
    int length = strUnicode.length();

    if (length == 0)
    {
        return "";
    }

    int bufSize = (length + 1) * 4;
    char* buf = (char*)malloc(bufSize);

    int ret = ::WideCharToMultiByte(
        CP_UTF8, 0, strUnicode.c_str(), -1,
        buf, bufSize, NULL, NULL);

    if (ret == 0)
    {
        return "";
    }

    string utf8 = buf;
    free(buf);

    return utf8;
}

wstring Utf8ToUnicode(const string& strUtf8)
{
    int length = strUtf8.length();

    if (length == 0)
    {
        return L"";
    }

    int charSize = length + 1;
    wchar_t* buf = (wchar_t*)malloc(sizeof(wchar_t) * charSize);

    int ret = ::MultiByteToWideChar(
        CP_UTF8, 0, strUtf8.c_str(), -1,
        buf, length + 1);

    if (ret <= 0)
    {
        return L"";
    }

    wstring unicode = buf;
    free(buf);

    return unicode;
}

#ifdef WIN32
string UnicodeToOem(const wstring& strUnicode)
{
	int nLength = ::WideCharToMultiByte(
		CP_OEMCP, 0, strUnicode.c_str(), strUnicode.size(), NULL, 0, NULL, NULL);

	char* szText = new char [nLength + 1];
	if ( szText == NULL )
	{
		return string();
	}
	memset(szText, 0xff, nLength + 1);

	::WideCharToMultiByte(
		CP_OEMCP, 0, strUnicode.c_str(), -1, szText, nLength, NULL, NULL);
	szText[nLength] = '\0';

	std::string strText(szText);
	delete[] szText;

	return strText;
}

wstring OemToUnicode(const string& strOem)
{
    int length = ::MultiByteToWideChar(
		CP_OEMCP, 0, strOem.c_str(), strOem.size(), NULL, 0); 

	if ( length < 1 )
	{
		return wstring();
	}

	wchar_t* buf = new wchar_t[length + 1];
	if ( buf == NULL )
	{
		return wstring();
	}

    ::MultiByteToWideChar(
		CP_OEMCP, 0, strOem.c_str(), strOem.size(), buf, length);
	buf[length] = '\0';

	wstring strUnicode(buf);
	delete[] buf;

	return strUnicode;
}

string OemToUtf8(const string& strOem)
{
	const wstring strUnicode = OemToUnicode(strOem);
	if ( strUnicode.empty() )
	{
		return string();
	}

	return UnicodeToUtf8(strUnicode);
}

string Utf8ToOem(const string& strUtf8)
{
	const wstring strUnicode = Utf8ToUnicode(strUtf8);
	if ( strUnicode.empty() )
	{
		return string();
	}

	return UnicodeToOem(strUnicode);
}
#endif

GW_NAMESPACE_END
