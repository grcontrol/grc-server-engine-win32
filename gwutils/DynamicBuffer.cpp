#include "DynamicBuffer.h"
#include "gwlog.h"

namespace
{
	const char* TAG = "UTIL";
}

GW_NAMESPACE_BEGIN

DynamicBuffer::DynamicBuffer(void) :
m_mode(BINARY_MODE),
m_nBufSize(0),
m_nDataSize(0),
m_pBuffer(NULL)
{
}

DynamicBuffer::DynamicBuffer(const int nBufSize, const MODE_TYPE mode) :
m_pBuffer(NULL)
{
	Init(nBufSize, mode);
}

DynamicBuffer::~DynamicBuffer(void)
{
	if(m_pBuffer)
	{
		delete[] m_pBuffer;
	}
}

int DynamicBuffer::Init(int nBufSize, const MODE_TYPE mode)
{
	if(m_pBuffer)
	{
		delete[] m_pBuffer;
	}

    nBufSize = max(0, nBufSize);

	if(nBufSize > 0)
	{
		if(mode == TEXT_MODE)
		{
			// for string termination character
			nBufSize++;
		}

		m_pBuffer = new char [nBufSize];
		m_pBuffer[0] = '\0';
	}
	else
	{
		m_pBuffer = NULL;
	}

	m_nBufSize = nBufSize;
	m_nDataSize = 0;
	m_mode = mode;

	return 0;
}

int DynamicBuffer::Write(const char* pBuf, const int nDataSize)
{
	if(nDataSize < 1)
	{
		return nDataSize;
	}

	int ret;
	const int nSpareBufSize = 512;

	if(m_nBufSize < m_nDataSize + nDataSize + 1)
	{
		const int nBufSize = gw_max(m_nBufSize * 2, m_nDataSize + nDataSize + nSpareBufSize);
		ret = Realloc(nBufSize);
		if (ret != 0)
		{
			return -1;
		}
	}

	memcpy(m_pBuffer + m_nDataSize, pBuf, nDataSize);
	m_nDataSize += nDataSize;

	if(m_mode == TEXT_MODE)
	{
		m_pBuffer[m_nDataSize] = '\0';
	}

	return nDataSize;
}

int DynamicBuffer::Read(char* pBuf, const int nBufSize)
{
	if(nBufSize < 0)
	{
		return -1;
	}

	int nBytesRead = gw_min(m_nDataSize, nBufSize);
	memcpy(pBuf, m_pBuffer, nBytesRead);

	return nBytesRead;
}

int DynamicBuffer::Consume(const int nSize)
{
	const int nConsumeSize = gw_min(m_nDataSize, nSize);

	if(nConsumeSize > 0)
	{
		m_nDataSize -= nConsumeSize;

		if(m_nDataSize > 0)
		{
			memmove(m_pBuffer, m_pBuffer + nConsumeSize, m_nDataSize);

			if(m_mode == TEXT_MODE)
			{
				m_pBuffer[m_nDataSize] = '\0';
			}
		}
	}

	return nConsumeSize;
}

void DynamicBuffer::Clear()
{
	if(m_nDataSize > 0)
	{
		m_nDataSize = 0;
	}
}

int DynamicBuffer::Realloc(int nBufSize)
{
	LOGV(TAG,
        "%s(%d) %s: Buffer overflow. bufsize(%d) m_nDataSize(%d) nBufSize(%d)",
		__CALL_INFO__, m_nBufSize, m_nDataSize, nBufSize);

	if (m_mode == TEXT_MODE)
	{
		nBufSize++;
	}

	// +1 is reserved for a string termination character.
	char* pBuffer = new char [nBufSize];
	if (pBuffer == NULL)
	{
		return -1;
	}

	if(m_pBuffer)
	{
		memcpy(pBuffer, m_pBuffer, m_nDataSize);
		delete[] m_pBuffer;
	}

	LOGV(TAG, "%s(%d) %s: Reallocation old(%d) -> new(%d)", __CALL_INFO__, m_nBufSize, nBufSize);

	m_pBuffer = pBuffer;
	m_nBufSize = nBufSize;

	if(m_mode == TEXT_MODE && m_pBuffer)
	{
		m_pBuffer[m_nDataSize] = '\0';
	}

	return 0;
}

/**
 * m_pBuffer should not be used any more after calling ReleaseBuffer().
 * Because m_pBuffer is located at heap memory, caller is in charge of deallocating m_pBuffer.
 */
char* DynamicBuffer::ReleaseBuffer(int& nDataSize)
{
	char* pBuffer = m_pBuffer;
	nDataSize = m_nDataSize;

	m_pBuffer = NULL;
	m_nBufSize = 0;
	m_nDataSize = 0;

	return pBuffer;
}

GW_NAMESPACE_END
