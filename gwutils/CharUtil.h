#pragma once
#include "gw.h"

/**
These functions are the same as those of StringUtil.h except for not using string library.
 */

GW_NAMESPACE_BEGIN

char* ltrimA(char* text, const char* delimiters = " \t\r\n");
char* rtrimA(char* text, const char* delimiters = " \t\r\n");
char* trimA(char* text, const char* delimiters = " \t\r\n");
int parseKeyValueLine(char* line, const char delimiter, char** key, char** value);

wchar_t* ltrimW(wchar_t* text, const wchar_t* delimiters = L" \t\r\n");
wchar_t* rtrimW(wchar_t* text, const wchar_t* delimiters = L" \t\r\n");
wchar_t* trimW(wchar_t* text, const wchar_t* delimiters = L" \t\r\n");

GW_NAMESPACE_END