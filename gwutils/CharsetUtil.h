#ifndef GW_CHARSET_UTIL_H
#define GW_CHARSET_UTIL_H

#include "gw.h"
#include <stdio.h>
#include <string>

using namespace std;

#if UNICODE
    #define _UTF8(x) gw::UnicodeToUtf8(x)
#else
    #define _UTF8(x) x
#endif

GW_NAMESPACE_BEGIN

string UnicodeToUtf8(const wstring& strUnicode);
wstring Utf8ToUnicode(const string& strUtf8);

string UnicodeToOem(const wstring& strUnicode);
wstring OemToUnicode(const string& strOem);
string OemToUtf8(const string& strOem);
string Utf8ToOem(const string& strUtf8);

GW_NAMESPACE_END

#endif
