#include "StringUtil.h"
#include <string>
#include <vector>
#include <sstream>

#if defined(LINUX)
	#include <string.h>
#endif

using namespace std;

GW_NAMESPACE_BEGIN

inline bool isDelimiterA(const char ch, const string& delimiters)
{
	return( delimiters.find(ch) != string::npos );
}

#if UNICODE
inline bool isDelimiterW(const wchar_t ch, const wstring& delimiters)
{
	return( delimiters.find(ch) != string::npos );
}
#endif

size_t indexOfA(
    const char* buf, const size_t size, const string& delimiters)
{
	if ( delimiters.empty() )
	{
		return -1;
	}

	for ( size_t i=0u; i<size; i++ )
	{
		if ( isDelimiterA(buf[i], delimiters) )
		{
			return i;
		}
	}

	return -1;
}

#if UNICODE
size_t indexOfW(
	const wchar_t* buf, const size_t size, const wstring& delimiters)
{
	if ( size == 0u || delimiters.empty() )
	{
		return -1;
	}

	for ( size_t i=0u; i<size; i++ )
	{
		if ( isDelimiterW(buf[i], delimiters) )
		{
			return i;
		}
	}

	return -1;
}
#endif

size_t indexNotOfA(
    const char* buf, const size_t size, const string& delimiters)
{
   	if ( delimiters.empty() )
	{
		return -1;
	}

	for ( size_t i=0; i<size; i++ )
	{
		if ( !isDelimiterA(buf[i], delimiters) )
		{
			return i;
		}
	}

	return -1;
}

#if UNICODE
size_t indexNotOfW(
    const wchar_t* buf, const size_t size, const wstring& delimiters)
{
   	if ( delimiters.empty() )
	{
		return -1;
	}

	for ( size_t i=0u; i<size; i++ )
	{
		if ( !isDelimiterW(buf[i], delimiters) )
		{
			return i;
		}
	}

	return -1;
}
#endif

size_t rindexOfA(const char* buf, const size_t size, const string& delimiters)
{
    if ( size == 0u || delimiters.empty() )
	{
		return -1;
	}

	for ( int i=size-1; i>=0; i-- )
	{
		if( isDelimiterA(buf[i], delimiters) )
		{
			return i;
		}
	}

	return -1;
}

size_t rindexOfA(const string& text, const string& delimiters)
{
	return rindexOfA(text.c_str(), text.size(), delimiters);
}

#if UNICODE
size_t rindexOfW(
	const wchar_t* buf, const size_t size, const wstring& delimiters)
{
    if ( size == 0u || delimiters.empty() )
	{
		return -1;
	}

	for ( int i=size-1; i>=0; i-- )
	{
		if( isDelimiterW(buf[i], delimiters) )
		{
			return i;
		}
	}

	return -1;
}

size_t rindexOfW(const wstring& text, const wstring& delimiters)
{
	return rindexOfW(text.c_str(), text.size(), delimiters);
}
#endif

size_t rindexNotOfA(
    const char* buf, const size_t size, const string& delimiters)
{
    if ( size == 0u || delimiters.empty() )
	{
		return -1;
	}

	for ( int i=size-1; i>=0; i-- )
	{
		if( !isDelimiterA(buf[i], delimiters) )
		{
			return i;
		}
	}

	return -1;
}

size_t rindexNotOfA(const string& text, const string& delimiters)
{
    return rindexNotOfA(text.c_str(), text.size(), delimiters);
}

#if UNICODE
size_t rindexNotOfW(
    const wchar_t* buf, const size_t size, const wstring& delimiters)
{
    if ( size == 0u || delimiters.empty() )
	{
		return -1;
	}

	for ( int i=size-1; i>=0; i-- )
	{
		if( !isDelimiterW(buf[i], delimiters) )
		{
			return i;
		}
	}

	return -1;
}

size_t rindexNotOfW(const wstring& text, const wstring& delimiters)
{
    return rindexNotOfW(text.c_str(), text.size(), delimiters);
}
#endif

string ltrimA(const string &text, const string& delimiters)
{
	size_t begin = indexNotOfA(text, delimiters);

	if ( begin == static_cast<size_t> (-1) )
	{
		return string();
	}

	return (begin == 0u) ? text : text.substr(begin);
}

#if UNICODE
wstring ltrimW(const wstring &text, const wstring& delimiters)
{
	size_t begin = indexNotOfW(text, delimiters);

	if ( begin == static_cast<size_t> (-1) )
	{
		return wstring();
	}

	return (begin == 0u) ? text : text.substr(begin);
}
#endif

string rtrimA(const string &text, const string& delimiters)
{
	const size_t end = rindexNotOfA(text, delimiters);

	if ( end == static_cast<size_t> (-1) )
	{
		return string();
	}

    return (end == text.size() - 1) ? text : text.substr(0, end + 1u);
}

#if UNICODE
wstring rtrimW(const wstring &text, const wstring& delimiters)
{
	const size_t end = rindexNotOfW(text, delimiters);

	if ( end == static_cast<size_t> (-1) )
	{
		return wstring();
	}

    return (end == text.size() - 1) ? text : text.substr(0, end + 1u);
}
#endif

string trimA(const string &text, const string& delimiters)
{
	return rtrimA( ltrimA(text, delimiters), delimiters );
}

#if UNICODE
wstring trimW(const wstring &text, const wstring& delimiters)
{
	return rtrimW( ltrimW(text, delimiters), delimiters );
}
#endif

string replaceAllA(
	const string &str, const string &pattern, const string &replace)  
{
	string result = str;
	string::size_type pos = 0;
	string::size_type offset = 0;

	while ( (pos = result.find(pattern, offset)) != string::npos )
	{
		result.replace(result.begin() + pos,
			result.begin() + pos + pattern.size(), replace);
		offset = pos + replace.size();
	}

	return result;
}

#if UNICODE
wstring replaceAllW(
	const wstring &str, const wstring &pattern, const wstring &replace)
{
	wstring result = str;
	wstring::size_type pos = 0;
	wstring::size_type offset = 0;

	while((pos = result.find(pattern, offset)) != wstring::npos)
	{
		result.replace(result.begin() + pos,
			result.begin() + pos + pattern.size(), replace);
		offset = pos + replace.size();
	}

	return result;
}
#endif

void splitA(const string &text, const char sep, vector<string> &tokens)
{
	size_t start = 0u;
	size_t end;

	while ((end = text.find(sep, start)) != string::npos)
	{
		const int length = end - start;

		if(length > 0)
		{
			tokens.push_back(text.substr(start, length));
		}

		start = end + 1;
	}

	if ( text.size() > start )
	{
		tokens.push_back(text.substr(start));
	}
}

#if UNICODE
void splitW(const wstring &text, const wchar_t sep, vector<wstring> &tokens)
{
	size_t start = 0u;
	size_t end;

	while ((end = text.find(sep, start)) != wstring::npos)
	{
		const int length = end - start;

		if(length > 0)
		{
			tokens.push_back(text.substr(start, length));
		}

		start = end + 1;
	}

	if ( text.size() > start )
	{
		tokens.push_back(text.substr(start));
	}
}
#endif

int replaceCharA(string& text, const char oldc, const char newc)
{
	if ( oldc == newc )
	{
		return 0;
	}

	int replace = 0;

	for ( auto& c : text )
	{
		if ( c == oldc )
		{
			c = newc;
			replace++;
		}
	}

	return replace;
}

#if UNICODE
int replaceCharW(wstring& text, const wchar_t oldc, const wchar_t newc)
{
	if ( oldc == newc )
	{
		return 0;
	}

	int replace = 0;
	for ( auto& c : text )
	{
		if ( c == oldc )
		{
			c = newc;
			replace++;
		}
	}

	return replace;
}
#endif

#if 0
int parseKeyValueLine(
	char* szLine, const char* szSeparator, char** pszKey, char** pszValue)
{
	char* szText;

	if(szLine[0] == '#')
	{
		return -1;
	}

	szText = strchr(szLine, szSeparator[0]);
	if(szText == NULL)
	{
		return -1;
	}

	szText[0] = '\0';
	*pszKey = szLine;

	szText++;

	while(*szText == ' ' || *szText == '\t')
	{
		szText++;
	}

	*pszValue = szText;

	return 0;
}
#endif

int parseKeyValueLineA(
    const string& line, string& key, string& value, const char delimiter)
{
    const size_t found = line.find(delimiter);
    if ( found == string::npos )
    {
        key = line;
        value.clear();
        return -1;
    }

    key = line.substr(0, found);
    value = line.substr(found + 1);

    return 0;
}

int parseKeyValueLineW(
    const wstring& line, wstring& key, wstring& value, const wchar_t delimiter)
{
    const size_t found = line.find(delimiter);
    if ( found == wstring::npos )
    {
        key = line;
        value.clear();
        return -1;
    }

    key = line.substr(0, found);
    value = line.substr(found + 1);

    return 0;
}

bool isHexW(const wstring& strHex)
{
	if ( strHex.empty() )
	{
		return false;
	}

	for ( const auto ch : strHex )
	{
		if ( !( (ch >= _T('0') && ch <= _T('9')) ||
			    (ch >= _T('a') && ch <= _T('f')) ||
			    (ch >= _T('A') && ch <= _T('F')) ) )
		{
			return false;
		}
	}

	return true;
}

bool isHexA(const string& strHex)
{
	if ( strHex.empty() )
	{
		return false;
	}

	for ( const auto ch : strHex )
	{
		if ( !isxdigit(ch) )
		{
			return false;
		}
	}

	return true;
}

bool isHexA(const string& strHex, const bool lowercase)
{
	if ( strHex.empty() )
	{
		return false;
	}

	for ( const auto ch : strHex )
	{
		if ( isdigit(ch) )
		{
			continue;
		}
		if ( lowercase )
		{
			if ( ch >= 'a' && ch <= 'f' )
			{
				continue;
			}
		}
		else
		{
			if ( ch >= 'A' && ch <= 'F' )
			{
				continue;
			}
		}

		return false;
	}

	return true;
}

#if UNICODE
bool isHexW(const wstring& strHex, const bool lowercase)
{
	if ( strHex.empty() )
	{
		return false;
	}

	for ( const auto ch : strHex )
	{
		if ( isdigit(ch) )
		{
			continue;
		}

		if ( lowercase )
		{
			if ( ch >= _T('a') && ch <= _T('f') )
			{
				continue;
			}
		}
		else
		{
			if ( ch >= _T('A') && ch <= _T('F') )
			{
				continue;
			}
		}

		return false;
	}

	return true;
}
#endif

int HexToString(
	const unsigned char* pBuf, int nDataSize, char* szHex, int nBufSize)
{
    int i;
    int j;
    int nCount = nBufSize / 2;
   
    if(nDataSize < nCount)
    {  
        return -1;
    }
   
    const char octet[] =
    {  
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'B', 'C', 'D', 'E', 'F'
    };
   
    for(i=0, j=0; i<nCount; i++, j+=2)
    {  
        szHex[j]   = octet[ (pBuf[i] & 0xF0) >> 4 ];
        szHex[j+1] = octet[ pBuf[i] & 0x0F ];
    }
   
    return j;
}

/**
 * Hex binary -> lower case string.
 * @return string's length
 */
int HexToLowerCaseString(
	const unsigned char* pBuf, int nDataSize, char* szHex, int nBufSize)
{
	int i;
	int j;
	int nCount = nBufSize / 2;
   
	if(nDataSize < nCount)
	{  
		return -1;
	}
   
	const char octet[] =
	{  
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'a', 'b', 'c', 'd', 'e', 'f'
	};
   
	for(i=0, j=0; i<nCount; i++, j+=2)
	{  
		szHex[j]   = octet[ (pBuf[i] & 0xF0) >> 4 ];
		szHex[j+1] = octet[ pBuf[i] & 0x0F ];
	}

	return j;
}

unsigned char GetNibble(char chHex)
{
	unsigned char byteNibble = 0;

	if(chHex >= '0' && chHex <= '9')
	{
		byteNibble = chHex - '0';
	}
	else if(chHex >= 'A' && chHex <= 'F')
	{
		byteNibble = chHex - 'A' + 10;
	}
	else if(chHex >= 'a' && chHex <= 'f')
	{
		byteNibble = chHex - 'a' + 10;
	}

	return byteNibble;
}

/**
 * Big endian only
 */
int HexToBin(const char* szHex, int length, unsigned char* pBuf, int bufSize)
{
    int binSize = (length / 2) + (length % 2);
    if (bufSize < binSize)
    {
        return -1;
    }

    memset(pBuf, 0, bufSize);

    int start = 0;
    // If the length of hexa string is odd
    if (length % 2)
    {
        start = 1;
        pBuf[0] = GetNibble(szHex[0]);
        ++pBuf;
    }

    for (int i = start; i < length; i += 2, ++pBuf)
    {
        *pBuf = (GetNibble(szHex[i]) << 4) | GetNibble(szHex[i + 1]);
    }

    return binSize;
}

/**
 * Little endian only
 */
int StringToHex(
	const char* szHex, int nBufSize, unsigned char* pBuf, int nDataSize)
{
	int i;
	int j;
	int nIndex;
	int nCount = nBufSize / 2;

	if(nDataSize < nCount)
	{
		return -1;
	}

	unsigned char uchNibble;

	j = 0;
	nIndex = nBufSize - 1;
	memset(pBuf, 0, nDataSize);

    for(i=0; i<nBufSize; i++, nIndex--)
    {
		uchNibble = GetNibble(szHex[nIndex]);

		if(i % 2)
		{
			pBuf[j] |= uchNibble << 4;
			j++;
		}
		else
		{
			pBuf[j] = uchNibble;
		}
    }

	return j;
}

unsigned int HexStringToInt(const char* szHex)
{
	unsigned int nValue = 0;

	for(int i=0; szHex[i] != '\0'; i++)
	{
		nValue <<= 4;
		nValue += GetNibble( szHex[i] );
	}

	return nValue;
}

int StringToInt(const char* szText, const int nLength)
{
	int nValue = 0;

	for(int i=0; i<nLength; i++)
	{
		const char ch = szText[i];

		if(ch < '0' && ch > '9')
		{
			return 0;
		}

		nValue *= 10;
		nValue += ch - '0';
	}

	return nValue;
}

string IntToString(const int nValue)
{
	ostringstream oss;
	oss << nValue;

	return oss.str();
}

string FloatToString(const float value)
{
    ostringstream oss;
    oss << fixed << value;
    return oss.str();
}

/**
 * "school" -> school
 * 'help' -> help
 */
string trimEnclosingMarksA(const string& text, const char c)
{
	string result;

	if ( text[0] != c )
	{
		return text;
	}

	result = text.substr(1);

	if ( result.back() == c )
	{
		result = result.substr(0, result.size() - 1 );
	}

	return result;
}

#if UNICODE
wstring trimEnclosingMarksW(const wstring& text, const wchar_t c)
{
	wstring result;

	if ( text[0] != c )
	{
		return text;
	}

	result = text.substr(1);

	if ( result.back() == c )
	{
		result = result.substr(0, result.size() - 1 );
	}

	return result;
}
#endif

string toLower(const string& text)
{
    string result = text;

    for (size_t i = 0; i < result.length(); i++)
    {
        char& c = result[i];

        if (c >= 'A' && c <= 'Z')
        {
            c += 32;
        }
    }

    return result;
}

string toUpper(const string& text)
{
    string result = text;

    for (size_t i = 0; i < result.length(); i++)
    {
        char& c = result[i];

        if (c >= 'a' && c <= 'z')
        {
            c -= 32;
        }
    }

    return result;
}

GW_NAMESPACE_END
