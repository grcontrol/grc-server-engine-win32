#include "PathUtil.h"
#include <sys/stat.h>
#include <list>
#include <sstream>
#if defined(WIN32)
	#include <Windows.h>
	#include <direct.h>
#else
	#include <string.h>
#endif
#ifdef OSX
	#include <CoreFoundation/CFBundle.h>
#endif

GW_NAMESPACE_BEGIN

string ExtractFileNameA(const char* szPathName, const char chPathDelimiter)
{
	const char szDot = '.';
	const char* szFileName = strrchr(szPathName, chPathDelimiter);
	szFileName = (szFileName == NULL) ? szPathName : szFileName + 1;

	string strFileName(szFileName);

	size_t found = strFileName.rfind(szDot);
	if(found != wstring::npos)
	{
		strFileName.resize(found);
	}

	return strFileName;
}

#if UNICODE
wstring ExtractFileNameW(
	const wchar_t* szPathName, const wchar_t chPathDelimiter)
{
	const wchar_t szDot = '.';
	const wchar_t* szFileName = wcsrchr(szPathName, chPathDelimiter);
	szFileName = (szFileName == NULL) ? szPathName : szFileName + 1;

	wstring strFileName(szFileName);

	size_t found = strFileName.rfind(szDot);
	if(found != wstring::npos)
	{
		strFileName.resize(found);
	}

	return strFileName;
}
#endif

#if UNICODE
const wchar_t* ExtractFileExtensionW(
	const wchar_t* szPathName, const wchar_t chPathDelimiter)
{
	const wchar_t szDot = '.';
	const wchar_t* szFileName = wcsrchr(szPathName, chPathDelimiter);
	szFileName = (szFileName == NULL) ? szPathName : szFileName + 1;

	const wchar_t* szFileExtension = wcsrchr(szFileName, szDot);
	if(szFileExtension == NULL)
	{
		return NULL;
	}

	return szFileExtension + 1;
}
#endif

const char* ExtractFileExtensionA(
	const char* szPathName, const char chPathDelimiter)
{
	const char szDot = '.';
	const char* szFileName = strrchr(szPathName, chPathDelimiter);
	szFileName = (szFileName == NULL) ? szPathName : szFileName + 1;

	const char* szFileExtension = strrchr(szFileName, szDot);
	if(szFileExtension == NULL)
	{
		return NULL;
	}

	return szFileExtension + 1;
}

string ExtractLastNameFromPathA(
	const string strDir, const char chPathDelimiter)
{
	size_t index = strDir.rfind(chPathDelimiter);

	if ( index == string::npos )
	{
		return strDir;
	}
	else
	{
		return strDir.substr(index + 1);
	}
}

#if UNICODE
wstring ExtractLastNameFromPathW(
	const wstring strDir, const wchar_t chPathDelimiter)
{
	size_t index = strDir.rfind(chPathDelimiter);

	if ( index == wstring::npos )
	{
		return strDir;
	}
	else
	{
		return strDir.substr(index + 1);
	}
}
#endif

string ExtractParentPathA(const string& strPath, const char chPathDelimiter)
{
	size_t found = strPath.rfind(chPathDelimiter);
	if ( found == string::npos )
	{
		return string();
	}

	return strPath.substr(0, found);
}

#if UNICODE
wstring ExtractParentPathW(
	const wstring& strPath, const wchar_t chPathDelimiter)
{
	size_t found = strPath.rfind(chPathDelimiter);
	if ( found == wstring::npos )
	{
		return wstring();
	}

	return strPath.substr(0, found);
}
#endif

string RemoveTrailingPathDelimiterA(
	const string& strDir, const char chPathDelimiter)
{
	const size_t length = strDir.length();

	if ( length > 0u )
	{
		if ( strDir[length - 1] == chPathDelimiter )
		{
			return strDir.substr(0, length -1);
		}
	}

	return strDir;
}

#if UNICODE
wstring RemoveTrailingPathDelimiterW(
	const wstring& strDir, const wchar_t chPathDelimiter)
{
	const size_t length = strDir.length();

	if ( length > 0u )
	{
		if ( strDir[length - 1] == chPathDelimiter )
		{
			return strDir.substr(0, length -1);
		}
	}

	return strDir;
}
#endif

static bool IsModeA(const string &strPath, unsigned short mode)
{
	int nRet;

	struct stat stat;
	nRet = ::stat(strPath.c_str(), &stat);

	if(nRet < 0)
	{
		return false;
	}

	return ((stat.st_mode & mode) == mode);
}

#ifdef UNICODE
static bool IsModeW(const wstring &strPath, unsigned short mode)
{
	int nRet;

	struct _stat64 stat;
	nRet = ::_tstat64(strPath.c_str(), &stat);

	if(nRet < 0)
	{
		return false;
	}

	return ((stat.st_mode & mode) == mode);
}
#endif

bool IsFileA(const string &strPath)
{
	return IsModeA(strPath, S_IFREG);
}

bool IsDirectoryA(const string &strPath)
{
	return IsModeA(strPath, S_IFDIR);
}

#ifdef UNICODE
bool IsFileW(const wstring &strPath)
{
	return IsModeW(strPath, S_IFREG);
}

bool IsDirectoryW(const wstring &strPath)
{
	return IsModeW(strPath, S_IFDIR);
}
#endif

int64_t GetFileSize(FILE* fp)
{
#ifdef WIN32

	struct __stat64 fileStatus;
	int ret = _fstat64(_fileno(fp), &fileStatus);

#elif defined(OSX)

	struct stat fileStatus;
	int ret = fstat(fileno(fp), &fileStatus);
    
#else
    
	struct stat64 fileStatus;
	int ret = fstat64(fileno(fp), &fileStatus);

#endif

	if( ret < 0 )
	{
		return ret;
	}

	return fileStatus.st_size;
}

int64_t GetFileSizeByPathA(const string& strPath)
{
#ifdef WIN32

	struct __stat64 fileStatus;
	int ret = _stat64(strPath.c_str(), &fileStatus);

#elif defined(OSX)

	struct stat fileStatus;
	int ret = stat(strPath.c_str(), &fileStatus);
    
#else
    
	struct stat64 fileStatus;
	int ret = stat64(strPath.c_str(), &fileStatus);

#endif

	if( ret < 0 )
	{
		return ret;
	}

	return fileStatus.st_size;
}

#if UNICODE
int64_t GetFileSizeByPathW(const wstring& strPath)
{
#ifdef WIN32

	struct __stat64 fileStatus;
	int ret = _wstat64(strPath.c_str(), &fileStatus);

#else

	struct stat64 fileStatus;
	int ret = stat64(strPath.c_str(), &fileStatus);

#endif

	if( ret < 0 )
	{
		return ret;
	}

	return fileStatus.st_size;
}
#endif

int MakeDirectoryRecursivelyA(string strDir, const uint32_t mode)
{
	list<string> dirs;
	string strToken;

	while ( strDir.size() > 1u )
	{
		if ( IsDirectoryA(strDir) )
		{
			break;
		}

		const size_t find = strDir.rfind(CH_PATH_DELIMITER);
		if ( find == string::npos )
		{
			return -1;
		}

		// ex) token includes path delimiter.
		strToken = strDir.substr(find);
		dirs.push_front(strToken);

		strDir = strDir.substr(0, find);
	}

	for ( const auto& token : dirs )
	{
		strDir.append(token);

		int ret = gw_mkdir(strDir.c_str(), mode);
		if ( ret < 0 )
		{
			return -1;
		}
	}

	return 0;
}

#if UNICODE
int MakeDirectoryRecursivelyW(wstring strDir, const uint32_t mode)
{
	list<wstring> dirs;
	wstring strToken;

	while ( strDir.size() > 1u )
	{
		if ( IsDirectoryW(strDir) )
		{
			break;
		}

		const size_t find = strDir.rfind(_T(CH_PATH_DELIMITER));
		if ( find == wstring::npos )
		{
			return -1;
		}

		// ex) token includes path delimiter.
		strToken = strDir.substr(find);
		dirs.push_front(strToken);

		strDir = strDir.substr(0, find);
	}

	for ( const auto& token : dirs )
	{
		strDir.append(token);

		int ret = gw_tmkdir(strDir.c_str(), mode);
		if ( ret < 0 )
		{
			return -1;
		}
	}

	return 0;
}
#endif

/**
 * dirs[0] = /a
 * dirs[1] = b
 * dirs[2] = c
 */
int MakeDirectoryRecursivelyA(const vector<string>& dirs, const uint32_t mode)
{
	if ( dirs.empty() )
	{
		return -1;
	}

	ostringstream oss;
	string strPath;

	for ( const auto& token : dirs )
	{
		oss << token;
		strPath = oss.str();

		if ( !IsDirectoryA(strPath) )
		{
			int ret = gw_mkdir(strPath.c_str(), mode);

			if ( ret < 0 )
			{
				return -1;
			}
		}

		oss << CH_PATH_DELIMITER;
	}

	return 0;
}

#if UNICODE
/**
 * dirs[0] = /a
 * dirs[1] = b
 * dirs[2] = c
 */
int MakeDirectoryRecursivelyW(const vector<wstring>& dirs, const uint32_t mode)
{
	if ( dirs.empty() )
	{
		return -1;
	}

	wostringstream oss;
	wstring strPath;

	for ( const auto& token : dirs )
	{
		oss << token;
		strPath = oss.str();

		if ( !IsDirectoryT(strPath) )
		{
			int ret = gw_tmkdir(strPath.c_str(), mode);

			if ( ret < 0 )
			{
				return -1;
			}
		}

		oss << _T(CH_PATH_DELIMITER);
	}

	return 0;
}
#endif

tstring GetInstallDirectory()
{
#ifdef WIN32
	TCHAR szPath[MAX_PATH];

	int length = static_cast<int> (GetModuleFileName(NULL, szPath, MAX_PATH));
	if ( length == 0 )
	{
		return tstring();
	}

	int i = length - 1;
	for (; i>=0; i-- )
	{
		if ( szPath[i] == _T(CH_PATH_DELIMITER) )
		{
			szPath[i] = _T('\0');
			break;
		}
	}

	if ( i < 0 )
	{
		return tstring();
	}

	return tstring(szPath);
#elif defined(OSX)
    //    return _T("/tmp");
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    
    CFURLRef resourceURL = CFBundleCopyBundleURL(mainBundle);
    CFURLRef resource2 = CFURLCopyAbsoluteURL(resourceURL);
    CFStringRef path = CFURLCopyPath(resource2);
    
    
    CFIndex length = CFStringGetLength(path);
    CFIndex maxSize = CFStringGetMaximumSizeForEncoding(length, kCFStringEncodingUTF8);
    char* buffer = (char *)malloc(maxSize);
    
    CFStringGetCString(path, buffer, maxSize, kCFStringEncodingUTF8);
    std::string pathString;
    pathString.append(buffer);
    free(buffer);
    
    pathString.append(_T("Contents"));
    pathString.append(_T(SZ_PATH_DELIMITER));
    pathString.append(_T("Resources"));
    return pathString;
#else
	return tstring();
#endif

}

#ifdef OSX
tstring GetExecDirectory()
{
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    
    CFURLRef resourceURL = CFBundleCopyBundleURL(mainBundle);
    CFURLRef resource2 = CFURLCopyAbsoluteURL(resourceURL);
    CFStringRef path = CFURLCopyPath(resource2);
    
    
    CFIndex length = CFStringGetLength(path);
    CFIndex maxSize = CFStringGetMaximumSizeForEncoding(length, kCFStringEncodingUTF8);
    char* buffer = (char *)malloc(maxSize);
    
    CFStringGetCString(path, buffer, maxSize, kCFStringEncodingUTF8);
    std::string pathString;
    pathString.append(buffer);
    free(buffer);
    
    pathString.append(_T("Contents"));
    pathString.append(_T(SZ_PATH_DELIMITER));
    pathString.append(_T("MacOS"));
    pathString.append(_T(SZ_PATH_DELIMITER));
    return pathString;
}
#endif

int IsContainedA(const string& strPath1, const string& strPath2)
{
	if ( strPath1.empty() || strPath2.empty() )
	{
		return -1;
	}

	string strPath[2];

	if ( strPath1.size() > strPath2.size() )
	{
		strPath[0] = strPath2;
		strPath[1] = strPath1;
	}
	else
	{
		strPath[0] = strPath1;
		strPath[1] = strPath2;
	}

	for ( size_t i=0; i<2; i++ )
	{
		if ( strPath[i][strPath[i].size() - 1] != CH_PATH_DELIMITER )
		{
			strPath[i].append(SZ_PATH_DELIMITER);
		}
	}

	return (strPath[1].compare(0, strPath[0].size(), strPath[0]) == 0) ? 1 : 0;
}

#if UNICODE
int IsContainedW(const wstring& strPath1, const wstring& strPath2)
{
	if ( strPath1.empty() || strPath2.empty() )
	{
		return -1;
	}

	wstring strPath[2];

	if ( strPath1.size() > strPath2.size() )
	{
		strPath[0] = strPath2;
		strPath[1] = strPath1;
	}
	else
	{
		strPath[0] = strPath1;
		strPath[1] = strPath2;
	}

	for ( size_t i=0; i<2; i++ )
	{
		if ( strPath[i][strPath[i].size() - 1] != _T(CH_PATH_DELIMITER) )
		{
			strPath[i].append( _T(SZ_PATH_DELIMITER) );
		}
	}

	return (strPath[1].compare(0, strPath[0].size(), strPath[0]) == 0) ? 1 : 0;
}
#endif

size_t CopyFileWithFilePointer(FILE* fpIn, FILE* fpOut)
{
	size_t size = 0u;
	char buf[1024];

	while ( !feof(fpIn) )
	{
		size_t bytesRead = fread(buf, 1, sizeof(buf), fpIn);
		if ( size == static_cast<size_t> (-1) )
		{
			return -1;
		}

		size_t bytesWritten = fwrite(buf, 1, bytesRead, fpOut);
		if ( bytesWritten != bytesRead )
		{
			return -1;
		}

		size += bytesRead;
	}

	return size;
}

/**
 * pathes[0] = /a
 * pathes[1] = b
 * pathes[2] = c
 * ...
 */
string ConcatenatePathA(const vector<string>& paths, const char chPathDelimiter)
{
	const size_t size = paths.size();

	if ( size == 0 )
	{
		return "";
	}

	ostringstream oss;
	oss << paths[0];

	for(size_t i=1; i<size; i++)
	{
		oss << chPathDelimiter << paths[i];
	}

	return oss.str();
}

#if UNICODE
wstring ConcatenatePathW(const vector<wstring>& paths, const wchar_t chPathDelimiter)
{
	const size_t size = paths.size();

	if ( size == 0 )
	{
		return _T("");
	}

	tostringstream oss;
	oss << paths[0];

	for(size_t i=1; i<size; i++)
	{
		oss << chPathDelimiter << paths[i];
	}

	return oss.str();
}
#endif

int MakeDirectorySafelyA(const string& dir, const uint32_t mode)
{
	int ret = 0;

	if ( !IsDirectoryA(dir) )
	{
		ret = gw_mkdir(dir.c_str(), mode);
	}

	return ret;
}

#if UNICODE
int MakeDirectorySafelyW(const wstring& dir, const uint32_t mode)
{
	int ret = 0;

	if ( !IsDirectoryW(dir) )
	{
		ret = gw_tmkdir(dir.c_str(), mode);
	}

	return ret;
}
#endif

GW_NAMESPACE_END
