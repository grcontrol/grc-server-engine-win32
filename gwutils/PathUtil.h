#ifndef __PATH_UTIL_H__
#define __PATH_UTIL_H__

#include "gw.h"
#include <string>
#include <vector>
#include "StringUtil.h"

using namespace std;

#if UNICODE
	#define ExtractFileNameT ExtractFileNameW
	#define ExtractFileExtensionT ExtractFileExtensionW
	#define ExtractLastNameFromPathT ExtractLastNameFromPathW
	#define ExtractParentPathT ExtractParentPathW
	#define RemoveTrailingPathDelimiterT RemoveTrailingPathDelimiterW
	#define IsDirectoryT IsDirectoryW
	#define IsFileT IsFileW
	#define GetFileSizeByPathT GetFileSizeByPathW
	#define IsContainedT IsContainedW
	#define MakeDirectoryRecursivelyT MakeDirectoryRecursivelyW
	#define MakeDirectorySafelyT MakeDirectorySafelyW
	#define ConcatenatePathT ConcatenatePathW
#else
	#define ExtractFileNameT ExtractFileNameA
	#define ExtractFileExtensionT ExtractFileExtensionA
	#define ExtractLastNameFromPathT ExtractLastNameFromPathA
	#define ExtractParentPathT ExtractParentPathA
	#define RemoveTrailingPathDelimiterT RemoveTrailingPathDelimiterA
	#define IsDirectoryT IsDirectoryA
	#define IsFileT IsFileA
	#define GetFileSizeByPathT GetFileSizeByPathA
	#define IsContainedT IsContainedA
	#define MakeDirectoryRecursivelyT MakeDirectoryRecursivelyA
	#define MakeDirectorySafelyT MakeDirectorySafelyA
	#define ConcatenatePathT ConcatenatePathA
#endif

GW_NAMESPACE_BEGIN

string ExtractFileNameA(const char* szPathName, const char chPathDelimiter);
const char* ExtractFileExtensionA(const char* szPathName, const char chPathDelimiter);
string ExtractLastNameFromPathA(
	const string strDir, const char chPathDelimiter=_T(CH_PATH_DELIMITER));
string ExtractParentPathA(
	const string& strPath, const char chPathDelimiter=_T(CH_PATH_DELIMITER));
string RemoveTrailingPathDelimiterA(
	const string& strDir, const char chPathDelimiter=_T(CH_PATH_DELIMITER));
bool IsFileA(const string &strPath);
bool IsDirectoryA(const string &strPath);
int64_t GetFileSizeByPathA(const string& strPath);
int IsContainedA(const string& strPath1, const string& strPath2);
int MakeDirectoryRecursivelyA(string strDir, const uint32_t mode);
int MakeDirectoryRecursivelyA(const vector<string>& dirs, const uint32_t mode);
int MakeDirectorySafelyA(const string& dir, const uint32_t mode);

#if UNICODE
wstring ExtractFileNameW(const wchar_t* szPathName, const wchar_t chPathDelimiter);
const wchar_t* ExtractFileExtensionW(const wchar_t* szPathName, const wchar_t chPathDelimiter);
wstring ExtractLastNameFromPathW(
	const wstring strDir, const wchar_t chPathDelimiter=_T(CH_PATH_DELIMITER));
wstring ExtractParentPathW(
	const wstring& strPath, const wchar_t chPathDelimiter=_T(CH_PATH_DELIMITER));
wstring RemoveTrailingPathDelimiterW(
	const wstring& strDir, const wchar_t chPathDelimiter=_T(CH_PATH_DELIMITER));
bool IsFileW(const wstring& strPath);
bool IsDirectoryW(const wstring& strPath);
int64_t GetFileSizeByPathW(const wstring& strPath);
int IsContainedW(const wstring& strPath1, const wstring& strPath2);
int MakeDirectoryRecursivelyW(wstring strDir, const uint32_t mode);
int MakeDirectoryRecursivelyW(const vector<wstring>& dirs, const uint32_t mode);
int MakeDirectorySafelyW(const wstring& dir, const uint32_t mode);
#endif

int64_t GetFileSize(FILE* fp);
tstring GetInstallDirectory();
size_t CopyFileWithFilePointer(FILE* fpIn, FILE* fpOut);

#ifdef OSX
tstring GetExecDirectory();
#endif

GW_NAMESPACE_END

#endif
