#!/bin/sh

git submodule init
git submodule update

# googletest
# Do nothing

# jsoncpp 
pushd ./ext/jsoncpp
python amalgamate.py
popd

# libubv
pushd ./ext/libuv
chmod +x ./vcbuild.bat
./vcbuild.bat
popd
