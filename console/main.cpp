// grc-server-engine-win32.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "GRCEngine.h"
#include <gwlog.h>
#include <json/json.h>
#include <GRCPacket.h>

#if 0
int main()
{
	gwlog_init("grc.log", eLO_CONSOLE, eLL_DEBUG);

	int ret;
	GRCEngine engine;

	ret = engine.Init();
	
	GRCEngine::Param param;
	param.port = 1106;
	ret = engine.Open(param);
	ret = engine.Start();

    printf("Press any key to exit.\n");
    getchar();

	ret = engine.Stop();
	ret = engine.Exit();

	gwlog_exit();

    return 0;
}
#endif

int startWSA()
{
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    /* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0) {
        /* Tell the user that we could not find a usable */
        /* Winsock DLL.                                  */
        printf("WSAStartup failed with error: %d\n", err);
        return 1;
    }

    return 0;
}

void CreateProcess(PROCESS_INFORMATION& pi)
{
    BOOL ret;
    STARTUPINFO si;

#if 0
    HANDLE hReadPipeOut, hWritePipeOut;
    HANDLE hReadPipeIn, hWritePipeIn;

    SECURITY_ATTRIBUTES saAttr;
    saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
    saAttr.bInheritHandle = TRUE;
    saAttr.lpSecurityDescriptor = NULL;

    if (!CreatePipe(&hReadPipeOut, &hWritePipeOut, &saAttr, 4096))
    {
        return -1;
    }

    if (!CreatePipe(&hReadPipeIn, &hWritePipeIn, &saAttr, 4096))
    {
        return -1;
    }
#endif

    ZeroMemory(&si, sizeof(STARTUPINFO));
    si.cb = sizeof(STARTUPINFO);
    si.lpDesktop = TEXT("winsta0\\default");
#if 0
    si.dwFlags = STARTF_USESTDHANDLES;
    si.hStdInput = hReadPipeIn;
    si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
    si.hStdError = GetStdHandle(STD_ERROR_HANDLE);
#endif

    TCHAR* path = _T("e:/goldworm/grc/grc-server-engine-win32/Debug/controller.exe");
    ret = CreateProcess(
        path,
        NULL,
        NULL,
        NULL,
        TRUE,
        NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW,
        NULL,
        NULL,
        &si,
        &pi
    );

    printf("Create a child process. Press any key to continue.\n");
    getchar();
}

int main()
{
    startWSA();

    PROCESS_INFORMATION pi;
    pi.hProcess = INVALID_HANDLE_VALUE;
    CreateProcess(pi);

    int ret;
    Json::Value root;
    root["type"] = 6; // keyboard

    Json::Value keyCodes;
    keyCodes.append(VK_LWIN);
    keyCodes.append(VK_PAUSE);
    root["keyCodes"] = keyCodes;

    Json::StyledWriter writer;
    string resText = writer.write(root);

    GRCPacket* grcPacket = GRCPacket::Create(ePT_JSON, resText.data(), resText.size());
    char* packet = grcPacket->GetData();
    int packetSize = grcPacket->GetDataSize();

#if 0
    DWORD bytesWritten;
    ret = WriteFile(hWritePipeIn, packet, resText.size() + 24, &bytesWritten, NULL);
    printf("ret: %d\n", ret);
#endif

    SOCKET sock = socket(AF_INET, SOCK_DGRAM, 0);

    struct sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(1105);
    saddr.sin_addr.s_addr = 0x0100007f;

    ret = sendto(sock, packet, packetSize, 0, (struct sockaddr*)&saddr, sizeof(saddr));
    GRCPacket::Delete(grcPacket);

    root.clear();

    char buf[256];
    struct sockaddr_in from;
    int fromlen = sizeof(from);
    ret = recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr*)&from, &fromlen);
    int type = htonl(*((int*)buf));
    int size = htonl(*((int*)buf + 4));
    int session = htonl(*((int*)buf + 8));
    Json::Reader reader;
    if (reader.parse(buf + 12, buf + 12 + size, root))
    {
        string response = writer.write(root);
        printf(response.c_str());
    }

    root.clear();
    root["type"] = 5;
    root["x"] = 0;
    root["y"] = 0;
    root["flags"] = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE;
    resText = writer.write(root);
    grcPacket = GRCPacket::Create(ePT_JSON, resText.data(), resText.size());
    packet = grcPacket->GetData();
    packetSize = grcPacket->GetDataSize();

    ret = sendto(sock, packet, packetSize, 0, (struct sockaddr*)&saddr, sizeof(saddr));
    GRCPacket::Delete(grcPacket);

    type = htonl(*((int*)buf));
    size = htonl(*((int*)buf + 4));
    session = htonl(*((int*)buf + 8));
    if (reader.parse(buf + 12, buf + 12 + size, root))
    {
        string response = writer.write(root);
        printf(response.c_str());
    }

    root.clear();
    root["type"] = 100;
    root["command"] = "exit";
    resText = writer.write(root);
    grcPacket = GRCPacket::Create(ePT_JSON, resText.data(), resText.size());
    packet = grcPacket->GetData();
    packetSize = grcPacket->GetDataSize();
    ret = sendto(sock, packet, packetSize, 0, (struct sockaddr*)&saddr, sizeof(saddr));
    GRCPacket::Delete(grcPacket);

    closesocket(sock);

    if (pi.hProcess != INVALID_HANDLE_VALUE)
    {
        WaitForSingleObject(pi.hProcess, INFINITE);
    }

    WSACleanup();
}