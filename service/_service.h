#pragma once
#ifndef _SERVICE_H
#define _SERVICE_H

#include <tchar.h>

// 
// Settings of the service
// 

// Internal name of the service
#define SERVICE_NAME             L"GRCWindowsService"

// Displayed name of the service
#define SERVICE_DISPLAY_NAME     L"Goldworm Remote Controller"

// Service start options.
#define SERVICE_START_TYPE       SERVICE_DEMAND_START

// List of service dependencies - "dep1\0dep2\0\0"
#define SERVICE_DEPENDENCIES     L""

// The name of the account under which the service should run
//#define SERVICE_ACCOUNT          L"NT AUTHORITY\\LocalService"
//#define SERVICE_ACCOUNT          L"NT AUTHORITY\\NetworkService"
#define SERVICE_ACCOUNT          NULL // LocalSystem account

// The password to the service account name
#define SERVICE_PASSWORD         NULL

#define SERVICE_PORT 1029

TCHAR* __stdcall GetLastErrorMessage(int error);

#endif
