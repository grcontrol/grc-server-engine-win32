#pragma once
#ifndef SERVICE_ENGINE_H
#define SERVICE_ENGINE_H

#include <_grc.h>
#include <uv.h>
#include <State.h>
#include <list>
#include <unordered_map>
#include <json/json.h>
#include <ThreadSafe.h>

GRC_NAMESPACE_BEGIN

class Packet;
class PacketTokenizer;
class ControllerMonitor;
class Executer;

class ServiceEngine
{
public:
    enum ASYNC_COMMAND
    {
        eAC_NONE = -1,
        eAC_CONSOLE_CONNECT,
        eAC_CONSOLE_DISCONNECT,
        eAC_SESSION_LOCK,
        eAC_SESSION_UNLOCK,
    };

    struct Param
    {
        uint16_t port;
        const char* configPath;
    };

public:
    ServiceEngine();
    ~ServiceEngine();

    int Open(const Param& param);
    int Start();
    int Stop();
    int Close();

    //int RunAsyncCommand(ASYNC_COMMAND command);
    int OnSessionChange(int state);

private:
    static void OnPrepare(uv_prepare_t* handle);
    static void OnCheck(uv_check_t* check);
    static void OnClose(uv_handle_t* handle);
    static void OnUDPRecv(
        uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
        const struct sockaddr* addr, unsigned flags);
    static void OnUDPSend(uv_udp_send_t* req, int status);
    static void OnAsyncStop(uv_async_t* async);
    static void OnSessionChange(uv_async_t* async);
    static void OnFSEvent(
        uv_fs_event_t* fs, const char* filename, int events, int status);

    int _Stop();
    void _OnUDPRecv(
        uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
        const struct sockaddr* addr, unsigned flags);
    void _OnSessionChange(uv_async_t* async);

    void OpenUDP(uint16_t port);
    void UDPSend(uv_udp_t* udp,
        const struct sockaddr* addr,
        const PACKET_TYPE packetType, const char* body, const int bodySize);
    int HandlePacket(Packet* packet);
    int ExecuteRequest(const Json::Value& reqJson, Json::Value& resJson);
    int SendResponse(const struct sockaddr* addr, Json::Value& resJson);
    int SendJsonPacket(uv_udp_t* udp, const struct sockaddr* addr, Json::Value& resJson);
    int StartController(int sessionState);

private:
    gw::State m_state;
    uv_loop_t* m_loop;
    uv_prepare_t* m_prepare;
    uv_check_t* m_check;
    // android <-> serviceEngine
    uv_udp_t* m_udpServer;
    // serviceEngine <-> controller
    uv_udp_t* m_udpClient;
    uv_async_t* m_asyncStop;
    std::list<Packet*> m_queue;
    std::unique_ptr<PacketTokenizer> m_packetTokenizer;

    int m_sessionState;
    uv_mutex_t* m_mutexSessionChange;
    uv_async_t* m_asyncSessionChange;

    string m_configPath;
    uv_fs_event_t* m_fs;

    std::unordered_map<int, shared_ptr<Executer>> m_executers;
    std::unique_ptr<ControllerMonitor> m_montior;
};

GRC_NAMESPACE_END

#endif