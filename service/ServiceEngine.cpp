#include "ServiceEngine.h"
#include <gwlog.h>
#include <Packet.h>
#include <GRCPacket.h>
#include "ControllerMonitor.h"
#include <EchoExecuter.h>

GRC_NAMESPACE_BEGIN

ServiceEngine::ServiceEngine() :
    m_packetTokenizer(new PacketTokenizer(4096)),
    m_loop(NULL),
    m_udpServer(NULL),
    m_udpClient(NULL),
    m_asyncStop(NULL),
    m_asyncSessionChange(NULL),
    m_fs(NULL),
    m_check(NULL),
    m_prepare(NULL),
    m_montior(new ControllerMonitor())
{
    LOGD_CALL(TAG, "start");

    //DebugBreak();

    m_state.SetState(gw::eLCS_INIT);

    m_executers[eCT_ECHO] =
        shared_ptr<Executer>(new EchoExecuter());

    LOGD_CALL(TAG, "end");
}

ServiceEngine::~ServiceEngine()
{
    LOGD_CALL(TAG, "start");

    m_executers.clear();

    LOGD_CALL(TAG, "end");
}

int ServiceEngine::Open(const Param& param)
{
    LOGD_CALL(TAG, "start");

    m_loop = (uv_loop_t*)malloc(sizeof(uv_loop_t));
    uv_loop_init(m_loop);
    m_loop->data = this;

    OpenUDP(param.port);

    m_prepare = (uv_prepare_t*)malloc(sizeof(uv_prepare_t));
    uv_prepare_init(m_loop, m_prepare);
    m_prepare->data = this;

    m_check = (uv_check_t*)malloc(sizeof(uv_check_t));
    uv_check_init(m_loop, m_check);
    m_check->data = this;

    m_asyncStop = (uv_async_t*)malloc(sizeof(uv_async_t));
    uv_async_init(m_loop, m_asyncStop, OnAsyncStop);
    m_asyncStop->data = this;

    m_mutexSessionChange = (uv_mutex_t*)malloc(sizeof(uv_mutex_t));
    uv_mutex_init(m_mutexSessionChange);

    m_asyncSessionChange = (uv_async_t*)malloc(sizeof(uv_async_t));
    uv_async_init(m_loop, m_asyncSessionChange, ServiceEngine::OnSessionChange);
    m_asyncSessionChange->data = this;

    m_configPath = param.configPath;
    m_fs = (uv_fs_event_t*)malloc(sizeof(uv_fs_event_t));
    uv_fs_event_init(m_loop, m_fs);
    m_fs->data = this;

    m_state.SetState(gw::eLCS_OPEN);

    LOGD_CALL(TAG, "end");
    return 0;
}

void ServiceEngine::OpenUDP(uint16_t port)
{
    LOGD(TAG, "%s(%d) %s: start port(%d)", __CALL_INFO__, port);

    int ret;

    uv_udp_t* udp = (uv_udp_t*)malloc(sizeof(uv_udp_t));
    uv_udp_init(m_loop, udp);
    udp->data = this;

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(port);

    ret = uv_udp_bind(udp, (const sockaddr*)&addr, UV_UDP_REUSEADDR);
    if (ret < 0)
    {
        LOGE(TAG, "%s(%d) %s: error(%s) %s",
            __CALL_INFO__, uv_err_name(ret), uv_strerror(ret));

        free(udp);
    }
    else
    {
        m_udpServer = udp;
    }

    LOGD_CALL(TAG, "end");
}

int ServiceEngine::Start()
{
    LOGD_CALL(TAG, "start");

    StartController(WTS_SESSION_LOGON);

    int ret;
    ret = uv_prepare_start(m_prepare, OnPrepare);
    ret = uv_check_start(m_check, OnCheck);
    ret = uv_udp_recv_start(m_udpServer, ::OnAlloc, OnUDPRecv);
    m_queue.clear();

    ret = uv_fs_event_start(m_fs, OnFSEvent, m_configPath.c_str(), 0);

    m_state.SetState(gw::eLCS_START);
    ret = uv_run(m_loop, UV_RUN_DEFAULT);

    LOGD_CALL(TAG, "end");
    return 0;
}

int ServiceEngine::StartController(int sessionState)
{
    LOGD_CALL(TAG, "start");

    m_montior->Stop(INFINITE);

    TCHAR* filePath =
        _T("E:\\goldworm\\grc\\grc-server-engine-win32\\Debug\\controller.exe");
    int ret = m_montior->Start(filePath, sessionState, true);
    LOGI(TAG, "%s(%d) %s: ControllerMonitor returns %d", __CALL_INFO__, ret);

    LOGD_CALL(TAG, "end");

    return ret;
}

int ServiceEngine::Stop()
{
    LOGD_CALL(TAG, "start");

    int ret;

    if (m_state.GetState() == gw::eLCS_START)
    {
        if (m_asyncStop)
        {
            ret = uv_async_send(m_asyncStop);
        }

        m_state.SetState(gw::eLCS_STOPPING);
    }

    LOGD_CALL(TAG, "end");
    return 0;
}

int ServiceEngine::_Stop()
{
    LOGD_CALL(TAG, "start");

    uv_check_stop(m_check);
    uv_prepare_stop(m_prepare);
    uv_udp_recv_stop(m_udpServer);

    uv_close((uv_handle_t*)m_prepare, ServiceEngine::OnClose);
    uv_close((uv_handle_t*)m_check, ServiceEngine::OnClose);
    uv_close((uv_handle_t*)m_udpServer, ServiceEngine::OnClose);
    uv_close((uv_handle_t*)m_asyncStop, ServiceEngine::OnClose);
    uv_close((uv_handle_t*)m_asyncSessionChange, ServiceEngine::OnClose);
    uv_close((uv_handle_t*)m_fs, ServiceEngine::OnClose);

    uv_stop(m_loop);

    m_montior->Stop(3000);

    m_state.SetState(gw::eLCS_OPEN);

    LOGD_CALL(TAG, "end");
    return 0;
}

int ServiceEngine::Close()
{
    LOGD_CALL(TAG, "start");

    // Call uv_loop_close function before deallocating handles
    // otherwise a crash happens in libuv.
    uv_loop_close(m_loop);

    if (m_loop)
    {
        free(m_loop);
        m_loop = NULL;
    }
    if (m_check)
    {
        free(m_check);
        m_check = NULL;
    }
    if (m_prepare)
    {
        free(m_prepare);
        m_prepare = NULL;
    }
    if (m_udpServer)
    {
        free(m_udpServer);
        m_udpServer = NULL;
    }
    if (m_asyncStop)
    {
        free(m_asyncStop);
        m_asyncStop = NULL;
    }
    if (m_mutexSessionChange)
    {
        free(m_mutexSessionChange);
        m_mutexSessionChange = NULL;
    }
    if (m_asyncSessionChange)
    {
        free(m_asyncSessionChange);
        m_asyncSessionChange = NULL;
    }
    if (m_fs)
    {
        free(m_fs);
        m_fs = NULL;
        m_configPath.clear();
    }

    m_state.SetState(gw::eLCS_INIT);

    LOGD_CALL(TAG, "end");
    return 0;
}

void ServiceEngine::OnPrepare(uv_prepare_t* handle)
{
    LOGD_CALL(TAG, "start");
    ServiceEngine* engine = (ServiceEngine*)handle->data;
    LOGD_CALL(TAG, "end");
}

void ServiceEngine::OnCheck(uv_check_t* check)
{
    LOGD_CALL(TAG, "start");
    ServiceEngine* engine = (ServiceEngine*)check->data;
    LOGD_CALL(TAG, "end");
}

void ServiceEngine::OnClose(uv_handle_t* handle)
{
    LOGD_CALL(TAG, "start");

    ServiceEngine* engine = (ServiceEngine*)handle->data;

    LOGD_CALL(TAG, "end");
}

void ServiceEngine::UDPSend(
    uv_udp_t* udp, const struct sockaddr* addr,
    const PACKET_TYPE packetType, const char* body, const int bodySize)
{
    LOGD_CALL(TAG, "start");

    GRCPacket* grcPacket = GRCPacket::Create(packetType, body, bodySize);

    uv_buf_t* buf = (uv_buf_t*)malloc(sizeof(uv_buf_t));
    buf->base = grcPacket->GetData();
    buf->len = grcPacket->GetDataSize();
    grcPacket->SetUserData(buf);

    uv_udp_send_t* req = (uv_udp_send_t*)malloc(sizeof(uv_udp_send_t));
    req->data = grcPacket;

    uv_udp_send(req, udp, buf, 1, addr, OnUDPSend);

    LOGD_CALL(TAG, "end");
}

void ServiceEngine::OnUDPSend(uv_udp_send_t* req, int status)
{
    LOGD_CALL(TAG, "start");

    GRCPacket* grcPacket = (GRCPacket*)req->data;

    uv_buf_t* buf = (uv_buf_t*)grcPacket->GetUserData();
    free(buf);
    free(req);

    GRCPacket::Delete(grcPacket);

    LOGD_CALL(TAG, "end");
}

int ServiceEngine::HandlePacket(Packet* packet)
{
    LOGD_CALL(TAG, "start");

    Json::Value reqJson; // request from a client
    Json::Value resJson;
    Json::Reader reader; // json parser

    int bodySize;
    const char* data = packet->GetBody(&bodySize);

    int ret;
    bool success = reader.parse(data, data + bodySize, reqJson, false);
    if (success)
    {
        ret = ExecuteRequest(reqJson, resJson);
    }
    else
    {
        ret = GRC_INVALID_JSON;
    }

    resJson["resultCode"] = ret;
    resJson["resultDesc"] = GetErrorDesc(ret);
    const struct sockaddr* addr = (const struct sockaddr*)packet->GetUserData();

    ret = SendResponse(addr, resJson);

    LOGD_CALL(TAG, "end");

    return 0;
}

int ServiceEngine::ExecuteRequest(const Json::Value& reqJson, Json::Value& resJson)
{
    LOGD(TAG, "%s(%d) %s: start\n%s", __CALL_INFO__, reqJson.toStyledString().c_str());

    if (!reqJson["type"].isInt())
    {
        return GRC_ERR;
    }

    int ret = GRC_ERR;
    const int commandType = reqJson["type"].asInt();

    auto found = m_executers.find(commandType);
    if (found != m_executers.end())
    {
        auto executer = found->second;
        ret = executer->Run(reqJson, resJson);
    }

    LOGD(TAG, "%s(%d) %s: end ret(%s)", __CALL_INFO__, GetErrorName(ret));

    return ret;
}

int ServiceEngine::SendResponse(const struct sockaddr* addr, Json::Value& resJson)
{
    return SendJsonPacket(m_udpServer, addr, resJson);
}

int ServiceEngine::SendJsonPacket(
    uv_udp_t* udp, const struct sockaddr* addr, Json::Value& json)
{
    LOGD_CALL(TAG, "start");

#if _DEBUG
    Json::StyledWriter writer;
#else
    Json::FastWriter writer;
#endif

    const string resText = writer.write(json);
    UDPSend(udp, addr, ePT_JSON, resText.data(), resText.length());

    LOGD_CALL(TAG, "end");

    return 0;
}

void ServiceEngine::OnUDPRecv(
    uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
    const struct sockaddr* addr, unsigned flags)
{
    LOGD_CALL(TAG, "start");

    ServiceEngine* engine = (ServiceEngine*)handle->data;
    engine->_OnUDPRecv(handle, nread, buf, addr, flags);

    LOGD_CALL(TAG, "end");
}

void ServiceEngine::_OnUDPRecv(
    uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
    const struct sockaddr* addr, unsigned flags)
{
    LOGD_CALL(TAG, "start");

    uint32_t type = ntohl(*(uint32_t*)buf->base);
    uint32_t bodySize = ntohl(*(uint32_t*)(buf->base + 4));
    uint32_t session = ntohl(*(uint32_t*)(buf->base + 8));
    LOGI(TAG, "%s(%d) %s nread(%d) type(%d) bodySize(%d) session(%d)",
        __CALL_INFO__, nread, type, bodySize, session);

    int ret;
    Packet packet;

    m_packetTokenizer->Input(buf->base, (int)nread);

    while ((ret = m_packetTokenizer->GetNext(&packet)) == 0)
    {
        packet.SetUserData((void*)addr);
        HandlePacket(&packet);
        m_packetTokenizer->Consume(packet.GetPacketSize());
    }

    free(buf->base);

    LOGD_CALL(TAG, "end");
}

void ServiceEngine::OnAsyncStop(uv_async_t* async)
{
    LOGD_CALL(TAG, "start");

    ServiceEngine* engine = (ServiceEngine*)async->data;
    engine->_Stop();

    LOGD_CALL(TAG, "end");
}

void ServiceEngine::OnSessionChange(uv_async_t* async)
{
    LOGD_CALL(TAG, "start");

    ServiceEngine* engine = (ServiceEngine*)async->data;
    engine->_OnSessionChange(async);

    LOGD_CALL(TAG, "end");
}

void ServiceEngine::_OnSessionChange(uv_async_t* async)
{
    LOGD_CALL(TAG, "start");

    int sessionState;

    uv_mutex_lock(m_mutexSessionChange);
    sessionState = m_sessionState;
    uv_mutex_unlock(m_mutexSessionChange);

    int ret = StartController(sessionState);

    LOGD_CALL(TAG, "end");
}

int ServiceEngine::OnSessionChange(int sessionState)
{
    LOGD_CALL(TAG, "start");

    if (m_state.GetState() == gw::eLCS_START)
    {
        uv_mutex_lock(m_mutexSessionChange);
        m_sessionState = sessionState;
        uv_mutex_unlock(m_mutexSessionChange);

        uv_async_send(m_asyncSessionChange);
    }

    LOGD_CALL(TAG, "end");

    return 0;
}

void ServiceEngine::OnFSEvent(
    uv_fs_event_t* fs, const char* filename, int events, int status)
{
    LOGD(TAG, "%s(%d) %s: start path(%s) events(%d) status(%d)",
        __CALL_INFO__, filename, events, status);
    LOGD_CALL(TAG, "end");
}


GRC_NAMESPACE_END