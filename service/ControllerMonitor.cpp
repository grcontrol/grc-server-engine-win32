#include "ControllerMonitor.h"
#include <tlhelp32.h>
#include <Wtsapi32.h>
#include <gwlog.h>
#include "_service.h"
#include <CharsetUtil.h>

static const char* TAG = "GRCSRV";

ControllerMonitor::ControllerMonitor()
{
    ZeroMemory(&m_pi, sizeof(m_pi));
}

ControllerMonitor::~ControllerMonitor()
{
}

int ControllerMonitor::Start(TCHAR* filePath, int sessionState, bool elevateProcess)
{
    LOGD_CALL(TAG, "start");

    BOOL bRet;

    SECURITY_ATTRIBUTES sa;
    ZeroMemory(&sa, sizeof(SECURITY_ATTRIBUTES));

    HANDLE hToken = GetToken(elevateProcess, &sa);
    if (hToken == NULL || hToken == INVALID_HANDLE_VALUE)
    {
        const DWORD error = GetLastError();
        string errorMessage;
        TCHAR* message = GetLastErrorMessage(error);
        if (message)
        {
            errorMessage = _UTF8(message);
            errorMessage = errorMessage.substr(0, errorMessage.size() - 2);
        }

        LOGD(TAG, "%s(%d) %s: Failed to create a token error(%d) msg(%s)",
            __CALL_INFO__, error, errorMessage.c_str());

        if (message)
        {
            LocalFree(message);
        }

        return -1;
    }

    TCHAR* desktop = GetDesktopName(sessionState);
    LOGI(TAG, "%s(%d) %s: desktop(%s) is selected.",
        __CALL_INFO__, _UTF8(desktop).c_str());

    bRet = StartProcess(filePath, desktop, hToken, &m_pi);
    LOGI(TAG, "%s(%d) %s: ret(%d) process(%p) thread(%p)",
        __CALL_INFO__, bRet, m_pi.hProcess, m_pi.hThread);

    if (hToken)
    {
        CloseHandle(hToken);
    }

    return bRet ? 0 : -1;
}

TCHAR* ControllerMonitor::GetDesktopName(int sessionChange)
{
    TCHAR* winlogonDesktop = _T("winsta0\\Winlogon");
    TCHAR* defaultDesktop = _T("winsta0\\Default");
    TCHAR* disconnectDesktop = _T("winsta0\\Disconnect");

    switch (sessionChange)
    {
    case WTS_CONSOLE_DISCONNECT:
    case WTS_SESSION_LOGOFF:
    case WTS_SESSION_LOCK:
        return winlogonDesktop;
    case WTS_SESSION_LOGON:
    case WTS_SESSION_UNLOCK:
        return defaultDesktop;
    default:
        return winlogonDesktop;
    }
}

int ControllerMonitor::Stop(uint32_t timeout_ms)
{
    LOGD_CALL(TAG, "start");

    if (m_pi.hProcess == NULL)
    {
        return 0;
    }

    int ret = -1;

    ::TerminateProcess(m_pi.hProcess, 0);

    DWORD dwRet = WaitForSingleObject(m_pi.hProcess, timeout_ms);
    if (dwRet == WAIT_OBJECT_0)
    {
        ret = 0;
    }
    else if (dwRet == WAIT_TIMEOUT)
    {
        ret = 1;
    }

    if (m_pi.hProcess)
    {
        CloseHandle(m_pi.hProcess);
    }
    if (m_pi.hThread)
    {
        CloseHandle(m_pi.hThread);
    }

    ZeroMemory(&m_pi, sizeof(m_pi));
    LOGD_CALL(TAG, "end");

    return 0;
}

BOOL ControllerMonitor::StartProcess(
    TCHAR* commandLine, TCHAR* desktop,
    HANDLE hToken, PROCESS_INFORMATION* pi)
{
    LOGD(TAG, "%s(%d) %s: start token(%p)", __CALL_INFO__, hToken);

    BOOL bRet;

    STARTUPINFO si;
    ZeroMemory(&si, sizeof(STARTUPINFO));
    si.cb = sizeof(STARTUPINFO);
    si.lpDesktop = desktop;
    //si.lpDesktop = elevateProcess ? _T("winsta0\\Winlogon") : _T("winsta0\\default");

    TCHAR path[MAX_PATH];
    _tcscpy_s(path, MAX_PATH, commandLine);

    bRet = CreateProcessAsUser(
        hToken,             // client's access token
        NULL,               // file to execute
        path,               // command line
        NULL,              // pointer to process SECURITY_ATTRIBUTES
        NULL,              // pointer to thread SECURITY_ATTRIBUTES
        FALSE,             // handles are not inheritable
        //NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW,   // creation flags
        NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE,   // creation flags
        //NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE,   // creation flags
        NULL,              // pointer to new environment block 
        NULL,              // name of current directory 
        &si,               // pointer to STARTUPINFO structure
        pi     // receives information about new process
    );

    LOGD(TAG, "%s(%d) %s: CreateProcessAsUser() ret(%d) pid(%d)\n",
        __CALL_INFO__, bRet, pi->dwProcessId);

    return bRet;
}

HANDLE ControllerMonitor::GetToken(bool elevateProcess, LPSECURITY_ATTRIBUTES sa)
{
    LOGD_CALL(TAG, "start");

    HANDLE hToken = NULL;

    if (elevateProcess)
    {
        hToken = GetAdminToken(_T("winlogon.exe"), sa);
    }
    else
    {
        hToken = GetUserToken(sa);
    }

    LOGD(TAG, "%s(%d) %s: end token(%p)", __CALL_INFO__, hToken);

    return hToken;
}

HANDLE ControllerMonitor::GetAdminToken(
    const TCHAR* fileName, LPSECURITY_ATTRIBUTES sa)
{
    LOGD_CALL(TAG, "start");

    int ret;
    BOOL bRet;
    HANDLE hToken = NULL;
    HANDLE hProcess = NULL;
    HANDLE hSourceToken = NULL;

    if (!GetProcessHandle(fileName, &hProcess))
    {
        return NULL;
    }

    bRet = OpenProcessToken(
        hProcess,
        TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS,
        &hSourceToken);

    if (!bRet)
    {
        goto EXIT;
    }

    hToken = DuplicateToken(hSourceToken, sa);
    ret = AdjustTokenPrivileges(hToken);
    if (ret < 0)
    {
        CloseHandle(hToken);
        hToken = NULL;
    }

    //DWORD uiAccess = 1;
    //SetTokenInformation(hToken, TokenUIAccess, &uiAccess, sizeof(DWORD));

EXIT:
    if (hProcess)
    {
        CloseHandle(hProcess);
    }
    if (hSourceToken)
    {
        CloseHandle(hSourceToken);
    }

    LOGD_CALL(TAG, "end");

    return hToken;
}

int ControllerMonitor::AdjustTokenPrivileges(HANDLE token)
{
    TOKEN_PRIVILEGES tokenPrivileges = { 0 };
    tokenPrivileges.PrivilegeCount = 1;

    LUID seDebugNameValue = { 0 };
    if (!LookupPrivilegeValue(nullptr, SE_DEBUG_NAME, &seDebugNameValue))
    {
        LOGE(TAG, "%s(%d) %s: LookupPrivilegeValue() is failed.", __CALL_INFO__);
        return -1;
    }

    tokenPrivileges.Privileges[0].Luid = seDebugNameValue;
    tokenPrivileges.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    if (!::AdjustTokenPrivileges(token, false, &tokenPrivileges, 0, nullptr, nullptr))
    {
        LOGE(TAG, "%s(%d) %s: AdjustTokenPrivileges() is failed.", __CALL_INFO__);
        return -2;
    }

    return 0;
}

HANDLE ControllerMonitor::GetUserToken(LPSECURITY_ATTRIBUTES sa)
{
    LOGD_CALL(TAG, "start");

    HANDLE sourceToken = NULL;
    DWORD activeSessionId = WTSGetActiveConsoleSessionId();

    if (!WTSQueryUserToken(activeSessionId, &sourceToken))
    {
        LOGE(TAG, "%s(%d) %s: WTSQueryUserToken() error activeSessionId(%d)",
            __CALL_INFO__, activeSessionId);
        return NULL;
    }

    LOGD_CALL(TAG, "end");

    return DuplicateToken(sourceToken, sa);
}

HANDLE ControllerMonitor::DuplicateToken(
    HANDLE sourceToken, LPSECURITY_ATTRIBUTES sa)
{
    LOGD_CALL(TAG, "start");

    HANDLE newToken = NULL;

    SetLastError(S_OK);

    BOOL ret = DuplicateTokenEx(
        sourceToken, MAXIMUM_ALLOWED, sa,
        SecurityImpersonation, TokenPrimary, &newToken);

    if (!ret)
    {
        LOGE(TAG, "%s(%d) %s: DuplicateTokenEx() ret(%d) sourceToken(%p) error(%d)",
            __CALL_INFO__, ret, sourceToken, GetLastError());
    }

    LOGD_CALL(TAG, "end");

    return newToken;
}

BOOL ControllerMonitor::GetProcessHandle(const TCHAR* name, HANDLE* pHandle)
{
    LOGD_CALL(TAG, "start");

    HANDLE hSnapshot;
    HANDLE hProcess = NULL;
    PROCESSENTRY32 pe32;

    hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    pe32.dwSize = sizeof(PROCESSENTRY32);

    if (!Process32First(hSnapshot, &pe32))
    {
        CloseHandle(hSnapshot);
        return FALSE;
    }

    do
    {
        hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
        if (hProcess)
        {
            if (_tcsicmp(name, pe32.szExeFile) == 0)
            {
                *pHandle = hProcess;
                break;
            }

            CloseHandle(hProcess);
            hProcess = NULL;
        }

    } while (Process32Next(hSnapshot, &pe32));

    if (hProcess)
    {
        *pHandle = hProcess;
        return TRUE;
    }

    LOGD_CALL(TAG, "end");

    return FALSE;
}