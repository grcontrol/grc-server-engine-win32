/****************************** Module Header ******************************\
* Module Name:  SampleService.cpp
* Project:      CppWindowsService
* Copyright (c) Microsoft Corporation.
* 
* Provides a sample service class that derives from the service base class - 
* CServiceBase. The sample service logs the service start and stop 
* information to the Application event log, and shows how to run the main 
* function of the service in a thread pool worker thread.
* 
* This source is subject to the Microsoft Public License.
* See http://www.microsoft.com/en-us/openness/resources/licenses.aspx#MPL.
* All other rights reserved.
* 
* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
* EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

#pragma region Includes
#include "_service.h"
#include "ServiceEngine.h"
#include "SampleService.h"
#include "ThreadPool.h"
#include "CharsetUtil.h"
#include <string>
#include <gwlog.h>
#pragma endregion

static const char* TAG = "SRV";

BOOL CALLBACK EnumDesktopProc(LPTSTR desktop, LPARAM lParam)
{
    string desktopName = _UTF8(desktop);

    LOGD(TAG, "%s(%d) %s: Desktop: %s", __CALL_INFO__, desktopName.c_str());
    return TRUE;
}

void ListDesktops()
{
    HWINSTA hWinStation = GetProcessWindowStation();
    LOGI(TAG, "%s(%d) %s: hWinStation(%p)", __CALL_INFO__, hWinStation);
    EnumDesktops(hWinStation, EnumDesktopProc, 0);
}

CSampleService::CSampleService(PWSTR pszServiceName, 
                               BOOL fCanStop, 
                               BOOL fCanShutdown, 
                               BOOL fCanPauseContinue)
: CServiceBase(pszServiceName, fCanStop, fCanShutdown, fCanPauseContinue),
m_engine(new ServiceEngine)
{
    m_fStopping = FALSE;

    // Create a manual-reset event that is not signaled at first to indicate 
    // the stopped signal of the service.
    m_hStoppedEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (m_hStoppedEvent == NULL)
    {
        throw GetLastError();
    }
}


CSampleService::~CSampleService(void)
{
    if (m_hStoppedEvent)
    {
        CloseHandle(m_hStoppedEvent);
        m_hStoppedEvent = NULL;
    }
}


//
//   FUNCTION: CSampleService::OnStart(DWORD, LPWSTR *)
//
//   PURPOSE: The function is executed when a Start command is sent to the 
//   service by the SCM or when the operating system starts (for a service 
//   that starts automatically). It specifies actions to take when the 
//   service starts. In this code sample, OnStart logs a service-start 
//   message to the Application log, and queues the main service function for 
//   execution in a thread pool worker thread.
//
//   PARAMETERS:
//   * dwArgc   - number of command line arguments
//   * lpszArgv - array of command line arguments
//
//   NOTE: A service application is designed to be long running. Therefore, 
//   it usually polls or monitors something in the system. The monitoring is 
//   set up in the OnStart method. However, OnStart does not actually do the 
//   monitoring. The OnStart method must return to the operating system after 
//   the service's operation has begun. It must not loop forever or block. To 
//   set up a simple monitoring mechanism, one general solution is to create 
//   a timer in OnStart. The timer would then raise events in your code 
//   periodically, at which time your service could do its monitoring. The 
//   other solution is to spawn a new thread to perform the main service 
//   functions, which is demonstrated in this code sample.
//
void CSampleService::OnStart(DWORD dwArgc, LPWSTR *lpszArgv)
{
    gwlog_init("e:/tmp/service.txt", eLO_FILE, eLL_DEBUG);

    // Log a service start message to the Application log.
    WriteEventLogEntry(L"CppWindowsService in OnStart 1", 
        EVENTLOG_INFORMATION_TYPE);

    // Queue the main service function for execution in a worker thread.
    CThreadPool::QueueUserWorkItem(&CSampleService::ServiceWorkerThread, this);
}


//
//   FUNCTION: CSampleService::ServiceWorkerThread(void)
//
//   PURPOSE: The method performs the main function of the service. It runs 
//   on a thread pool worker thread.
//
void CSampleService::ServiceWorkerThread(void)
{
    ServiceEngine::Param param;
    param.port = SERVICE_PORT;
    param.configPath = "e:/tmp/config";
    m_engine->Open(param);
    m_engine->Start();
    m_engine->Close();

    // Signal the stopped event.
    SetEvent(m_hStoppedEvent);
}

//
//   FUNCTION: CSampleService::OnStop(void)
//
//   PURPOSE: The function is executed when a Stop command is sent to the 
//   service by SCM. It specifies actions to take when a service stops 
//   running. In this code sample, OnStop logs a service-stop message to the 
//   Application log, and waits for the finish of the main service function.
//
//   COMMENTS:
//   Be sure to periodically call ReportServiceStatus() with 
//   SERVICE_STOP_PENDING if the procedure is going to take long time. 
//
void CSampleService::OnStop()
{
    // Log a service stop message to the Application log.
    WriteEventLogEntry(L"CppWindowsService in OnStop", 
        EVENTLOG_INFORMATION_TYPE);

    // Indicate that the service is stopping and wait for the finish of the 
    // main service function (ServiceWorkerThread).
    m_fStopping = TRUE;
    m_engine->Stop();

    if (WaitForSingleObject(m_hStoppedEvent, INFINITE) != WAIT_OBJECT_0)
    {
        throw GetLastError();
    }

    gwlog_exit();
}

void CSampleService::OnSessionChange(
    DWORD dwEventType, WTSSESSION_NOTIFICATION* notification)
{
    LOGD_CALL(TAG, "start");

    const char* text[] = {
        "WTS_CONSOLE_CONNECT",
        "WTS_CONSOLE_DISCONNECT",
        "WTS_REMOTE_CONNECT",
        "WTS_REMOTE_DISCONNECT",
        "WTS_SESSION_LOGON",
        "WTS_SESSION_LOGOFF",
        "WTS_SESSION_LOCK",
        "WTS_SESSION_UNLOCK",
        "WTS_SESSION_REMOTE_CONTROL",
        "WTS_SESSION_CREATE",
        "WTS_SESSION_TERMINATE"
    };

    uint32_t index = dwEventType - 1;
    const char* type = "WTS_SESSION_UNKNOWN";

    if (index < sizeof(text) / sizeof(text[0]))
    {
        type = text[index];
    }

    DWORD dwSessionId = 0xFFFFFFFFu;
    if (notification)
    {
        dwSessionId = notification->dwSessionId;
    }

    DWORD dwActiveSessionId = WTSGetActiveConsoleSessionId();

    LOGI(TAG, "%s(%d) %s: %s sessionId(%ld) activeSessionId(%ld)",
        __CALL_INFO__, type, dwSessionId, dwActiveSessionId);

    if (dwActiveSessionId == (DWORD)-1 || dwSessionId == dwActiveSessionId)
    {
        int sessionState = -1;

        switch (dwEventType)
        {
        case WTS_CONSOLE_CONNECT:
        case WTS_CONSOLE_DISCONNECT:
        case WTS_SESSION_LOGON:
        case WTS_SESSION_LOGOFF:
        case WTS_SESSION_LOCK:
        case WTS_SESSION_UNLOCK:
            sessionState = dwEventType;
            break;
        }

        if (sessionState > 0)
        {
            m_engine->OnSessionChange(sessionState);
        }
    }

    ListDesktops();

    LOGD_CALL(TAG, "end");
}
