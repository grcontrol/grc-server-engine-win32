#include "Windows.h"
//const char* TAG = "GRCService";

TCHAR* __stdcall GetLastErrorMessage(int error)
{
    TCHAR* lpMsgBuf = NULL;

    int ret = FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        error,
        MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
        (LPTSTR)&lpMsgBuf,
        0, NULL);

    return lpMsgBuf;
}