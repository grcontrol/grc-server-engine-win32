#pragma once
#ifndef CONTROLLER_MONITOR_H
#define CONTROLLER_MONITOR_H

#include <Windows.h>
#include <tchar.h>
#include <stdint.h>

/**
 * Launch and manage a controller process which is used to control mouse and keyboard devices.
 */
class ControllerMonitor
{
public:
    ControllerMonitor();
    ~ControllerMonitor();

    int Start(TCHAR* filePath, int sessionState, bool elevateProcess);
    int Stop(uint32_t timeout_ms);

private:
    BOOL StartProcess(
        TCHAR* commandLine, TCHAR* desktop,
        HANDLE hToken, PROCESS_INFORMATION* pi);
    HANDLE GetToken(bool elevateProcess, LPSECURITY_ATTRIBUTES sa);
    HANDLE GetAdminToken(const TCHAR* fileName, LPSECURITY_ATTRIBUTES sa);
    HANDLE GetUserToken(LPSECURITY_ATTRIBUTES sa);
    HANDLE DuplicateToken(HANDLE sourceToken, LPSECURITY_ATTRIBUTES sa);
    int AdjustTokenPrivileges(HANDLE token);
    BOOL GetProcessHandle(const TCHAR* name, HANDLE* pHandle);
    TCHAR* GetDesktopName(int sessionChange);

private:
    PROCESS_INFORMATION m_pi;
};

#endif