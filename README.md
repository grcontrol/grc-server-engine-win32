# README #

Goldworm Remote Control Server Engine for win32

## OpenSources ##

* [libuv](https://github.com/libuv/libuv)
* [jsoncpp](https://github.com/open-source-parsers/jsoncpp)
* [googletest](https://https://github.com/google/googletest)
* [protocol-buffers](https://developers.google.com/protocol-buffers/)
* [doxygen](http://www.stack.nl/~dimitri/doxygen/index.html)

## Projects ##

* console
* grc-server-engine-win32
* gtest-md
* gwutils
* jsoncpp
* libuv
* service: Windows Service
* unittest: unittest using google test

## Protocol ##
### Basic packet header ###
| name | format | desc |
|------|--------|------|
| type | uint32_t | packet type |
| bodySize | uint32_t | body size in bytes |
| payload | byte | payload data |

## Features ##

## Programming Rules ##

## How to use doxygen ##
```
// Create a default configuration file simply.
$ doxygen -g <config-file>

// Generate the documentation
$ doxygen <config-file>
```
