# -*- coding: utf8 -*-
from socket import *
import struct
import json
import sys

def main():
    if len(sys.argv) != 2:
        print('[Usage] python3 {0} <file.json>'.format(sys.argv[0]))
        return 1

    text = readText(sys.argv[1])
    print('text: {0}'.format(text))

    sendText(text)

def readText(path):
    with open(path, 'r') as file:
        return file.read()    

def sendText(text):
    ip = '127.0.0.1'
    port = 1106

    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)

    data = str.encode(text)
    size = len(data)
    fmt = '>II{0}s'.format(len(data))
    packet = struct.pack(fmt, 1000, size, data)

    sock.sendto(packet, (ip, port))

    data, address = sock.recvfrom(1024)
    text = data[8:].decode('utf-8')
    print('recv data: {0}'.format(text))

    sock.close()

if __name__ == '__main__':
    main()
