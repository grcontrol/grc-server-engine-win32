﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string SERVICE_NAME = "GRCWindowsService";

        GRCServiceController m_sc = new GRCServiceController();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void onInitialized(object sender, EventArgs e)
        {
            Console.WriteLine("onInitialized");
            m_sc.Open(SERVICE_NAME);   
        }

        private void onClosed(object sender, EventArgs e)
        {
            Console.WriteLine("onClosed");
            m_sc.Close();
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_sc.Start();
            }
            catch (InvalidOperationException ioe)
            {
                Console.WriteLine(ioe.StackTrace);
            }
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_sc.Stop();
            }
            catch (InvalidOperationException ioe)
            {
                Console.WriteLine(ioe.StackTrace);
            }
        }
    }
}
