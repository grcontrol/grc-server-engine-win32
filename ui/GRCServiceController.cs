﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace ui
{
    class GRCServiceController
    {
        private ServiceController m_sc;

        public GRCServiceController()
        {
        }

        public int Open(string serviceName)
        {
            Close();

            ServiceController sc = new ServiceController(serviceName);
            Console.WriteLine("Status = " + sc.Status);
            Console.WriteLine("Can Pause and Continue = " + sc.CanPauseAndContinue);
            Console.WriteLine("Can ShutDown = " + sc.CanShutdown);
            Console.WriteLine("Can Stop = " + sc.CanStop);

            m_sc = sc;

            return 0;
        }

        public int Start()
        {
            if (m_sc == null)
            {
                return -1;
            }

            m_sc.Refresh();

            if (m_sc.Status.Equals(ServiceControllerStatus.Stopped))
            {
                m_sc.Start();
            }

            return 0;
        }

        public int Stop()
        {
            if (m_sc == null)
            {
                return -1;
            }

            m_sc.Refresh();

            if (m_sc.Status.Equals(ServiceControllerStatus.Running))
            {
                m_sc.Stop();

            }

            return 0;
        }

        public int Close()
        {
            if (m_sc != null)
            {
                m_sc.Close();
                m_sc = null;
            }

            return 0;
        }
    }
}
