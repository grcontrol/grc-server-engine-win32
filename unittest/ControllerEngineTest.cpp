#include "unittest.h"
#include <ControllerEngine.h>
#include <GRCPacket.h>

void CreateProcess(PROCESS_INFORMATION& pi)
{
    BOOL ret;
    STARTUPINFO si;

    ZeroMemory(&si, sizeof(STARTUPINFO));
    si.cb = sizeof(STARTUPINFO);
    si.lpDesktop = TEXT("winsta0\\default");

    TCHAR* path = _T("e:/goldworm/grc/grc-server-engine-win32/Debug/controller.exe");
    ret = CreateProcess(
        path,
        NULL,
        NULL,
        NULL,
        TRUE,
        //NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE,
        NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW,
        NULL,
        NULL,
        &si,
        &pi
    );
}

void CheckResponse(char* buf, int size)
{
    uint32_t type = htonl(*((int*)buf));
    ASSERT_EQ(ePT_JSON, type);
    uint32_t bodySize = htonl(*((int*)(buf+4)));
    ASSERT_TRUE(bodySize > 0);
    uint32_t session = htonl(*((int*)(buf+8)));
    ASSERT_EQ(0, session);

    Json::Reader reader;
    Json::Value root;

    bool success = reader.parse(buf + 12, buf + 12 + size, root);
    ASSERT_TRUE(success);
    ASSERT_TRUE(root["resultCode"].isInt());
    ASSERT_EQ(0, root["resultCode"].asInt());
    ASSERT_TRUE(root["resultDesc"].isString());
    ASSERT_STREQ("ok", root["resultDesc"].asString().c_str());
}

void TestKeyboard(SOCKET sock, struct sockaddr_in& saddr)
{
    int ret;
    Json::Value root;
    root["type"] = 6; // keyboard

    Json::Value keyCodes;
    keyCodes.append(VK_LWIN);
    keyCodes.append(VK_PAUSE);
    root["keyCodes"] = keyCodes;

    Json::StyledWriter writer;
    string resText = writer.write(root);

    GRCPacket* grcPacket = GRCPacket::CreateJsonPacket(resText);
    ASSERT_FALSE(grcPacket == NULL);

    int packetSize = grcPacket->GetDataSize();
    char* packet = grcPacket->GetData();
    ASSERT_FALSE(packet == NULL);
    ASSERT_EQ(resText.size(), grcPacket->GetBodySize());

    ret = sendto(sock, packet, packetSize, 0, (struct sockaddr*)&saddr, sizeof(saddr));
    ASSERT_TRUE(ret > 0);

    char buf[256];
    struct sockaddr_in from;
    int fromlen = sizeof(from);
    ret = recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr*)&from, &fromlen);
    ASSERT_TRUE(ret >= 0);

    CheckResponse(buf, ret);

    GRCPacket::Delete(grcPacket);
}

void TestMouse(SOCKET sock, struct sockaddr_in& saddr)
{
    int ret;
    Json::Value root;
    root["type"] = 5;
    root["x"] = 0;
    root["y"] = 0;
    root["flags"] = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE;

    Json::StyledWriter writer;
    string resText = writer.write(root);

    GRCPacket* grcPacket = GRCPacket::CreateJsonPacket(resText);
    ASSERT_FALSE(grcPacket == NULL);

    int packetSize = grcPacket->GetDataSize();
    char* packet = grcPacket->GetData();
    ASSERT_FALSE(packet == NULL);
    ASSERT_EQ(resText.size(), grcPacket->GetBodySize());

    ret = sendto(sock, packet, packetSize, 0, (struct sockaddr*)&saddr, sizeof(saddr));
    ASSERT_TRUE(ret > 0);

    char buf[256];
    struct sockaddr_in from;
    int fromlen = sizeof(from);
    ret = recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr*)&from, &fromlen);
    ASSERT_TRUE(ret >= 0);

    CheckResponse(buf, ret);

    GRCPacket::Delete(grcPacket);
}

static void TerminateController(SOCKET sock, struct sockaddr_in& saddr)
{
    int ret;
    Json::Value root;
    root["type"] = 100;
    root["command"] = "exit";

    Json::StyledWriter writer;
    string resText = writer.write(root);

    GRCPacket* grcPacket = GRCPacket::CreateJsonPacket(resText);
    ASSERT_FALSE(grcPacket == NULL);

    int packetSize = grcPacket->GetDataSize();
    char* packet = grcPacket->GetData();
    ASSERT_FALSE(packet == NULL);
    ASSERT_EQ(resText.size(), grcPacket->GetBodySize());

    ret = sendto(sock, packet, packetSize, 0, (struct sockaddr*)&saddr, sizeof(saddr));
    ASSERT_TRUE(ret > 0);

#if 0
    char buf[256];
    struct sockaddr_in from;
    int fromlen = sizeof(from);
    ret = recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr*)&from, &fromlen);
    ASSERT_TRUE(ret >= 0);
#endif

    GRCPacket::Delete(grcPacket);
}

TEST(ControllerEngine, Test)
{
    PROCESS_INFORMATION pi;
    ZeroMemory(&pi, sizeof(pi));
    pi.hProcess = INVALID_HANDLE_VALUE;
    CreateProcess(pi);

    Sleep(2000);

    SOCKET sock = socket(AF_INET, SOCK_DGRAM, 0);

    struct sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(1106);
    saddr.sin_addr.s_addr = 0x0100007f;

    TestKeyboard(sock, saddr);
    TestMouse(sock, saddr);
    TerminateController(sock, saddr);

    if (pi.hProcess != INVALID_HANDLE_VALUE)
    {
        DWORD dwRet = WaitForSingleObject(pi.hProcess, 5000);
        ASSERT_EQ(WAIT_OBJECT_0, dwRet);

        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }

    closesocket(sock);
}