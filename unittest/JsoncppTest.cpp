#include "unittest.h"
#include <json/json.h>
#include <string>

using namespace std;

TEST(JsoncppTest, Parse)
{
    const string text = "{ \"keyCodes\": [ 10, 20, 30, 40, 50 ] }";
    Json::Value root;
    Json::Reader reader;
    
    const char* beginDoc = text.data();
    bool ret = reader.parse(beginDoc, beginDoc + text.length(), root, false);
    ASSERT_TRUE(ret);

    const Json::Value& jsonKeyCodes = root["keyCodes"];
    const int size = jsonKeyCodes.size();
    ASSERT_EQ(5, size);

    const int keyCodes[] = { 10, 20, 30, 40, 50 };
    for (int i = 0; i < 5; i++)
    {
        ASSERT_EQ(keyCodes[i], jsonKeyCodes[i].asInt());
    }

    const Json::Value& empty = root["empty"];
    ASSERT_TRUE(empty.isNull());
}