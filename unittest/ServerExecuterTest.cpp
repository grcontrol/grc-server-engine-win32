#include "unittest.h"
#include <Packet.h>
#include <ServerExecuter.h>
#include <CharsetUtil.h>

TEST(ServerExecuter, Authenticate)
{
    Json::Value reqJson;
    Json::Value resJson;

    reqJson["type"] = eCT_SERVER;
    reqJson["subType"] = eSCS_AUTHENTICATE;
    reqJson["checksum"] = "d41d8cd98f00b204e9800998ecf8427e";

    ServerExecuter executer;
    int ret = executer.Run(reqJson, resJson);
    ASSERT_EQ(0, ret);
    ASSERT_TRUE(resJson["session"].isUInt());
    ASSERT_TRUE(resJson["mac"].isString());
    ASSERT_STREQ("aaaaaaaaaaaa", resJson["mac"].asString().c_str());
    ASSERT_FALSE(resJson["result"].isInt());
}

TEST(ServerExecuter, Detect)
{
    TCHAR computerName[MAX_COMPUTERNAME_LENGTH + 1];
    DWORD size = MAX_COMPUTERNAME_LENGTH + 1;
    if (!::GetComputerName(computerName, &size))
    {
        _tcscmp(computerName, _T("GoldwormRC"));
        size = _tcslen(computerName);
    }

    Json::Value reqJson;
    Json::Value resJson;

    reqJson["type"] = eCT_SERVER;
    reqJson["subType"] = eSCS_DETECTION;

    ServerExecuter executer;
    int ret = executer.Run(reqJson, resJson);
    ASSERT_EQ(0, ret);
    ASSERT_TRUE(resJson["name"].isString());
    ASSERT_STREQ(_UTF8(computerName).c_str(), resJson["name"].asString().c_str());
    ASSERT_TRUE(resJson["isLocked"].isBool());
    ASSERT_EQ(false, resJson["isLocked"].asBool());
    ASSERT_FALSE(resJson["result"].isInt());
}