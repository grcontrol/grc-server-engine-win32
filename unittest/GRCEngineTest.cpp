#include "unittest.h"
#include <grc.h>

TEST(GRCEngine, Test)
{
#if 1
	int ret;
	GRCEngine engine;

	ret = engine.Init();
	ASSERT_EQ(0, ret);

	GRCEngine::Param param;
	param.port = 1105;
	ret = engine.Open(param);
	ASSERT_EQ(0, ret);

	ret = engine.Start();
	ASSERT_EQ(0, ret);

    ret = engine.WaitForReady(3000);
    ASSERT_EQ(0, ret);

	ret = engine.Stop();
	ASSERT_EQ(0, ret);

	ret = engine.Close();
	ASSERT_EQ(0, ret);

	ret = engine.Exit();
	ASSERT_EQ(0, ret);
#endif
}