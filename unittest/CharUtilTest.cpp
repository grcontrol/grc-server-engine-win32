#include "unittest.h"
#include <CharUtil.h>

TEST(CharUtilTest, trim_ok)
{
    const char* sample = "   abcd  \n";
    char* text;
    char buf[256];

    sprintf_s(buf, sizeof(buf), "  \tabcd    \t\r\n");
    text = gw::ltrimA(buf);
    ASSERT_STREQ("abcd    \t\r\n", text);

    sprintf_s(buf, sizeof(buf), "  \tabcd    \t\r\n");
    text = gw::rtrimA(buf);
    ASSERT_STREQ("  \tabcd", text);

    sprintf_s(buf, sizeof(buf), "  \tabcd    \t\r\n");
    text = gw::trimA(buf);
    ASSERT_STREQ("abcd", text);
}

TEST(CharUtilTest, trim_error)
{
    char* text;
    char buf[256];

    buf[0] = '\0';
    text = gw::ltrimA(buf);
    ASSERT_STREQ("", text);

    buf[0] = '\0';
    text = gw::rtrimA(buf);
    ASSERT_STREQ("", text);

    buf[0] = '\0';
    text = gw::trimA(buf);
    ASSERT_STREQ("", text);
}