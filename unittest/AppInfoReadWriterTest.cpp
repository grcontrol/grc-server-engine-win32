#include "unittest.h"
#include "AppInfoConfReadWriter.h"
#include <CharsetUtil.h>

static const char* config =
"APP_NAME: ALShow\n"
"CLASS_NAME :\n"
"PATH_NAME: C:\\Program Files\\ESTsoft\\ALShow\\ALShow.exe\n"
"FULL_SCREEN : RETURN\n"
"PLAY : SPACE\n"
"STOP : S\n"
"JUMP_BACKWARD : CTRL + LEFT\n"
"JUMP_FORWARD : CTRL + RIGHT\n"
"NEXT : PAGEDOWN\n"
"PREV : PAGEUP\n"
"UP : UP\n"
"DOWN : DOWN\n"
"MUTE : CTRL + M\n"
"SUBTITLE : CTRL + H\n"
"LEFT : LEFT\n"
"RIGHT : RIGHT\n"
"CLOVER : CTRL + ENTER\n"
"HEART :\n"
"DIAMOND:\n"
"SPADE:\n";

TEST(AppInfoConfReadWriter, Load)
{
    int ret;
    vector<int> keyCodes;
    AppInfoConfReadWriter readWriter;

    int size = strlen(config);
    char* buf = (char*)malloc(size + 1);
    strcpy_s(buf, size + 1, config);

    AppInfo* appInfo = readWriter.Load(buf, size);
    ASSERT_TRUE(appInfo != NULL);

    ASSERT_STREQ("ALShow", _UTF8(appInfo->GetAppName()).c_str());
    ASSERT_STREQ("", _UTF8(appInfo->GetClassName()).c_str());
    ASSERT_STREQ("C:\\Program Files\\ESTsoft\\ALShow\\ALShow.exe", _UTF8(appInfo->GetPathName()).c_str());
    ASSERT_STREQ("ALShow.exe", _UTF8(appInfo->GetFileName()).c_str());
    ASSERT_STREQ("C:\\Program Files\\ESTsoft\\ALShow", _UTF8(appInfo->GetDir()).c_str());

    ret = appInfo->GetKeyCodes(eRC_FULL_SCREEN, keyCodes);
    ASSERT_EQ(0, ret);
    ASSERT_EQ(1, keyCodes.size());
    ASSERT_EQ(VK_RETURN, keyCodes[0]);

    ret = appInfo->GetKeyCodes(eRC_PLAY, keyCodes);
    ASSERT_EQ(0, ret);
    ASSERT_EQ(1, keyCodes.size());
    ASSERT_EQ(VK_SPACE, keyCodes[0]);

    ret = appInfo->GetKeyCodes(eRC_STOP, keyCodes);
    ASSERT_EQ(0, ret);
    ASSERT_EQ(1, keyCodes.size());
    ASSERT_EQ('S', keyCodes[0]);

    ret = appInfo->GetKeyCodes(eRC_JUMP_BACKWARD, keyCodes);
    ASSERT_EQ(0, ret);
    ASSERT_EQ(2, keyCodes.size());
    ASSERT_EQ(VK_CONTROL, keyCodes[0]);
    ASSERT_EQ(VK_LEFT, keyCodes[1]);

    ret = appInfo->GetKeyCodes(eRC_JUMP_FORWARD, keyCodes);
    ASSERT_EQ(0, ret);
    ASSERT_EQ(2, keyCodes.size());
    ASSERT_EQ(VK_CONTROL, keyCodes[0]);
    ASSERT_EQ(VK_RIGHT, keyCodes[1]);

    ret = appInfo->GetKeyCodes(eRC_NEXT, keyCodes);
    ASSERT_EQ(0, ret);
    ASSERT_EQ(1, keyCodes.size());
    ASSERT_EQ(VK_NEXT, keyCodes[0]);

    ret = appInfo->GetKeyCodes(eRC_PREV, keyCodes);
    ASSERT_EQ(0, ret);
    ASSERT_EQ(1, keyCodes.size());
    ASSERT_EQ(VK_PRIOR, keyCodes[0]);

    if (appInfo)
    {
        delete appInfo;
    }
    free(buf);
}

TEST(AppInfoConfReadWriter, Save)
{
    AppInfoConfReadWriter readWriter;
}