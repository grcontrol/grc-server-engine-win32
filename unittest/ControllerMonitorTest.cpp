#include "unittest.h"
#include <grc.h>
#include <ControllerMonitor.h>
#include <json/json.h>
#include <string>
#include <GRCPacket.h>

using namespace std;

static void TerminateController()
{
    SOCKET sock = socket(AF_INET, SOCK_DGRAM, 0);

    struct sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(1105);
    saddr.sin_addr.s_addr = 0x0100007f;

    int ret;
    Json::Value root;
    root["type"] = 100;
    root["command"] = "exit";

    Json::StyledWriter writer;
    string resText = writer.write(root);

    GRCPacket* grcPacket = GRCPacket::Create(ePT_JSON, resText.data(), resText.size());
    ASSERT_FALSE(grcPacket == NULL);
    int packetSize = grcPacket->GetDataSize();
    char* packet = grcPacket->GetData();
    ASSERT_TRUE(packet);
    ASSERT_TRUE(packetSize > 0);
    ASSERT_EQ(resText.size(), grcPacket->GetBodySize());

    ret = sendto(sock, packet, packetSize, 0, (struct sockaddr*)&saddr, sizeof(saddr));
    ASSERT_TRUE(ret > 0);

#if 1
    char buf[256];
    struct sockaddr_in from;
    int fromlen = sizeof(from);
    ret = recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr*)&from, &fromlen);
    ASSERT_TRUE(ret >= 0);
#endif

    GRCPacket::Delete(grcPacket);
}

TEST(ControllerMonitorTest, Run)
{
    int ret;
    ControllerMonitor monitor;

    bool elevateProcess = true;
    TCHAR* filePath = _T("E:/goldworm/grc/grc-server-engine-win32/Debug/controller.exe");

    ret = monitor.Start(filePath, WTS_SESSION_LOGON, elevateProcess);
    ASSERT_EQ(-1, ret);

    if (ret != 0)
    {
        // ERROR_ACCESS_DENIED
        ASSERT_EQ(18, GetLastError());
    }

#if 0
    elevateProcess = false;
    ret = monitor.Start(filePath, elevateProcess);
    ASSERT_EQ(0, ret);

    Sleep(3000);
    TerminateController();
#endif

    ret = monitor.Stop(3000);
    ASSERT_EQ(0, ret);
}