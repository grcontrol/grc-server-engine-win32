#include "unittest.h"
#include <uv.h>

TEST(LibUV, ConditionVariable)
{
    uv_mutex_t mutex;
    uv_cond_t cond;

    uv_mutex_init(&mutex);
    uv_cond_init(&cond);

    uv_cond_signal(&cond);

    int ret = uv_cond_timedwait(&cond, &mutex, 3000000000LL);
    ASSERT_EQ(UV_ETIMEDOUT, ret);

    uv_mutex_destroy(&mutex);
    uv_cond_destroy(&cond);
}