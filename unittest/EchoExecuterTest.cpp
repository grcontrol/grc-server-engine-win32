#include "unittest.h"
#include <EchoExecuter.h>

TEST(EchoExecuterTest, Run)
{
    EchoExecuter echoExecuter;

    int ret;
    Json::Value reqJson;
    Json::Value resJson;

    const time_t curTime = time(NULL);
    reqJson["echo"] = curTime;

    ASSERT_EQ(curTime, reqJson["echo"].asInt64());
    ASSERT_EQ(eCT_ECHO, echoExecuter.GetType());

    ret = echoExecuter.Run(reqJson, resJson);
    ASSERT_EQ(0, ret);
    ASSERT_TRUE(resJson["echo"].isInt64());
    ASSERT_EQ(curTime, resJson["echo"].asInt64());
    ASSERT_TRUE(reqJson == resJson);
}