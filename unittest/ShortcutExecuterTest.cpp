#include "unittest.h"
#include <ShortcutExecuter.h>

TEST(ShortcutExecuter, Run)
{
    ShortcutExecuter shortcutExecuter;

    int ret;
    Json::Value reqJson;
    Json::Value resJson;

    ret = shortcutExecuter.Run(reqJson, resJson);
    ASSERT_EQ(0, ret);
}