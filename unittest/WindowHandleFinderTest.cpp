#include "unittest.h"
#include <WindowHandleFinder.h>
#include <list>

TEST(WindowHandleFinderTest, Run)
{
    const TCHAR* pathName =
        _T("C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\Common7\\IDE\\devenv.exe");

    WindowHandleFinder finder;
    finder.SetPathName(pathName);

    std::list<HWND> windowHandles;
    HRESULT hr = finder.Run(windowHandles);
    ASSERT_EQ(S_OK, hr);
    ASSERT_TRUE(windowHandles.size() > 0);
}