#include "unittest.h"
#include "ServiceEngine.h"
#include "GRCPacket.h"
#include <Windows.h>

static const uint16_t SERVER_PORT = 1106;

DWORD WINAPI ThreadProc(void* data)
{
    int ret;
    ServiceEngine::Param param;
    param.port = SERVER_PORT;
    param.configPath = "e:/tmp/config";

    ServiceEngine* engine = (ServiceEngine*)data;

    ret = engine->Open(param);
    ret = engine->Start();
    ret = engine->Close();

    return 0;
}

static void CheckEchoRequest(uint16_t port)
{
    int ret;

    SOCKET sock = socket(AF_INET, SOCK_DGRAM, 0);
    ASSERT_TRUE(sock != INVALID_SOCKET);

    struct sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    saddr.sin_addr.s_addr = 0x0100007f;

    Json::Value request;
    request["type"] = eCT_ECHO;
    request["text"] = "echo";
    request["int"] = 1234;

    Json::StyledWriter writer;
    string resText = writer.write(request);

    GRCPacket* grcPacket =
        GRCPacket::Create(ePT_JSON, resText.data(), resText.size());
    ASSERT_FALSE(grcPacket == NULL);

    char* packet = grcPacket->GetData();
    ASSERT_FALSE(packet == NULL);

    int packetSize = grcPacket->GetDataSize();
    ASSERT_TRUE(packetSize > 0);
    ASSERT_EQ(resText.size(), grcPacket->GetBodySize());

    ret = sendto(sock, packet, packetSize, 0, (struct sockaddr*)&saddr, sizeof(saddr));
    ASSERT_TRUE(ret > 0);

    char buf[256];
    struct sockaddr_in from;
    int fromlen = sizeof(from);
    ret = recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr*)&from, &fromlen);
    ASSERT_TRUE(ret >= 0);

    uint32_t type = htonl(*((int*)buf));
    ASSERT_EQ(ePT_JSON, type);
    uint32_t bodySize = htonl(*((int*)(buf + 4)));
    ASSERT_EQ(bodySize, ret - sizeof(GRCPacket::Header));
    uint32_t session = htonl(*((int*)(buf + 8)));
    ASSERT_EQ(0, session);

    Json::Reader reader;
    Json::Value response;
    
    bool success = reader.parse(buf + 12, buf + ret, response);
    ASSERT_TRUE(success);
    ASSERT_TRUE(response["resultCode"].isInt());
    ASSERT_EQ(0, response["resultCode"].asInt());
    ASSERT_TRUE(response["resultDesc"].isString());
    ASSERT_STREQ("ok", response["resultDesc"].asString().c_str());
    ASSERT_STREQ(request["text"].asString().c_str(), response["text"].asString().c_str());
    ASSERT_EQ(request["int"].asInt(), response["int"].asInt());

    GRCPacket::Delete(grcPacket);
    ret = closesocket(sock);
    ASSERT_EQ(0, ret);
}

TEST(ServiceEngine, Run)
{
    DWORD dwRet;
    ServiceEngine* engine = new ServiceEngine();
    ASSERT_FALSE(engine == NULL);

    HANDLE hThread = CreateThread(
        NULL,
        0,
        ThreadProc,
        engine,
        0,
        NULL
    );
    ASSERT_FALSE(hThread == NULL);

    dwRet = WaitForSingleObject(hThread, 3000);
    ASSERT_TRUE(dwRet == WAIT_TIMEOUT);

    CheckEchoRequest(SERVER_PORT);

    int ret = engine->Stop();
    ASSERT_EQ(0, ret);

    dwRet = WaitForSingleObject(hThread, 3000);
    ASSERT_TRUE(dwRet == WAIT_OBJECT_0);

    delete engine;
}