#include "unittest.h"
#include <Packet.h>

TEST(PacketTokenizer, Run)
{
    PacketTokenizer packetTokenizer(4096);

    char buf[128];
    memset(buf, 0xFF, sizeof(buf));

    Packet::Header* header = (Packet::Header*)buf;
    header->type = htonl(1);
    header->session = htonl(1234);
    header->bodySize = htonl(4);

    int packetSize = sizeof(Packet::Header) + 4;
    int ret = packetTokenizer.Input(buf, packetSize);
    ASSERT_EQ(packetSize, ret);

    Packet packet;
    ret = packetTokenizer.GetNext(&packet);
    ASSERT_EQ(0, ret);

    ASSERT_EQ(1, packet.GetType());
    ASSERT_EQ(1234, packet.GetSession());

    int bodySize;
    const char* body = packet.GetBody(&bodySize);
    ASSERT_FALSE(body == NULL);
    ASSERT_EQ(packet.GetBodySize(), bodySize);
    ASSERT_EQ(0xFFFFFFFF, *((int*)body));

    packetTokenizer.Consume(packetSize);
    ret = packetTokenizer.GetNext(&packet);
    ASSERT_EQ(-1, ret);
}