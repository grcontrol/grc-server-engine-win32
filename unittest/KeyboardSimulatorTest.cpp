#include "unittest.h"
#include <Windows.h>
#include <vector>
#include <KeyboardSimulator.h>

TEST(KeyboardSimulator, Input)
{
    int ret;
    int keyCodes[4];
    keyCodes[0] = VK_LWIN;
    keyCodes[1] = VK_PAUSE;

#if 0
    ret = KeyboardSimulator::SendInputText(L"Hello World!!!");
    ASSERT_EQ(0, ret);
#endif // 0

    ret = KeyboardSimulator::SendInputKeyCodes(2, keyCodes);
    ASSERT_EQ(0, ret);

    Sleep(2000);

    keyCodes[0] = VK_LMENU;
    keyCodes[1] = VK_F4;
    ret = KeyboardSimulator::SendInputKeyCodes(2, keyCodes);
    ASSERT_EQ(0, ret);
}
