#include "unittest.h"
#include <FileBrowser.h>

TEST(FileBrowserTest, List)
{
    int ret;
    FileBrowser fileBrowser;
    Json::Value files;

    ret = fileBrowser.List(".", files);
    ASSERT_EQ(0, ret);

    cout << files.toStyledString() << endl;
}