#pragma once
#ifndef MOUSE_SIMULATOR_H
#define MOUSE_SIMULATOR_H

#include "_grc.h"

class MouseSimulator
{
public:
    MouseSimulator();
    ~MouseSimulator();

    static uint32_t SendInput(int32_t x, int32_t y, uint32_t flags);
};

#endif