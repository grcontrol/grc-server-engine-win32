#include "KeyboardSimulator.h"
#include <Windows.h>

/**
* Generate keyboard input event so that we can achieve virtual key input effect.
*
* @param count the number of keyCodes to simulate.
* @param keyCodes array containing keyCodes
* @return 0
*/
int KeyboardSimulator::SendInputKeyCodes(const int count, const int keyCodes[])
{
    const int MAX_SHORTCUT_KEY_COUNT = 4;

    int i;
    INPUT input[MAX_SHORTCUT_KEY_COUNT] = { 0 };
    int cbSize = sizeof(INPUT);
    uint32_t virtualKey;
    bool extended = false;

    for (i = 0; i < count; i++)
    {
        virtualKey = keyCodes[i] & 0xFFFF;

        input[i].type = INPUT_KEYBOARD;
        //input[i].ki.wScan = MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC);
        input[i].ki.wVk = (WORD)virtualKey;

        if (virtualKey == VK_SHIFT
            || virtualKey == VK_MENU
            || virtualKey == VK_CONTROL)
        {
            extended = true;
        }
    }

    if (extended)
    {
        input[count - 1].ki.dwFlags |= KEYEVENTF_EXTENDEDKEY;
    }

    // Simulate KeyDown event.
    SendInput(count, input, cbSize);

    // If this code omits, some korean input is skipped.
    Sleep(5);

    // Simulate KeyUp event.
    for (i = 0; i < count; i++)
    {
        input[i].ki.dwFlags = KEYEVENTF_KEYUP;
    }
    SendInput(count, input, cbSize);

    return 0;
}

/**
 * VK_PACKET mode
 * Simulate text input.
 *
 * @param text unicode format
 * @return 0
 */
int KeyboardSimulator::SendInputText(const std::wstring& text)
{
    int i;
    INPUT input;
    int cbSize = sizeof(INPUT);

    memset(&input, 0, cbSize);
    input.type = INPUT_KEYBOARD;

    const int size = (int)text.size();
    for (i = 0; i < size; i++)
    {
        input.ki.wScan = text[i];

        input.ki.dwFlags = KEYEVENTF_UNICODE;
        ::SendInput(1, &input, cbSize);

        input.ki.dwFlags |= KEYEVENTF_KEYUP;
        ::SendInput(1, &input, cbSize);
    }

    return 0;
}
