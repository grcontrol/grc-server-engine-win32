#include "MouseSimulator.h"
#include <Windows.h>

MouseSimulator::MouseSimulator()
{
}

MouseSimulator::~MouseSimulator()
{
}

/**
 * MOUSEEVENTF_MOVE        0x0001       // mouse move
 * MOUSEEVENTF_LEFTDOWN    0x0002          // left button down 
 * MOUSEEVENTF_LEFTUP      0x0004          // left button up 
 * MOUSEEVENTF_RIGHTDOWN   0x0008          // right button down 
 * MOUSEEVENTF_RIGHTUP     0x0010          // right button up 
 * MOUSEEVENTF_MIDDLEDOWN  0x0020          // middle button down 
 * MOUSEEVENTF_MIDDLEUP    0x0040          // middle button up 
 * MOUSEEVENTF_XDOWN       0x0080          // x button down 
 * MOUSEEVENTF_XUP         0x0100          // x button down 
 * MOUSEEVENTF_WHEEL                0x0800 // wheel button rolled 
 * MOUSEEVENTF_HWHEEL              0x01000 // hwheel button rolled 
 * MOUSEEVENTF_MOVE_NOCOALESCE      0x2000 // do not coalesce mouse moves 
 * MOUSEEVENTF_VIRTUALDESK          0x4000 // map to entire virtual desktop 
 * MOUSEEVENTF_ABSOLUTE             0x8000 // absolute move 
 */
uint32_t MouseSimulator::SendInput(int32_t x, int32_t y, uint32_t flags)
{
    LOGD(TAG, "%s(%d) %s: start x(%u) y(%u) flags(%X)", __CALL_INFO__, x, y, flags);

    INPUT input = { 0 };
    uint32_t mouseData = 0u;

    input.type = INPUT_MOUSE;

    if (flags & MOUSEEVENTF_WHEEL)
    {
        mouseData = WHEEL_DELTA * y;
        y = 0;
    }
#if 0
    else if (flags & MOUSEEVENTF_HWHEEL)
    {
        mouseData = WHEEL_DELTA * x;
        x = 0;
    }
#endif

    input.mi.dx = x;
    input.mi.dy = y;
    input.mi.dwFlags = flags;
    input.mi.mouseData = mouseData;

    uint32_t events = ::SendInput(1, &input, sizeof(INPUT));

    LOGD(TAG, "%s(%d) %s: end events(%u)", __CALL_INFO__, events);

    return events;
}