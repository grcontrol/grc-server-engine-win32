#pragma once
#include "Executer.h"

GRC_NAMESPACE_BEGIN

class ServerExecuter : public Executer
{
public:
    ServerExecuter();
    virtual ~ServerExecuter();

    // Inherited via Executer
    virtual COMMAND_TYPE GetType() override;
    virtual int Run(const Json::Value& reqJson, Json::Value& resJson) override;

private:
    int Authenticate(const Json::Value& reqJson, Json::Value& resJson);
    int Detect(const Json::Value& reqJson, Json::Value& resJson);

    bool IsLocked() const { return false; }
    bool CompareChecksum(const string& checksum) const;

private:
    // mac address represented by hexa string format.
    string m_macAddress;
};

GRC_NAMESPACE_END