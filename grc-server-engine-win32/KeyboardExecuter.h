#pragma once
#ifndef KEYBOARD_EXECUTER_H
#define KEYBOARD_EXECUTER_H

#include "Executer.h"

class KeyboardExecuter : public Executer
{
public:
	KeyboardExecuter();
	virtual ~KeyboardExecuter();

    // Inherited via Executer
    virtual COMMAND_TYPE GetType() override;

    virtual int Run(const Json::Value& reqJson, Json::Value& resJson) override;
};

#endif