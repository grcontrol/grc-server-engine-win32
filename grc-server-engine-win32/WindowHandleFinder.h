#pragma once
#ifndef WINDOW_HANDLE_FINDER_H
#define WINDOW_HANDLE_FINDER_H

#include "_grc.h"
#include <list>

enum WINDOW_STYLE_MODE
{
	eWSM_INCLUSIVE,
	eWSM_EXCLUSIVE,
};

/**
 * Find Window handle with PathName and ClassName.
 */
class WindowHandleFinder
{
public:
	WindowHandleFinder(void);
	~WindowHandleFinder(void);

	void SetPathName(const TCHAR* szPathName);
	void SetClassName(const TCHAR* szClassName);
	void SetWindowStyle(const DWORD dwStyle, const int nMode);
	void SetWindowExStyle(const DWORD dwExStyle, const int nMode);

	HRESULT Run(list<HWND>& hWndList);
	void Clear();

private:
    static BOOL CALLBACK EnumWindowsFunc(HWND hWnd, LPARAM lParam);

private:
	typedef list<HWND> HWND_LIST;

	HWND m_hWnd;

	/**
	 * WindowStyle which CWnd has to have.
	 */
	DWORD m_dwWindowStyle[2];
	DWORD m_dwWindowExStyle[2];

	const TCHAR* m_szPathName;
	const TCHAR* m_szClassName;

	HWND_LIST m_hWndList;

	BOOL Check(HWND* phWnd);
	HRESULT EnumWindowProc(HWND hWnd);
};

#endif