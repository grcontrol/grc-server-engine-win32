#pragma once
#ifndef COMMAND_EXECUTER_STORAGE_H
#define COMMAND_EXECUTER_STORAGE_H

#include "_grc.h"
#include <unordered_map>
#include <memory>

class Executer;

/**
 *
 */
class ExecuterStorage
{
public:
	ExecuterStorage();
	~ExecuterStorage();

    int Add(shared_ptr<Executer> executer);
    shared_ptr<Executer> Get(const int commandType);

private:
	typedef unordered_map<int, shared_ptr<Executer>> EXECUTER_MAP;
	EXECUTER_MAP m_executers;
};

#endif