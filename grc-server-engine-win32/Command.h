#pragma once
#ifndef __COMMAND_H__
#define __COMMAND_H__

typedef struct _SCommandHeader
{
	int nType;
	int nLength;
	int nSession;
} SCommandHeader;

typedef struct _SServerCommand
{
	SCommandHeader hdr;

	int nSubType;
	char checkSum[16];
} SServerCommand;

typedef struct _SServerResponse
{
	SCommandHeader hdr;

	int nSubType;
	int nResult;
	char pData[1];
	
} SServerResponse;

typedef struct _SKeyboardCommand
{
	SCommandHeader hdr;

	int nKeyCount;
	int pKeyCodes[1];
} SKeyboardCommand;

typedef struct _SMouseCommand
{
	SCommandHeader hdr;

	int nAction;
	int x;
	int y;
} SMouseCommand;

typedef struct _SShortcutCommand
{
	SCommandHeader hdr;

	int nSubType;
	int nAppID;
	int nKeyStatus;
	int nShortcut;
} SShortcutCommand;

typedef struct _SShortcutTitleCommand
{
	SCommandHeader hdr;

	int nSubType;
	int nAppID;
} SShortcutTitleCommand;

typedef struct _SShortcutOpenCommand
{
	SCommandHeader hdr;
	int nSubType;
	int nAppID;
	int nLength; // textLength
	int nSize; // byteSize
	char pData[1]; // UTF-8 Text Data
} SShortcutOpenCommand;

typedef struct _SShortcutAppListResponse
{
	SCommandHeader hdr;

	int nSubType;
	int nAppCount;
	char pData[1];
} SShortcutAppListResponse;

typedef struct _SShortCommandHeader
{
	SCommandHeader hdr;
	int nSubType;
} SShortCommandHeader;

typedef struct _STextCommand
{
	SCommandHeader hdr;
	int nSubType;
	int nLength; // textLength
	int nSize; // byteSize
	char pData[1]; // UTF-8 Text Data
} STextCommand;

// SystemCommand ==========================================
typedef struct _SSystemCommandHeader
{
	SCommandHeader hdr;
	int nSubType;
} SSystemCommandHeader;

typedef struct _SSystemCommandSetVolume
{
	SCommandHeader hdr;
	int nSubType;
	int nVolume;
} SSystemCommandSetVolume;

typedef struct _SSystemResponseGetVolume
{
	SCommandHeader hdr;
	int nSubType;
	int nResult;
	int nVolume;
	int nMin;
	int nMax;
} SSystemResponseGetVolume;

typedef struct _SSystemCommandNonSleepingMode
{
	SCommandHeader hdr;
	int nSubType;
	int bNonSleepingMode;
} SSystemCommandNonSleepingMode, SSystemResponseNonSleepingMode;

// FileBrowseCommand =====================================
typedef struct _SFileBrowseCommandHeader
{
	SCommandHeader hdr;
	int nSubType;
} SFileBrowseCommandHeader;

typedef struct _SFileBrowseCommand
{
	SCommandHeader hdr;
	int nSubType;
	int nSize; // byteSize
	char pData[1]; // UTF-8 Text Data
} SFileBrowseCommand;

typedef struct _SFileBrowseResponse
{
	SCommandHeader hdr;
	int nSubType;

	char pData[1];
} SFileBrowseResponse;

typedef struct _SFileBrowseResponseLIST
{
	SCommandHeader hdr;
	int nSubType;
	short nFlag;
	short nCount;

	char pData[1];
} SFileBrowseResponseLIST;

typedef struct _SFileBrowseResponseCWD
{
	SCommandHeader hdr;
	int nSubType;

	int nTextSize;
	char pData[1];
} SFileBrowseResponseCWD;

#endif // __COMMAND_H__