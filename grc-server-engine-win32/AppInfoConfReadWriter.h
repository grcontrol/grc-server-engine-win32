#pragma once
#include "AppInfo.h"
#include "AppInfoReadWriter.h"

GRC_NAMESPACE_BEGIN

class AppInfoConfReadWriter : public AppInfoReadWriter
{
public:
    AppInfoConfReadWriter();
    virtual ~AppInfoConfReadWriter();

    // Inherited via AppInfoReadWriter
    virtual AppInfo* Load(const wstring& path) override;
    AppInfo* Load(char* buf, int size);
    virtual int Save(const wstring& path) override;

private:
    int Save(FILE* fp);
    int ConvertToKeyCodes(char* buf, KeyCodes& keyCodes);
    int GetVirtualKeyCode(const char* keyName) const;
};

GRC_NAMESPACE_END