#include "KeyboardExecuter.h"
#include "KeyboardSimulator.h"

KeyboardExecuter::KeyboardExecuter()
{
}

KeyboardExecuter::~KeyboardExecuter()
{
}

COMMAND_TYPE KeyboardExecuter::GetType()
{
    return eCT_KEYBOARD;
}

/**
 * @code
{
    "keyCodes": [20, 30, 96]
}
 * @endcode
 * @param reqJson json object
 * @return 0 (success) <0 (failure)
 */
int KeyboardExecuter::Run(const Json::Value& reqJson, Json::Value& resJson)
{
    LOGD_CALL(TAG, "start");

    const Json::Value& jsonKeyCodes = reqJson["keyCodes"];
    const int size = (int)jsonKeyCodes.size();

    static const int ARRAY_SIZE = 32;
    int keyCodes[ARRAY_SIZE];

    if (size < 1 || size > ARRAY_SIZE)
    {
        return -1;
    }

    for (int i = 0; i < size; i++)
    {
        keyCodes[i] = jsonKeyCodes[i].asInt();
    }

    KeyboardSimulator::SendInputKeyCodes(size, keyCodes);

    LOGD_CALL(TAG, "end");

    return 0;
}
