#include "UDPServer.h"
#include "GRCEngine.h"
#include "Packet.h"
#include "GRCPacket.h"

GRC_NAMESPACE_BEGIN

UDPServer::UDPServer() :
    m_grcEngine(NULL)
    ,m_loop(NULL)
    ,m_server(NULL)
    ,m_packetTokenizer(new PacketTokenizer(4096))
{
}

UDPServer::~UDPServer()
{
}

/**
 * UDP 통신을 위한 서버 소켓을 준비한다.
 *
 * @param param: UDP 서버 소켓 정보를 담고 있는 구조체
 * @return 0: success, <0: error
 */
int UDPServer::Open(const Param& param)
{
	LOGD_CALL(TAG, "start");

	int ret;
	const uint16_t port = param.port;
	m_loop = param.loop;
    m_grcEngine = (GRCEngine*)param.loop->data;

	uv_udp_t* udp = (uv_udp_t*)malloc(sizeof(uv_udp_t));
	ret = uv_udp_init(m_loop, udp);
    udp->data = this;

	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);

	ret = uv_udp_bind(udp, (const sockaddr*)&addr, UV_UDP_REUSEADDR);

	m_server = udp;

	LOGD_CALL(TAG, "end");
	return 0;
}

int UDPServer::Start()
{
	LOGD_CALL(TAG, "start");

	int ret = uv_udp_recv_start(m_server, ::OnAlloc, UDPServer::OnRecv);

	LOGD_CALL(TAG, "end");
	return 0;
}

int UDPServer::Stop()
{
	LOGD_CALL(TAG, "start");

	uv_udp_recv_stop(m_server);

	LOGD_CALL(TAG, "end");
	return 0;
}

int UDPServer::Close()
{
	LOGD_CALL(TAG, "start");

	uv_close((uv_handle_t*)m_server, UDPServer::OnClose);
	m_server = NULL;

	LOGD_CALL(TAG, "end");
	return 0;
}

void UDPServer::OnClose(uv_handle_t* handle)
{
    LOGD_CALL(TAG, "start");

    if (handle)
    {
        UDPServer* udpServer = (UDPServer*)handle->data;
        free(udpServer->m_server);
        udpServer->m_server = NULL;
    }

    LOGD_CALL(TAG, "end");
}

/**
 * Data has been received.
 */
void UDPServer::OnRecv(
	uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
	const struct sockaddr* addr, unsigned flags)
{
	LOGD_CALL(TAG, "start");

    UDPServer* udpServer = (UDPServer*)handle->data;
    if (udpServer)
    {
        udpServer->_OnRecv(handle, nread, buf, addr, flags);
    }

	LOGD_CALL(TAG, "end");
}

/**
 * Packet data has been received and passed to GRCEngine.
 */
void UDPServer::_OnRecv(
    uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
    const struct sockaddr* addr, unsigned flags)
{
    LOGD_CALL(TAG, "start");

    int ret;
    Packet packet;

    m_packetTokenizer->Input(buf->base, (int)nread);

    while (1)
    {
        ret = m_packetTokenizer->GetNext(&packet);
        if (ret < 0)
        {
            break;
        }

        m_callback->OnRecvPacket(addr, &packet);
        m_packetTokenizer->Consume(packet.GetPacketSize());
    }
    
    LOGD_CALL(TAG, "end");
}

#if 0
/**
* Process a recevied packet from each servers such as udpServer, tcpServer and so on.
* According to the packet type, Use the suitable packet body data parser.
*
* @param packet request data from client in binary packet format defined by grc.
* @param addr client network address
*/
void UDPServer::OnRecvPacket(Packet* packet, const struct sockaddr* addr)
{
    LOGD(TAG, "%s(%d) %s: start packet type(%u) size(%u)",
        __CALL_INFO__, packet->GetType(), packet->GetBodySize());

    Json::Value resJson; // response message to send to the client.

    Execute(packet, resJson);
    Response(addr, resJson);

    LOGD_CALL(TAG, "end");
}
#endif

#if 0
/**
 * Execute the request from a client.
 *
 * @param packet request data from a client
 * @param resJson result for the request in json format.
 */
int UDPServer::Execute(Packet* packet, Json::Value& resJson)
{
    Json::Value reqJson; // request from a client
    Json::Reader reader; // json parser

    int bodySize;
    const char* data = packet->GetBody(&bodySize);

    int ret;
    bool success = reader.parse(data, data + bodySize, reqJson, false);
    if (success)
    {
        ret = m_grcEngine->ExecuteRequest(reqJson, resJson);
    }
    else
    {
        ret = GRC_INVALID_JSON;
    }

    resJson["resultCode"] = ret;
    resJson["resultDesc"] = GetErrorDesc(ret);

    return 0;
}
#endif

/**
 * Send the result for request to the client indicated by addr.
 *
 * @param addr the network information of the request client
 * @param resJson response data to send
 */
int UDPServer::Response(const struct sockaddr* addr, const Json::Value& resJson)
{
    LOGD_CALL(TAG, "start");

#if _DEBUG
    Json::StyledWriter writer;
#else
    Json::FastWriter writer;
#endif

    const string resText = writer.write(resJson);

    GRCPacket* grcPacket =
        GRCPacket::Create(ePT_JSON, resText.c_str(), resText.length());

    uv_buf_t* buf = (uv_buf_t*)malloc(sizeof(uv_buf_t));
    buf->base = grcPacket->GetData();
    buf->len = grcPacket->GetDataSize();
    grcPacket->SetUserData(buf);

    uv_udp_send_t* req = (uv_udp_send_t*)malloc(sizeof(uv_udp_send_t));
    req->data = grcPacket;
    
    int ret = uv_udp_send(req, m_server, buf, 1, addr, OnSend);

    // Is it fine to free packet memory at this time?
    //free(packet);

    LOGD(TAG, "%s(%d) %s: end ret(%d)", __CALL_INFO__, ret);

    return ret;
}

/**
 * callback for uv_udp_send()
 * Free the memory allocated to req.
 */
void UDPServer::OnSend(uv_udp_send_t* req, int status)
{
    LOGD(TAG, "%s(%d) %s: start status(%d)", __CALL_INFO__, status);

    GRCPacket* grcPacket = (GRCPacket*)req->data;

    uv_buf_t* buf = (uv_buf_t*)grcPacket->GetUserData();
    //free(buf->base);
    free(buf);
    free(req);

    GRCPacket::Delete(grcPacket);

    LOGD_CALL(TAG, "end");
}

GRC_NAMESPACE_END