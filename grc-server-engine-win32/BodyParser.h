#pragma once
#ifndef BODY_PARSER_H
#define BODY_PARSER_H

#include "_grc.h"
#include <json/json.h>

GRC_NAMESPACE_BEGIN

class Packet;

class BodyParser
{
public:
    BodyParser();
    ~BodyParser();

    static int Parse(Packet* packet, Json::Value& reqJson);

private:
    static int ParseJson(Packet* packet, Json::Value& reqJson);
    static int ParseBinary(Packet* packet, Json::Value& reqJson);
};

GRC_NAMESPACE_END

#endif