#include "ExecuterStorage.h"
#include "EchoExecuter.h"
#include "KeyboardExecuter.h"
#include "MouseExecuter.h"
#include "SystemExecuter.h"

ExecuterStorage::ExecuterStorage()
{
    m_executers[eCT_ECHO] =
        shared_ptr<Executer>(new EchoExecuter());
    m_executers[eCT_KEYBOARD] =
        shared_ptr<Executer>(new KeyboardExecuter());
    m_executers[eCT_MOUSE] =
        shared_ptr<Executer>(new MouseExecuter());
    m_executers[eCT_SYSTEM] =
        shared_ptr<Executer>(new SystemExecuter());
}

ExecuterStorage::~ExecuterStorage()
{
}

int ExecuterStorage::Add(shared_ptr<Executer> executer)
{
    m_executers[executer->GetType()] = executer;
    return 0;
}

shared_ptr<Executer> ExecuterStorage::Get(const int commandType)
{
	LOGD_CALL(TAG, "start");

	shared_ptr<Executer> executer;

	auto it = m_executers.find(commandType);
	if (it != m_executers.end())
	{
        executer = it->second;
	}

	LOGD_CALL(TAG, "end");

	return executer;
}
