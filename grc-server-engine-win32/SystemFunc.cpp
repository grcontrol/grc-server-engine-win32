#include "SystemFunc.h"
#include <PowrProf.h>

int __stdcall ShutdownSystem(bool reboot)
{
    BOOL bRet;
    HANDLE hToken;
    TOKEN_PRIVILEGES tkp;

    // Get a token for this process. 
    if (!OpenProcessToken(GetCurrentProcess(),
        TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
        return -1;

    // Get the LUID for the shutdown privilege. 
    bRet = LookupPrivilegeValue(
        NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);

    tkp.PrivilegeCount = 1;  // one privilege to set    
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    // Get the shutdown privilege for this process. 
    AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);

    if (GetLastError() != ERROR_SUCCESS)
    {
        return -1;
    }

    uint32_t uFlags;

    if (reboot)
    {
        uFlags = EWX_REBOOT;
    }
    else
    {
        uFlags = EWX_POWEROFF | EWX_SHUTDOWN;
    }

#if 1
    // Shut down the system and force all applications to close. 
    if (!ExitWindowsEx(uFlags,
        SHTDN_REASON_MAJOR_OPERATINGSYSTEM |
        SHTDN_REASON_MINOR_UPGRADE |
        SHTDN_REASON_FLAG_PLANNED))
    {
        return -1;
    }
#else
    bRet = InitiateSystemShutdown(NULL, NULL, 0, TRUE, FALSE);
#endif

    return bRet ? 0 : -1;
}

int __stdcall SuspendSystem(bool hibernate)
{
    BOOL bRet;
    HANDLE hToken;
    TOKEN_PRIVILEGES tkp;

    // Get a token for this process. 
    if (!OpenProcessToken(GetCurrentProcess(),
        TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
        return -1;

    // Get the LUID for the shutdown privilege. 
    bRet = LookupPrivilegeValue(
        NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);

    tkp.PrivilegeCount = 1;  // one privilege to set    
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    // Get the shutdown privilege for this process. 
    AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);

    if (GetLastError() != ERROR_SUCCESS)
    {
        return -1;
    }

    /**
    * To use suspend mode, make sure that bWakeupEventsDisabled is set to TRUE.
    * More verfication is needed.
    */
    SetSuspendState((BOOLEAN)hibernate, FALSE, TRUE);

    return 0;
}

int __stdcall SetNonSleepingMode(bool on)
{
    EXECUTION_STATE esOldFlags;
    EXECUTION_STATE esNewFlags = ES_CONTINUOUS;

    if (on)
    {
        esNewFlags |= ES_SYSTEM_REQUIRED | ES_DISPLAY_REQUIRED;
    }

    esOldFlags = ::SetThreadExecutionState(esNewFlags);

    return esOldFlags ? 0 : -1;
}

int __stdcall LogOff()
{
    BOOL ret = ExitWindowsEx(EWX_LOGOFF, SHTDN_REASON_MAJOR_APPLICATION);
    return ret ? 0 : -1;
}

int __stdcall TurnOffMonitor(HWND hWnd)
{
    LOGD_CALL(TAG, "start");

    //const int MONITOR_ON = -1;
    //const int MONITOR_STANBY = 1;
    const int MONITOR_OFF = 2;

    hWnd = ::GetDesktopWindow();
    BOOL ret = ::PostMessage(hWnd, WM_SYSCOMMAND, SC_MONITORPOWER, MONITOR_OFF);

    return ret ? 0 : -1;
}

int __stdcall LockScreen()
{
    BOOL ret = LockWorkStation();
    return ret ? 0 : -1;
}