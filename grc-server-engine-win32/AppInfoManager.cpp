#include "StdAfx.h"
#include <map>
#include "AppInfo.h"
#include "AppInfoManager.h"

CAppInfoManager::CAppInfoManager(void)
{
}

CAppInfoManager::~CAppInfoManager(void)
{
	Clear();
}

int CAppInfoManager::GetCount() const
{
	return m_AppInfoMap.size();
}

/**
 * Reloading is also considered in this method.
 */
HRESULT CAppInfoManager::Load(const TCHAR* szPathName)
{
	HRESULT hr;
	CFileFind finder;
	FILE* fp;

	int i = 0;
	CAppInfo* pAppInfo = NULL;

	hr = Clear();

	CString strPathName;
	strPathName.Format(_T("%s\\*.grc"), szPathName);

	BOOL bFound = finder.FindFile(strPathName);

	while(bFound)
	{
		bFound = finder.FindNextFile();
		strPathName = finder.GetFilePath();

		fp = _tfopen(strPathName, _T("rb"));
		if(fp == NULL)
		{
			continue;
		}

		if(pAppInfo == NULL)
		{
			pAppInfo = new CAppInfo();
		}

		hr = pAppInfo->Load(fp);
		if(SUCCEEDED(hr))
		{
			m_AppInfoMap[i] = pAppInfo;
			pAppInfo = NULL;
		}

		fclose(fp);
		i++;
	}

	if(pAppInfo)
	{
		delete pAppInfo;
	}

	return S_OK;
}

CAppInfo* CAppInfoManager::GetByID(int nID)
{
	return GetByIndex(nID);
}

CAppInfo* CAppInfoManager::GetByIndex(int nIndex)
{
	APP_INFO_MAP::iterator itr = m_AppInfoMap.begin();
	int nSize = m_AppInfoMap.size();

	if(nIndex < 0 || nIndex >= nSize)
	{
		return NULL;
	}

	for(int i=0; i<nIndex; i++, itr++);

	return itr->second;
}

HRESULT CAppInfoManager::Clear()
{
	CAppInfo* pAppInfo;
	APP_INFO_MAP::iterator itr = m_AppInfoMap.begin();
	APP_INFO_MAP::iterator itrEnd = m_AppInfoMap.end();

	while(itr != itrEnd)
	{
		pAppInfo = itr->second;
		pAppInfo->Clear();

		delete pAppInfo;

		itr++;
	}

	m_AppInfoMap.clear();

	return S_OK;
}