#ifdef WIN32

#include <WinSock2.h>

#else

#include <sys/socket.h>
#include <netinet/in.h>

#endif

#include "Packet.h"
#include <DynamicBuffer.h>
#include <gwlog.h>

GRC_NAMESPACE_BEGIN

PacketTokenizer::PacketTokenizer(const int bufSize) :
m_pBuffer(new DynamicBuffer(bufSize))
{
}

PacketTokenizer::~PacketTokenizer(void)
{
}

int PacketTokenizer::Input(const char* pBuf, const int nDataSize)
{
	int nRet = m_pBuffer->Write(pBuf, nDataSize);
	return nRet;
}

int PacketTokenizer::GetNext(Packet* pPacket)
{
	const int nDataSize = m_pBuffer->GetDataSize();
    const int headerSize = sizeof(Packet::Header);

	if(nDataSize < headerSize)
	{
		return -1;
	}

	const char* pBuffer = m_pBuffer->GetBuffer();
	const Packet::Header* pHeader = reinterpret_cast<const Packet::Header*>(pBuffer);

	uint32_t type = ntohl(pHeader->type);
	uint32_t bodySize = ntohl(pHeader->bodySize);
    uint32_t session = ntohl(pHeader->session);
	const int packetSize = headerSize + bodySize;

	if(nDataSize < packetSize)
	{
		return -1;
	}

	pPacket->Clear();
	pPacket->SetType(type);
    pPacket->SetSession(session);
	pPacket->SetBody(pBuffer + headerSize, bodySize);

	return 0;
}

int PacketTokenizer::Consume(const int nSize)
{
	return m_pBuffer->Consume(nSize);
}

int PacketTokenizer::Clear()
{
	m_pBuffer->Clear();
	return 0;
}

GRC_NAMESPACE_END