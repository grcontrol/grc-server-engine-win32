#include "BodyParser.h"
#include "Packet.h"
#include "BinaryBodyConverter.h"

GRC_NAMESPACE_BEGIN

BodyParser::BodyParser()
{
}

BodyParser::~BodyParser()
{
}

int BodyParser::Parse(Packet* packet, Json::Value& reqJson)
{
    uint32_t type = packet->GetType();
    if (type == ePT_JSON)
    {
        return ParseJson(packet, reqJson);
    }
    else
    {
        return ParseBinary(packet, reqJson);
    }
}

int BodyParser::ParseJson(Packet* packet, Json::Value& reqJson)
{
    Json::Reader reader; // json parser

    int bodySize;
    const char* data = packet->GetBody(&bodySize);

    bool success = reader.parse(data, data + bodySize, reqJson, false);
    return success ? GRC_OK : GRC_INVALID_JSON;
}

int BodyParser::ParseBinary(Packet* packet, Json::Value& reqJson)
{
    return BinaryBodyConverter::BinToJson(packet, reqJson);
}

GRC_NAMESPACE_END