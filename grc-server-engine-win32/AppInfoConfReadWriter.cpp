#include "AppInfoConfReadWriter.h"
#include <PathUtil.h>
#include <StringUtil.h>
#include <CharUtil.h>

GRC_NAMESPACE_BEGIN

static int GetKeyCount()
{
    return CONFIG_KEY_COUNT;
}

static int GetConfigKey(const char* szKey)
{
    int i;
    int nCount = GetKeyCount();

    for (i = 0; i<nCount; i++)
    {
        if (strcmp(szKey, SZ_CONFIG_KEY_NAME[i]) == 0)
        {
            return i;
        }
    }

    return -1;
}

static const char* GetKey(int nKey)
{
    return SZ_CONFIG_KEY_NAME[nKey];
}

AppInfoConfReadWriter::AppInfoConfReadWriter()
{
}

AppInfoConfReadWriter::~AppInfoConfReadWriter()
{
}

AppInfo* AppInfoConfReadWriter::Load(const wstring& path)
{
    return nullptr;
}

AppInfo* AppInfoConfReadWriter::Load(char* buf, int size)
{
    int ret;
    char* context = NULL;
    const char* delimiter = "\r\n";
    char* token = strtok_s(buf, delimiter, &context);

    char* key;
    char* value;
    vector<int> keyCodes;
    AppInfo* appInfo = new AppInfo();

    while (token)
    {
        ret = gw::parseKeyValueLine(token, ':', &key, &value);
        if (ret < 0)
        {
            break;
        }

        key = gw::trimA(key);
        value = gw::trimA(value);

        uint32_t configKey = GetConfigKey(key);
        switch (configKey)
        {
        case eAPP_NAME:
            appInfo->SetAppName(value);
            break;
        case eCLASS_NAME:
            appInfo->SetClassName(value);
            break;
        case ePATH_NAME:
            appInfo->SetPathName(value);
            break;

        case eFULL_SCREEN:
        case ePLAY:
        case ePAUSE:
        case eSTOP:
        case eJUMP_BACKWARD:
        case eJUMP_FORWARD:
        case eNEXT:
        case ePREV:
        case eUP:
        case eDOWN:
        case eMUTE:
        case eSUBTITLE:
        case eLEFT:
        case eRIGHT:
        case eCLOVER:
        case eHEART:
        case eDIAMOND:
        case eSPADE:
        case eSUBTITLE_BACKWARD:
        case eSUBTITLE_RESET:
        case eSUBTITLE_FORWARD:
        case eAUDIO_BACKWARD:
        case eAUDIO_RESET:
        case eAUDIO_CHANGE:
        case eAUDIO_FORWARD:
        case eCIRCLE:
        case eCROSS:
        case eRECTANGLE:
        case eTRIANGLE:
            ret = ConvertToKeyCodes(value, keyCodes);
            if (ret == 0)
            {
                int shortcut = CONFIG_KEY_COMMAND_TABLE[configKey];
                appInfo->SetKeyCodes(shortcut, keyCodes);
            }
            break;
        default:
            break;
        }

        token = strtok_s(NULL, delimiter, &context);
    }

    if (ret < 0)
    {
        delete appInfo;
        appInfo = NULL;
    }

    return appInfo;
}

int AppInfoConfReadWriter::Save(const wstring& path)
{
    return 0;
}

int AppInfoConfReadWriter::ConvertToKeyCodes(char* buf, KeyCodes& keyCodes)
{
    char* context = NULL;
    const char* delimiter = "+ ";

    keyCodes.clear();

    char* token = strtok_s(buf, delimiter, &context);
    while (token)
    {
        // strToken contains one key's name.
        int virtualKeyCode = GetVirtualKeyCode(token);
        if (virtualKeyCode < 0)
        {
            return -1;
        }

        keyCodes.push_back(virtualKeyCode);
        token = strtok_s(NULL, delimiter, &context);
    }

    return 0;
}

int AppInfoConfReadWriter::GetVirtualKeyCode(const char* keyName) const
{
    int virtualKeyCode = ::GetVirtualKeyCode(keyName);

    if (virtualKeyCode < 0)
    {
        // Check if strToken is NUMPAD+ or not. (Exception)
        if (_stricmp(keyName, "NUMPAD") == 0)
        {
            virtualKeyCode = VK_ADD;
        }
    }

    return virtualKeyCode;
}

GRC_NAMESPACE_END