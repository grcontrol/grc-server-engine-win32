#pragma once
#include "_grc.h"
#include <stdint.h>

GRC_NAMESPACE_BEGIN

class GRCPacket
{
public:
    struct Header
    {
        uint32_t type;
        uint32_t bodySize;
        uint32_t session;
    };

public:
    uint32_t GetType() const { return m_header.type; }
    uint32_t GetBodySize() const { return m_header.bodySize; }
    uint32_t GetSession() const { return m_header.session; }
    uint32_t GetDataSize() const { return sizeof(Header) + m_header.bodySize; }

    static GRCPacket* Create(char* data, uint32_t size);
    static GRCPacket* Create(
        const PACKET_TYPE packetType, const char* body, const uint32_t bodySize);
    static GRCPacket* CreateJsonPacket(const string& json);
    static void Delete(GRCPacket* packet);

    char* GetBody(uint32_t* size = NULL) const;
    char* GetData(uint32_t* size = NULL) const;

    void SetUserData(void* userData) { m_userData = userData; }
    void* GetUserData() const { return m_userData; }

private:
    GRCPacket();
    ~GRCPacket();

    int Set(char* data, uint32_t size);

private:
    Header m_header;
    // total packet data: header + data
    char* m_data;
    
    // It is not related to grc packet data.
    void* m_userData;
};

GRC_NAMESPACE_END