#include "MouseExecuter.h"
#include "MouseSimulator.h"

MouseExecuter::MouseExecuter()
{
}

MouseExecuter::~MouseExecuter()
{
}

COMMAND_TYPE MouseExecuter::GetType()
{
    return eCT_MOUSE;
}

int MouseExecuter::Run(const Json::Value & reqJson, Json::Value & resJson)
{
    int32_t x = reqJson.get("x", Json::Value(0)).asInt();
    int32_t y = reqJson.get("y", Json::Value(0)).asInt();
    uint32_t flags = reqJson.get("action", Json::Value(0u)).asUInt();

    uint32_t events = MouseSimulator::SendInput(x, y, flags);

    return (events > 0) ? GRC_OK : GRC_ERR;
}
