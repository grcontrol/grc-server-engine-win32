#include "GRCEngine.h"
#include <gwlog.h>
#include "Command.h"
#include "Executer.h"
#include "Packet.h"
#include <json/json.h>
#include "BodyParser.h"

#define SAFE_RELEASE(handle)    \
    if (handle)                 \
    {                           \
        free(handle);           \
        handle = NULL;          \
    }

GRC_NAMESPACE_BEGIN

/**
 * Constructor
 */
GRCEngine::GRCEngine() :
    m_state(gw::eLCS_NONE)
    ,m_running(false)
    ,m_loop(NULL)
    ,m_asyncExit(NULL)
    ,m_tcpServer(NULL)
    ,m_udpServer(NULL)
    ,m_commandExecuterStorage(NULL)
{
}

/**
 * Destructor
 */
GRCEngine::~GRCEngine()
{
}

/**
 * Initialize GRCEngine.<br>
 * Prepare necessary resources.<br>
 * These resources are freed in Exit().
 *
 * @return 0: success, <0: error
 */
int GRCEngine::Init()
{
    LOGD_CALL(TAG, "start");

    m_commandExecuterStorage = new ExecuterStorage();

    m_loop = uv_default_loop();
    m_loop->data = this;

    m_asyncExit = (uv_async_t*)malloc(sizeof(uv_async_t));
    m_asyncExit->data = this;

    m_tcpServer = new TCPServer();
    m_udpServer = new UDPServer();

    m_state.SetState(gw::eLCS_INIT);

    LOGD_CALL(TAG, "end");

    return 0;
}

/**
 * Open GRCEngine.<br>
 * Open server sockets. (UDP, TCP and so on)
 * Open() is the counterpart of Close().
 *
 * @param param
 * @return 0: success, <0: error
 */
int GRCEngine::Open(const GRCEngine::Param& param)
{
    LOGD_CALL(TAG, "start");

    int ret;
    ret = OpenTCPServer(param.port);
    ret = OpenUDPServer(param.port);

    m_state.SetState(gw::eLCS_OPEN);

    LOGD_CALL(TAG, "end");
    return 0;
}

int GRCEngine::OpenTCPServer(uint16_t port)
{
    LOGD_CALL(TAG, "start");

    TCPServer::Param param;
    param.loop = m_loop;
    param.port = port;

    int ret = m_tcpServer->Open(param);
    if (ret != 0)
    {
        LOGE(TAG, "%s(%d) %s: ret(%d)", __CALL_INFO__, ret);
    }

    LOGD_CALL(TAG, "end");

    return ret;
}

int GRCEngine::OpenUDPServer(uint16_t port)
{
    LOGD_CALL(TAG, "start");

    UDPServer::Param param;
    param.loop = m_loop;
    param.port = port;
    param.callback = this;

    int ret = m_udpServer->Open(param);
    if (ret != 0)
    {
        LOGE(TAG, "%s(%d) %s: ret(%d)", __CALL_INFO__, ret);
    }

    LOGD_CALL(TAG, "end");

    return ret;
}

/**
 * Start GRCEngine.<br>
 * Returns immediately, launching a worker thread.
 *
 * @return 0: success, <0: error
 */
int GRCEngine::Start()
{
    LOGD_CALL(TAG, "start");

    int ret;
    ret = uv_async_init(m_loop, m_asyncExit, OnAsyncExit);
    //ret = m_udpServer->Start();

    ret = gw::ThreadLauncher::Execute(this);
    if (ret == 0)
    {
        m_state.SetState(gw::eLCS_START);
    }

    LOGD_CALL(TAG, "end");
    return ret;
}

/**
 * Wait until a working thread is ready.
 */
int GRCEngine::WaitForReady(uint32_t timeout_ms)
{
    if (m_state.GetState() == gw::eLCS_START)
    {
        while (!IsRunning())
        {
            Sleep(0);
        }

        return 0;
    }

    return -1;
}

/**
* Stop GRCEngine.<br>
* Wait for the termination of worker thread.
*
* @return 0: success, <0: error
*/
int GRCEngine::Stop()
{
    LOGD_CALL(TAG, "start");

    int ret;

    ret = uv_async_send(m_asyncExit);

    ret = Join(INFINITE);
    if (ret == 0)
    {
        SetRunning(false);
        m_state.SetState(gw::eLCS_OPEN);
    }

    LOGD_CALL(TAG, "end");
    return ret;
}

/**
 * Close GRCEngine.<br>
 * All resources created in Open() are freed.
 *
 * @return 0: success, <0: error
 */
int GRCEngine::Close()
{
    LOGD_CALL(TAG, "start");

    m_state.SetState(gw::eLCS_INIT);
    LOGD_CALL(TAG, "end");
    return 0;
}

int GRCEngine::CloseTCPServer()
{
    LOGD_CALL(TAG, "start");

    int ret = -1;

    if (m_tcpServer)
    {
        ret = m_tcpServer->Close();
    }

    LOGD_CALL(TAG, "end");

    return ret;
}

int GRCEngine::CloseUDPServer()
{
    LOGD_CALL(TAG, "start");

    int ret = -1;

    if (m_udpServer)
    {
        ret = m_udpServer->Close();
    }

    LOGD_CALL(TAG, "end");

    return ret;
}

/**
 * Exit GRCEngine.<br>
 * All resources created by Init() are freed.
 *
 * @return 0: success, <0: error
 */
int GRCEngine::Exit()
{
    LOGD_CALL(TAG, "start");

    SAFE_RELEASE(m_asyncExit);
    SAFE_RELEASE(m_commandExecuterStorage);
    SAFE_RELEASE(m_udpServer);
    SAFE_RELEASE(m_tcpServer);
    
    m_state.SetState(gw::eLCS_NONE);

    LOGD_CALL(TAG, "end");
    return 0;
}

/**
 * It is called on another thread rather than main thread.
 *
 * @param userData 별도 쓰레드로 실행될 때 사용할 데이터.
 * @return Thread callback return value (0: success, otherwise: failure)
 */
int GRCEngine::Run(void* userData)
{
    LOGD_CALL(TAG, "start");

    int ret;

    SetRunning(true);

    ret = m_udpServer->Start();
    //ret = m_tcpServer->Start();

    ret = uv_run(m_loop, UV_RUN_DEFAULT);

    LOGD_CALL(TAG, "end");
    return 0;
}

/**
 * static method
 * Called when uv_async_send(m_asyncExit) is called in GRCEngine::Stop().
 * It runs on the same thread as event loop.
 *
 * @param handle uv_async_t handle
 */
void GRCEngine::OnAsyncExit(uv_async_t* handle)
{
    LOGD_CALL(TAG, "start");

    GRCEngine* engine = (GRCEngine*)handle->data;
    if (engine)
    {
        engine->_OnAsyncExit(handle);
    }

    LOGD_CALL(TAG, "end");
}

/**
 * Called when uv_async_send(m_asyncExit) is called in GRCEngine::Stop().
 * It runs on the same thread as event loop.
 *
 * @param handle uv_async_t handle
 */
void GRCEngine::_OnAsyncExit(uv_async_t* handle)
{
    LOGD_CALL(TAG, "start");

    // Before stopping the event loop, stop and close uv handles.
    uv_close((uv_handle_t*)m_asyncExit, OnClose);
    m_udpServer->Close();
    m_tcpServer->Close();

    // Stop the event loop in another thread.
    uv_stop(m_loop);

    LOGD_CALL(TAG, "end");
}

/**
 * static method
 * callback function of uv_close()
 * @param handle uv handle to close
 */
void GRCEngine::OnClose(uv_handle_t* handle)
{
    LOGD(TAG, "%s(%d) %s: start handle(%p)", __CALL_INFO__, handle);

    GRCEngine* engine = (GRCEngine*)handle->data;
    if (engine)
    {
        engine->_OnClose(handle);
    }

    LOGD_CALL(TAG, "end");
}

void GRCEngine::_OnClose(uv_handle_t* handle)
{
    LOGD(TAG, "%s(%d) %s: start handle(%p)", __CALL_INFO__, handle);

    if (handle)
    {
        // Do something when closing a handle.
    }

    LOGD_CALL(TAG, "end");
}

/**
 * Callback method which is called by Server implementation.
 * It is called on another thread.
 */
void GRCEngine::OnRecvPacket(const struct sockaddr* addr, Packet* packet)
{
    LOGD(TAG, "%s(%d) %s: start packet type(%u) size(%u)",
        __CALL_INFO__, packet->GetType(), packet->GetBodySize());

    Json::Value reqJson;
    Json::Value resJson;

    int ret = BodyParser::Parse(packet, reqJson);
    if (ret == GRC_OK)
    {
        HandleRequest(reqJson, resJson);
    }

    LOGD_CALL(TAG, "end");
}

int GRCEngine::HandleRequest(const Json::Value& reqJson, Json::Value& resJson)
{
    LOGD(TAG, "%s(%d) %s: start\n%s", __CALL_INFO__, reqJson.toStyledString().c_str());

    int ret = GRC_ERR;
    const int commandType = reqJson["type"].asInt();

    shared_ptr<Executer> commandExecuter =
        m_commandExecuterStorage->Get(commandType);
    if (commandExecuter)
    {
        ret = commandExecuter->Run(reqJson, resJson);
    }

    LOGD(TAG, "%s(%d) %s: end ret(%s)", __CALL_INFO__, GetErrorName(ret));

    return ret;
}

GRC_NAMESPACE_END