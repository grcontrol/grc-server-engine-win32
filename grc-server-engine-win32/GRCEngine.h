#pragma once
#ifndef GRCENGINE_H
#define GRCENGINE_H

#include "_grc.h"
#include <State.h>
#include <ThreadLauncher.h>
#include "UDPServer.h"
#include "TCPServer.h"
#include "ExecuterStorage.h"

#include <list>

GRC_NAMESPACE_BEGIN

/**
 * \brief GRCEngine Interface
 *
 * GRCEngine will be created as dll library.
 */
class GRCEngine : public gw::Runnable, public UDPServerCallback
{
public:
	/**
	 * These bitwise flags indicate that which server types are enabled by GRCEngine.
	 */
	enum SERVER_TYPE
	{
		// TCP 
		TCP			= 0x01,
		// UDPServer
		UDP			= 0x02,
		// BlueTooth
		BLUETOOTH	= 0x04,
	};

	/**
	 * Open() 메소드에서 사용되는 파라메터 구조체
	 */
	struct Param
	{
		SERVER_TYPE serverFlags;

		/**
		 * Network server port number
		 */
		uint16_t port;
	};

public:
	explicit GRCEngine();
	virtual ~GRCEngine();

	// Inherited via Runnable
	virtual int Run(void* userData) override;

	int Init();
	int Open(const Param& param);
	int Start();
    int WaitForReady(uint32_t timeout_ms);
	int Stop();
	int Close();
	int Exit();

    // Inherited via ServerCallback
    virtual void OnRecvPacket(const struct sockaddr* addr, Packet * packet) override;

private:
    bool IsRunning() const { return m_running; }
    void SetRunning(bool running) { m_running = running; }

	int OpenTCPServer(uint16_t port);
	int OpenUDPServer(uint16_t port);

	int CloseTCPServer();
	int CloseUDPServer();

    static void OnAsyncExit(uv_async_t* handle);
    void _OnAsyncExit(uv_async_t* handle);
    static void OnClose(uv_handle_t* handle);
    void _OnClose(uv_handle_t* handle);

    int HandleRequest(const Json::Value& reqJson, Json::Value& resJson);

private:
	/**
	 * GRCEngine lifecycle status (None, Init, Open, Start)
	 */
	gw::State m_state;

    bool m_running;

	/**
	 * libuv loop
	 */
	uv_loop_t* m_loop;

    /**
     * make event loop in another thread stopped.
     */
    uv_async_t* m_asyncExit;
	TCPServer* m_tcpServer;
	UDPServer* m_udpServer;

	ExecuterStorage* m_commandExecuterStorage;
};

GRC_NAMESPACE_END

#endif
