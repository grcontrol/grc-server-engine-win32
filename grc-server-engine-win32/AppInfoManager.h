#pragma once
#include <map>

class CAppInfo;

typedef map<UINT, CAppInfo*> APP_INFO_MAP;

class CAppInfoManager
{
protected:
	APP_INFO_MAP m_AppInfoMap;

public:
	CAppInfoManager(void);
	~CAppInfoManager(void);

	int GetCount() const;

	HRESULT Load(const TCHAR* szPathName);
	CAppInfo* GetByID(int nID);
	CAppInfo* GetByIndex(int nIndex);

	HRESULT Clear();
};
