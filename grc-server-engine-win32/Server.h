#pragma once
#ifndef SERVER_H
#define SERVIER_H

#include "_grc.h"

GRC_NAMESPACE_BEGIN

class Packet;

class ServerCallback
{
public:
    virtual void OnRecvPacket(Packet* packet) = 0;
};

class Server
{
public:
    virtual void SetCallback(ServerCallback* callback) = 0;
    virtual int SendPacket(Packet* packet) = 0;
};

GRC_NAMESPACE_END

#endif