#pragma once
#ifndef SHORTCUT_EXECUTER_H
#define SHORTCUT_EXECUTER_H

#include "Executer.h"

class ShortcutExecuter : public Executer
{
public:
    ShortcutExecuter();
    virtual ~ShortcutExecuter();

    // Inherited via Executer
    virtual COMMAND_TYPE GetType() override;
    virtual int Run(const Json::Value & reqJson, Json::Value & resJson) override;
};

#endif