#pragma once
#ifndef ECHO_EXECUTER_H
#define ECHO_EXECUTER_H

#include "Executer.h"

/**
 * CommanadExecuter for test.
 */
class EchoExecuter : public Executer
{
public:
    EchoExecuter();
    virtual ~EchoExecuter();

    // Inherited via Executer
    virtual COMMAND_TYPE GetType() override;
    virtual int Run(const Json::Value& reqJson, Json::Value& resJson) override;
};

#endif