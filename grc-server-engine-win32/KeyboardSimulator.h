#pragma once
#ifndef KEY_INPUTER_H
#define KEY_INPUTER_H

#include <vector>
#include <string>

/**
 * Generate keyboard input event so that we can achieve virtual key input effect.
 * This class will be used in each Executer.
 */
class KeyboardSimulator
{
public:
    static int SendInputKeyCodes(const int count, const int keyCodes[]);
    static int SendInputText(const std::wstring& text);
};

#endif