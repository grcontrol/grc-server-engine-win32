#pragma once
#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#include "Server.h"
#include <json/json.h>

GRC_NAMESPACE_BEGIN

class GRCEngine;
class Packet;
class PacketTokenizer;

class UDPServerCallback
{
public:
    virtual void OnRecvPacket(const struct sockaddr* addr, Packet* packet) = 0;
};

class UDPServer
{
	friend void OnClose(uv_handle_t* handle);
	friend void OnAlloc(uv_handle_t* handle, size_t size, uv_buf_t* buf);

public:
	struct Param
	{
		uv_loop_t* loop;
		uint16_t port;
        UDPServerCallback* callback;
	};

public:
	UDPServer();
	virtual ~UDPServer();

	int Open(const Param& param);
	int Start();
	int Stop();
	int Close();

private:
	static void OnRecv(
		uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
		const struct sockaddr* addr, unsigned flags);
    void _OnRecv(
		uv_udp_t* handle, ssize_t nread, const uv_buf_t* buf,
		const struct sockaddr* addr, unsigned flags);
    static void OnClose(uv_handle_t* handle);
    void OnRecvPacket(Packet* packet, const struct sockaddr* addr);
    int Execute(Packet* packet, Json::Value& resJson);
    int Response(const struct sockaddr* addr, const Json::Value& resJson);
    static void OnSend(uv_udp_send_t* req, int status);

private:
    GRCEngine* m_grcEngine;
	uv_loop_t* m_loop;
	uv_udp_t* m_server;
    std::unique_ptr<PacketTokenizer> m_packetTokenizer;
    UDPServerCallback* m_callback;
};

GRC_NAMESPACE_END

#endif