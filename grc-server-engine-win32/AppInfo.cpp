#include "AppInfo.h"
#include <sys/stat.h>
#include <CharsetUtil.h>

GRC_NAMESPACE_BEGIN

static void SetString(wstring* pstrText, const char* szValue)
{
    *pstrText = gw::OemToUnicode(szValue);
}

// AppInfo Implementation =============================================
AppInfo::AppInfo(void)
{
}

AppInfo::~AppInfo(void)
{
}

void AppInfo::SetAppName(const char* appName)
{
    SetString(&m_appName, appName);
}

void AppInfo::SetClassName(const char* className)
{
    SetString(&m_className, className);
}

int AppInfo::SetPathName(const char* pathName)
{
    SetString(&m_pathName, pathName);

    int index = m_pathName.rfind('\\');
    if (index > 0)
    {
        m_dir = m_pathName.substr(0, index);
        m_fileName = m_pathName.substr(index + 1);
    }
    else
    {
        m_dir.clear();
    }

    return 0;
}

int AppInfo::SetKeyCodes(uint32_t configKey, const KeyCodes& keyCodes)
{
    m_keyMap[configKey] = keyCodes;
    return 0;
}

int AppInfo::GetKeyCodes(uint32_t configKey, KeyCodes& keyCode)
{
    KEY_MAP::iterator it;
    KEY_MAP::iterator itEnd = m_keyMap.end();

    it = m_keyMap.find(configKey);
    if (it == itEnd)
    {
        return -1;
    }

    keyCode = it->second;

    return 0;
}

void AppInfo::Clear()
{
    m_appName.clear();
    m_className.clear();
    m_fileName.clear();
    m_pathName.clear();
    m_dir.clear();
    m_keyMap.clear();
}

GRC_NAMESPACE_END