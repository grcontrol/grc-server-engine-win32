#pragma once
#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include "_grc.h"

class TCPServer
{
	friend void OnClose(uv_handle_t* handle);

public:
    struct Param
    {
        uv_loop_t* loop;
        uint16_t port;
    };

    TCPServer();
    ~TCPServer();

    int Open(const Param& param);
    int Start();
    int Stop();
    int Close();

private:
    static void OnConnection(uv_stream_t* server, int status);
	static void OnAlloc(uv_handle_t* handle, size_t size, uv_buf_t* buf);
	static void OnRead(uv_stream_t* handle, ssize_t nread, const uv_buf_t* buf);

    int OpenServerHandle(uv_loop_t* loop, uint16_t port);

private:
    uv_loop_t* m_loop;
    uv_tcp_t* m_server;

	char* m_buf;
	size_t m_bufSize;
};

#endif