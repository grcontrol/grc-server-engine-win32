#pragma once
#ifndef FILE_BROWSER_H
#define FILE_BROWSER_H

#include "_grc.h"
#include <string>
#include <json/json.h>

using namespace std;

/**
 * All input and output strings are in UTF-8 format.
 * Handles all requests related to file browsing.
 */
class FileBrowser
{
public:

public:
	FileBrowser(void);
	~FileBrowser(void);

	int List(const string& dir, Json::Value& files) const;
	int Remove(const string& filePath);
	int Move(const string& oldPathName, const string& newPathName);

private:
	bool IsFileSkipped(const WIN32_FIND_DATA& findFileData) const;
	FILE_SYSTEM_TYPE GetFileType(const WIN32_FIND_DATA& findFileData) const
	{
		return (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ? eFST_DIRECTORY : eFST_FILE;
	}

	int ListDrive(Json::Value& files) const;
	int ListDirectory(const string& dir, Json::Value& files) const;
};

#endif