#include "BinaryBodyConverter.h"
#include "Command.h"
#include "Packet.h"

GRC_NAMESPACE_BEGIN

BinaryBodyConverter::BinaryBodyConverter()
{
}

BinaryBodyConverter::~BinaryBodyConverter()
{
}

int BinaryBodyConverter::BinToJson(Packet* packet, Json::Value& json)
{
    const uint32_t commandType = packet->GetType();

    switch (commandType)
    {
    case eCT_SERVER:
        return HandleServerCommand(packet, json);
    }

    return GRC_NOT_IMPLEMENTED;
}

int BinaryBodyConverter::HandleServerCommand(Packet* packet, Json::Value& json)
{
    return 0;
}

int BinaryBodyConverter::HandleServerAuthenticateCommand(
    Packet* packet, Json::Value& json)
{
    return GRC_OK;
}

GRC_NAMESPACE_END