#include "TCPServer.h"
#include "_grc.h"

TCPServer::TCPServer() :
	m_loop(NULL),
	m_server(NULL),
	m_buf(NULL),
	m_bufSize(0)
{
}

TCPServer::~TCPServer()
{
}

int TCPServer::Open(const Param& param)
{
	LOGD_CALL(TAG, "start");

	m_server = (uv_tcp_t*)malloc(sizeof(uv_tcp_t));

	int ret = OpenServerHandle(param.loop, param.port);
	m_loop = param.loop;

	m_bufSize = 128 * 1024;
	m_buf = (char*)malloc(m_bufSize);

	LOGD_CALL(TAG, "end");

	return ret;
}

/**
* open a server handle to received parentDeviceID.
*
* @param port server port
* @return 0(success)
*/
int TCPServer::OpenServerHandle(uv_loop_t* loop, uint16_t port)
{
    LOGD_CALL(TAG, "start");

    int ret = uv_tcp_init(loop, m_server);
    m_server->data = this;

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(port);

    ret = uv_tcp_bind(m_server, (const struct sockaddr*)&addr, 0);
    if (ret != 0)
    {
        return -1;
    }

    ret = uv_listen((uv_stream_t*)m_server, 5, TCPServer::OnConnection);
	LOGD_CALL(TAG, "end");

    return ret;
}

int TCPServer::Start()
{
	LOGD_CALL(TAG, "start");
	LOGD_CALL(TAG, "end");
	return 0;
}

int TCPServer::Stop()
{
	LOGD_CALL(TAG, "start");
	LOGD_CALL(TAG, "end");
	return 0;
}

int TCPServer::Close()
{
	LOGD_CALL(TAG, "start");
	LOGD_CALL(TAG, "end");
	return 0;
}

/**
* Callback called when a stream server has received an incoming connection.
* The user can accept the connection by calling uv_accept().
* status will be 0 in case of success, < 0 otherwise.
* @param server: relay server's tcp handle
*/
void TCPServer::OnConnection(uv_stream_t* server, int status)
{
    LOGD_CALL(TAG, "start");

    TCPServer* thiz = (TCPServer*)server->data;

    // Init client connection using `server->loop`, passing the client handle.
    uv_tcp_t* client = (uv_tcp_t*)malloc(sizeof(uv_tcp_t));
    uv_tcp_init(thiz->m_loop, client);
    client->data = thiz;

    // Accept the now initialized client connection.
    int ret = uv_accept(server, (uv_stream_t*)client);
    LOGD(TAG, "%s(%d) %s uv_accept() returns %d", __CALL_INFO__, ret);

    if (ret == 0)
    {
        //thiz->m_localReadBuffer.Clear();
        ret = uv_read_start(
            (uv_stream_t*)client, TCPServer::OnAlloc, TCPServer::OnRead);
        LOGD(TAG, "%s(%d) %s uv_read_start() returns %d", __CALL_INFO__, ret);
    }

    if (ret != 0)
    {
        uv_close((uv_handle_t*)client, ::OnClose);
        LOGE_CALL(TAG, "error");
    }

    LOGD_CALL(TAG, "end");
}

/**
* Before reading data, allocate a buffer to read.
*/
void TCPServer::OnAlloc(uv_handle_t* handle, size_t size, uv_buf_t* buf)
{
	TCPServer* thiz = (TCPServer*)handle->data;

	if (size > thiz->m_bufSize)
	{
		if (thiz->m_buf)
		{
			free(thiz->m_buf);
		}

		thiz->m_buf = (char*)malloc(size);
		thiz->m_bufSize = size;
	}

	/* libuv suggests a buffer size but leaves it up to us to create one of any size we see fit */
	buf->base = thiz->m_buf;
	buf->len = thiz->m_bufSize;
}

/**
* Callback called when data was read on a stream.
* nread is > 0 if there is data available, 0 if libuv is done reading for now, or < 0 on error.
* The callee is responsible for stopping closing the stream when an error happens by calling uv_read_stop() or uv_close().
* Trying to read from the stream again is undefined.
* The callee is responsible for freeing the buffer, libuv does not reuse it.
* The buffer may be a null buffer (where buf->base=NULL and buf->len=0) on error.
*
* Data from wireless module has been received.
*/
void TCPServer::OnRead(uv_stream_t* handle, ssize_t nread, const uv_buf_t* buf)
{
	LOGD_CALL(TAG, "start");

	buf->base[nread] = '\0';
	printf("%s\n", buf->base);

#if 0
	TCPServer* thiz = (TCPServer*)handle->data;
	int ret = 0;

	/* Errors or EOF */
	if (nread < 0)
	{
		/* Client signaled that all data has been sent,
		so we can close the connection and are done */
		ret = uv_read_stop(handle);
		uv_close((uv_handle_t*)handle, TCPServer::OnClose);
	}
	else if (nread > 0)
	{
		thiz->m_localReadBuffer.Write(buf->base, nread);

		while (true)
		{
			int dataSize = thiz->m_localReadBuffer.GetDataSize();
			if (dataSize <= 4)
			{
				break;
			}

			int size;
			char* buf = thiz->m_localReadBuffer.GetBuffer();

			memcpy(&size, buf, sizeof(int));
			size = ntohl(size);

			if (size <= dataSize + 4)
			{
				Json::Value root;
				Json::Reader reader;
				if (reader.parse(buf + 4, buf + 4 + size, root, false))
				{
					LOGI_CALL(TAG, root.toStyledString().c_str());
					thiz->OnLocalData(root);
				}

				thiz->m_localReadBuffer.Consume(size + 4);
			}
			else
			{
				LOGI(TAG, "%s(%d) %s: Not enough data. size(%d) dataSize(%d)",
					size, dataSize);
				break;
			}
		}
	}
#endif

	LOGD_CALL(TAG, "end");
}