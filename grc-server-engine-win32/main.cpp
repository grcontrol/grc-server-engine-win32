// grc-server-engine-win32.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "GRCEngine.h"
#include <gwlog.h>

int main()
{
	gwlog_init("grc.log", eLO_CONSOLE, eLL_DEBUG);

	GRCEngine engine;

	engine.Init();
	engine.Exit();

	gwlog_exit();

    return 0;
}

