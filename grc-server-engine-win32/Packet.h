#ifndef PACKET_TOKENIZER_H
#define PACKET_TOKENIZER_H

#include "_grc.h"
#include <memory>

namespace gw
{
    class DynamicBuffer;
}

USE_GW_NAMESPACE

GRC_NAMESPACE_BEGIN

class Packet
{
public:
    struct Header
    {
        uint32_t type;
        uint32_t bodySize;
        uint32_t session;
    };

public:
    Packet(void) : m_userData(NULL) {}
    ~Packet(void) {}

    uint32_t GetType() const { return m_header.type; }
    void SetType(const uint32_t type) { m_header.type = type; }

    uint32_t GetSession() const { return m_header.session; }
    void SetSession(const uint32_t session) { m_header.session = session; }

    unsigned short GetBodySize() const { return m_header.bodySize; }
    int GetPacketSize() const
    {
        return m_header.bodySize + sizeof(m_header);
    }

    void SetBody(const char* pBuf, const int nDataSize)
    {
        m_header.bodySize = static_cast<uint32_t> (nDataSize);
        m_pBody = pBuf;
    }
    const char* GetBody(int* pBodySize) const
    {
        if (pBodySize)
        {
            *pBodySize = m_header.bodySize;
        }

        return m_pBody;
    }

    void Clear()
    {
        m_header.bodySize = 0;
        m_pBody = NULL;
        m_userData = NULL;
    }

    void SetUserData(void* data) { m_userData = data; }
    void* GetUserData() const { return m_userData; }

private:
    Header m_header;
    const char* m_pBody;
    void* m_userData;
};

class PacketTokenizer
{
public:
	PacketTokenizer(const int bufSize);
	~PacketTokenizer(void);

	int Input(const char* pBuf, const int nDataSize);
	int GetNext(Packet* pPacket);
	int Clear();
	int Consume(const int nSize);

private:
	std::unique_ptr<DynamicBuffer> m_pBuffer;

	int ParseHeader(Packet::Header& header);
	int ParsePacket(Packet* pPacket);
};

GRC_NAMESPACE_END

#endif