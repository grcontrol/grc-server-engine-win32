#pragma once
#include "_grc.h"
#include <unordered_map>

using namespace std;

typedef vector<int> KeyCodes;
typedef unordered_map<uint32_t, KeyCodes> KEY_MAP;
typedef pair<uint32_t, KeyCodes> KeyPair;

GRC_NAMESPACE_BEGIN

class AppInfo
{
public:
	AppInfo(void);
	~AppInfo(void);

    uint32_t GetID() const { return m_id; }
    void SetID(uint32_t id) { m_id = id; }

    const TCHAR* GetAppName() const { return m_appName.c_str(); }
    const TCHAR* GetClassName() const { return m_className.c_str(); }
    const TCHAR* GetFileName() const { return m_fileName.c_str(); }
    const TCHAR* GetPathName() const { return m_pathName.c_str(); }
    const TCHAR* GetDir() const { return m_dir.c_str(); }

    void SetAppName(const char* appName);
    void SetClassName(const char* className);
    int SetPathName(const char* pathName);

    int GetKeyCodes(uint32_t configKey, KeyCodes& keyCodes);
    int SetKeyCodes(uint32_t configKey, const KeyCodes& keyCodes);
    void Clear();

private:
    /**
    * Application ID
    */
    uint32_t m_id;
    KEY_MAP m_keyMap;

    wstring m_appName;
    wstring m_className;
    wstring m_fileName;
    wstring m_pathName;
    wstring m_dir;
};

GRC_NAMESPACE_END