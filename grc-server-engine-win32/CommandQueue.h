#pragma once
#ifndef COMMAND_QUEUE_H
#define COMMAND_QUEUE_H

#include "_grc.h"
#include <list>

class Command;

class CommandQueue
{
public:
	CommandQueue();
	~CommandQueue();

	void Push(Command* command);
	Command* Pop();

private:
	typedef std::list<Command*> COMMAND_LIST;
	COMMAND_LIST m_commands;
};

#endif