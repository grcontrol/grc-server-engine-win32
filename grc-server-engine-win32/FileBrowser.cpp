#include "FileBrowser.h"
#include <CharsetUtil.h>
#include <sstream>

#if _DEBUG
#define JSON_WRITER Json::StyledWriter
#else
#define JSON_WRITER Json::FastWriter
#endif

FileBrowser::FileBrowser(void)
{
}

FileBrowser::~FileBrowser(void)
{
}

int FileBrowser::List(const string& dir, Json::Value& value) const
{
    int ret;
    Json::Value files(Json::arrayValue);

    if( dir.size() == 0 || dir.compare("\\") == 0 )
    {
        ret = ListDrive(files);
    }
    else
    {
        ret = ListDirectory(dir, files);
    }

    if (ret < 0)
    {
        return ret;
    }

    value["list"] = files;

    return 0;
}

int FileBrowser::ListDirectory(const string& dir, Json::Value& files) const
{
    wstring wstrPathName = gw::Utf8ToUnicode(dir);
    wstrPathName.append(L"\\*");

    WIN32_FIND_DATA findFileData;
    HANDLE hFindFile = FindFirstFile(wstrPathName.c_str(), &findFileData);

    if (hFindFile == INVALID_HANDLE_VALUE)
    {
        return -1;
    }

    BOOL bRet;
    int i = 0;
    wstring wstrFileName;
    Json::Value file;

    do
    {
        // Find a file or directory which should be skipped.
        if (!IsFileSkipped(findFileData))
        {
            file["type"] = GetFileType(findFileData);
            file["name"] = gw::UnicodeToUtf8(findFileData.cFileName);
            file["size"] =
                ((uint64_t)findFileData.nFileSizeHigh << 32) |
                (uint64_t)findFileData.nFileSizeLow;

            files.append(file);
            i++;
        }

        bRet = FindNextFile(hFindFile, &findFileData);

    } while(bRet);

    FindClose(hFindFile);

    return files.size();
}

int FileBrowser::ListDrive(Json::Value& files) const
{
    // Get DriveList
    const DWORD dwDrives = ::GetLogicalDrives();
    const char chDriveSymbol = 'A';
    char chDrives[26];
    DWORD dwMask = 1;
    int nDriveCount = 0;
    int i;

    TCHAR szRootPathName[MAX_PATH+1];
    TCHAR szVolumeName[MAX_PATH+1];
    TCHAR szFileSystemName[MAX_PATH+1];
    char szDrive[3] = { 'A', ':', '\0' };

    // 26 is the number of alphabet characters.
    for (i = 0; i < 26; i++)
    {
        if (dwDrives & dwMask)
        {
            chDrives[nDriveCount] = chDriveSymbol + i;
            nDriveCount++;
        }

        dwMask <<= 1;
    }

    string strDriveInUtf8;
    Json::Value file;
    BOOL bRet;

    for ( i = 0; i < nDriveCount; i++)
    {
        // Get VolumeName
        _stprintf_s(szRootPathName, MAX_PATH, _T("%c:\\"), chDrives[i]);
        bRet = GetVolumeInformation(
            szRootPathName,
            szVolumeName, MAX_PATH,
            NULL, NULL, NULL,
            szFileSystemName, MAX_PATH);

        if(bRet && _tcslen(szVolumeName) > 0)
        {
            wostringstream oss;
            oss << chDrives[i] << "  " << szVolumeName;

            strDriveInUtf8 = gw::UnicodeToUtf8(oss.str());
        }

        // ex) C:
        if(strDriveInUtf8.size() < 2)
        {
            szDrive[0] = chDrives[i];
            strDriveInUtf8 = szDrive;
        }

        file["type"] = static_cast<int> (eFST_DRIVE);
        file["name"] = strDriveInUtf8;
        file["size"] = 0;

        files.append(file);
        strDriveInUtf8.clear();
    }

    return nDriveCount;
}

bool FileBrowser::IsFileSkipped(const WIN32_FIND_DATA& findFileData) const
{
    const DWORD dwSkipAttributes =
        FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM | FILE_ATTRIBUTE_DEVICE;

    if (findFileData.dwFileAttributes & dwSkipAttributes)
    {
        return true;
    }

    const TCHAR* cFileName = findFileData.cFileName;
    const int nLength = _tcslen(cFileName);

    if (cFileName[0] == _T('.') && nLength < 3)
    {
        if (!_tcscmp(findFileData.cFileName, _T(".")) ||
            !_tcscmp(findFileData.cFileName, _T("..")))
        {
            return true;
        }
    }

    return false;
}
