#pragma once
#ifndef MOUSE_EXECUTER_H
#define MOUSE_EXECUTER_H

#include "Executer.h"

class MouseExecuter : public Executer
{
public:
    MouseExecuter();
    virtual ~MouseExecuter();



    // Inherited via Executer
    virtual COMMAND_TYPE GetType() override;

    virtual int Run(const Json::Value & reqJson, Json::Value & resJson) override;
};

#endif