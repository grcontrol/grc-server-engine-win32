#pragma once
#ifndef UV_FUNC_H
#define UV_FUNC_H

#include <uv.h>

void OnClose(uv_handle_t* handle);
void OnAlloc(uv_handle_t* handle, size_t size, uv_buf_t* buf);

#endif