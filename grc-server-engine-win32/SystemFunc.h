#pragma once
#ifndef SYSTEM_FUNC_H
#define SYSTEM_FUNC_H

#include "_grc.h"
#include <Windows.h>

extern int __stdcall ShutdownSystem(bool reboot);
extern int __stdcall SuspendSystem(bool hibernate);
extern int __stdcall SetNonSleepingMode(bool on);
extern int __stdcall LogOff();
extern int __stdcall TurnOffMonitor(HWND hWnd);
extern int __stdcall LockScreen();

#endif