#pragma once
#ifndef WINDOW_POSITIONER_H
#define WINDOW_POSITIONER_H

#include <windows.h>
#include <stdint.h>

/**
 * Bit-wise operation mask
 */
enum POSITION_TYPE
{
	ePT_LEFT		= 0x01,
	ePT_RIGHT		= 0x02,
	ePT_CENTER		= 0x03,
	ePT_HORIZONTAL	= 0xFF,

	ePT_TOP			= 0x0100,
	ePT_BOTTOM		= 0x0200,
	ePT_VCENTER		= 0x0300,
	ePT_VERTICAL	= 0xFF00,
};

/**
 * 윈도우의 위치를 조정하는 클래스
 */
class WindowPositioner
{
public:
	WindowPositioner(void);
	~WindowPositioner(void);

	void Run(const uint32_t uFlags, HWND hWnd);

private:
    int InitScreenSize();
    int CalcX(const uint32_t uType, const RECT& rtWindow);
    int CalcY(const uint32_t uType, const RECT& rtWindow);

private:
	RECT m_rtScreen;
};

#endif