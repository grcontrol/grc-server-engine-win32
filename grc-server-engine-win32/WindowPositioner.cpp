#include "WindowPositioner.h"

static long GetWidth(const RECT& rect)
{
    return rect.right - rect.left;
}

static long GetHeight(const RECT& rect)
{
    return rect.bottom - rect.top;
}

WindowPositioner::WindowPositioner(void)
{
}

WindowPositioner::~WindowPositioner(void)
{
}

/**
 * 지정된 윈도우의 위치를 변경한다.
 *
 * @param uFlags 위치 변경에 대한 flags
 * @param hWnd 위치를 변경할 윈도우 핸들
 */
void WindowPositioner::Run(const uint32_t uFlags, HWND hWnd)
{
	int ret = InitScreenSize();
	if (ret < 0)
	{
		return;
	}

	RECT rtWindow;
	GetWindowRect(hWnd, &rtWindow);

	const int x = CalcX(uFlags & ePT_HORIZONTAL, rtWindow);
	const int y = CalcY(uFlags & ePT_VERTICAL, rtWindow);

	if (x > -1 && y > -1)
	{
		SetWindowPos(
			hWnd, 0, x, y, 0, 0, 
			SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_SHOWWINDOW);
	}
}

/**
 * 현재 데스크탑 윈도우의 전체 크기를 확인한다.
 */
int WindowPositioner::InitScreenSize()
{
	HWND hWndDesktop = GetDesktopWindow();
	if (!IsWindow(hWndDesktop))
	{
		return -1;
	}

	GetWindowRect(hWndDesktop, &m_rtScreen);

	return 0;
}

/**
 * 윈도우를 이동시킬 위치의 x 좌표를 계산한다.
 */
int WindowPositioner::CalcX(const uint32_t uType, const RECT& rtWindow)
{
	int x = -1;

	switch (uType)
	{
	case ePT_LEFT:
		x = 0;
		break;

	case ePT_RIGHT:
		x = GetWidth(m_rtScreen) - GetWidth(rtWindow);
		break;

	case ePT_CENTER:
		x = (GetWidth(m_rtScreen) - GetWidth(rtWindow)) / 2;
		break;
	}

	return x;
}

/**
* 윈도우를 이동시킬 위치의 y 좌표를 계산한다.
*/
int WindowPositioner::CalcY(const uint32_t uType, const RECT& rtWindow)
{
	int y = -1;

	switch (uType)
	{
	case ePT_TOP:
		y = 0;
		break;

	case ePT_BOTTOM:
		y = GetHeight(m_rtScreen) - GetHeight(rtWindow);
		break;

	case ePT_VCENTER:
		y = (GetHeight(m_rtScreen) - GetHeight(rtWindow)) / 2;
		break;
	}

	return y;
}