#include "_grc.h"

const char* TAG = "GRC";

const int LOGGER_BUF_SIZE = 1024 * 4;
const unsigned short DEFAULT_UDP_SERVER_PORT = 1028;
const unsigned short DEFAULT_TCP_SERVER_PORT = 1105;
const TCHAR* TSZ_SERVER_NAME = _T("GoldwormController");

const char* SZ_CONFIG_KEY_NAME[] =
{
    "APP_NAME",
    "CLASS_NAME",
    "PATH_NAME",
    "JUMP_BACKWARD",
    "JUMP_FORWARD",
    "PREV",
    "NEXT",
    "PLAY",
    "PAUSE",
    "STOP",
    "FULL_SCREEN",
    "DOWN",
    "UP",
    "MUTE",
    "SUBTITLE",
    "LEFT",
    "RIGHT",
    "CLOVER",
    "HEART",
    "DIAMOND",
    "SPADE",
    // subtitle
    "SUBTITLE_BACKWARD",
    "SUBTITLE_RESET",
    "SUBTITLE_FORWARD",
    // audio
    "AUDIO_BACKWARD",
    "AUDIO_RESET",
    "AUDIO_CHANGE",
    "AUDIO_FORWARD",
    // playstation
    "CIRCLE",
    "CROSS",
    "RECTANGLE",
    "TRIANGLE",
};

const TCHAR* WSZ_CONFIG_KEY_NAME[] =
{
    _T("APP_NAME"),
    _T("CLASS_NAME"),
    _T("PATH_NAME"),
    _T("JUMP_BACKWARD"),
    _T("JUMP_FORWARD"),
    _T("PREV"),
    _T("NEXT"),
    _T("PLAY"),
    _T("PAUSE"),
    _T("STOP"),
    _T("FULL_SCREEN"),
    _T("DOWN"),
    _T("UP"),
    _T("MUTE"),
    _T("SUBTITLE"),
    _T("LEFT"),
    _T("RIGHT"),
    _T("CLOVER"),
    _T("HEART"),
    _T("DIAMOND"),
    _T("SPADE"),
    // subtitle
    _T("SUBTITLE_BACKWARD"),
    _T("SUBTITLE_RESET"),
    _T("SUBTITLE_FORWARD"),
    // audio
    _T("AUDIO_BACKWARD"),
    _T("AUDIO_RESET"),
    _T("AUDIO_CHANGE"),
    _T("AUDIO_FORWARD"),
    // playstation
    _T("CIRCLE"),
    _T("CROSS"),
    _T("RECTANGLE"),
    _T("TRIANGLE"),
};

const int CONFIG_KEY_COMMAND_TABLE[] =
{
    eRC_UNKNOWN,
    eRC_UNKNOWN,
    eRC_UNKNOWN,
    eRC_JUMP_BACKWARD,
    eRC_JUMP_FORWARD,
    eRC_PREV,
    eRC_NEXT,
    eRC_PLAY,
    eRC_PAUSE,
    eRC_STOP,
    eRC_FULL_SCREEN,
    eRC_DOWN,
    eRC_UP,
    eRC_MUTE,
    eRC_SUBTITLE,
    eRC_LEFT,
    eRC_RIGHT,
    // trump
    eRC_CLOVER,
    eRC_HEART,
    eRC_DIAMOND,
    eRC_SPADE,
    // subtitle
    eRC_SUBTITLE_BACKWARD,
    eRC_SUBTITLE_RESET,
    eRC_SUBTITLE_FORWARD,
    // audio
    eRC_AUDIO_BACKWARD,
    eRC_AUDIO_RESET,
    eRC_AUDIO_CHANGE,
    eRC_AUDIO_FORWARD,
    // playstation
    eRC_CIRCLE,
    eRC_CROSS,
    eRC_RECTANGLE,
    eRC_TRIANGLE,
};

const int CONFIG_KEY_COUNT =
sizeof(SZ_CONFIG_KEY_NAME) / sizeof(SZ_CONFIG_KEY_NAME[0]);

extern int VERSION[] = { 1, 3, 5 };
extern const int VERSION_COUNT = sizeof(VERSION) / sizeof(VERSION[0]);

typedef struct
{
    char* text;
    int value;
} TextValueItem;
const TextValueItem VIRTUAL_KEYCODE_TABLE[] =
{
    { "LBUTTON", VK_LBUTTON }, // 0x01
    { "RBUTTON", VK_RBUTTON }, // 0x02
    { "CANCEL", VK_CANCEL }, // 0x03
    { "MBUTTON", VK_MBUTTON }, // 0x04
    { "XBUTTON1", VK_XBUTTON1 }, // 0x05
    { "XBUTTON2", VK_XBUTTON2 }, // 0x06
                                 // 0x07: Undefined // 0x07
    { "BACKSPACE", VK_BACK }, // 0x08
    { "BACK", VK_BACK }, // 0x08
    { "TAB", VK_TAB }, // 0x09
                       // (0x0A - 0x0B) Reserved
    { "CLEAR", VK_CLEAR }, // 0x0C
    { "RETURN", VK_RETURN }, // 0x0D
    { "ENTER", VK_RETURN }, // 0x0D
                            // 0x0E - 0x0F Undefined
    { "SHIFT", VK_SHIFT }, // 0x10
    { "CONTROL", VK_CONTROL }, // 0x11
    { "CTRL", VK_CONTROL }, // 0x11
    { "MENU", VK_MENU }, // 0x12
    { "ALT", VK_MENU }, // 0x12
    { "PAUSE", VK_PAUSE }, // 0x13
    { "CAPITAL", VK_CAPITAL }, // 0x14
    { "KANA", VK_KANA }, // 0x15 Input Method Editor (IME) Kana mode
    { "HANGUEL", VK_HANGEUL }, // 0x15 IME Hangul mode
    { "HANGUL", VK_HANGUL }, // 0x15 IME Hangul mode
                             // 0x16 Undefined
    { "JUNJA", VK_JUNJA }, // 0x17 IME Junja mode
    { "FINAL", VK_FINAL }, // 0x18 IME final mode
    { "HANJA", VK_HANJA }, // 0x19 IME Hanja mode
    { "KANJI", VK_KANJI }, // 0x19 IME Kanji mode
                           // 0x1A Undefined
    { "ESAPCE", VK_ESCAPE }, // 0x1B
    { "ESC", VK_ESCAPE }, // 0x1B
    { "CONVERT", VK_CONVERT }, // 0x1C IME convert
    { "NONCONVERT", VK_NONCONVERT }, // 0x1D IME nonconvert
    { "ACCEPT", VK_ACCEPT }, // 0x1E IME accept
    { "MODECHANGE", VK_MODECHANGE }, // 0x1F IME mode change request
    { "SPACE", VK_SPACE }, // 0x20
    { "PRIOR", VK_PRIOR }, // 0x21
    { "PAGEUP", VK_PRIOR }, // 0x21
    { "NEXT", VK_NEXT }, // 0x22
    { "PAGEDOWN", VK_NEXT }, // 0x22
    { "END", VK_END }, // 0x23
    { "HOME", VK_HOME }, // 0x24
    { "LEFT", VK_LEFT }, // 0x25
    { "UP", VK_UP }, // 0x26
    { "RIGHT", VK_RIGHT }, // 0x27
    { "DOWN", VK_DOWN }, // 0x28
    { "SELECT", VK_SELECT }, // 0x29
    { "PRINT", VK_PRINT }, // 0x2A
    { "EXECUTE", VK_EXECUTE }, // 0x2B
    { "SNAPSHOT", VK_SNAPSHOT }, // 0x2C
    { "INSERT", VK_INSERT }, // 0x2D
    { "INS", VK_INSERT }, // 0x2D
    { "DELETE", VK_DELETE }, // 0x2E
    { "DEL", VK_DELETE }, // 0x2E
    { "HELP", VK_HELP }, // 0x2F
    { "0", 0x30 }, // 0x30
    { "1", 0x31 }, // 0x31
    { "2", 0x32 }, // 0x32
    { "3", 0x33 }, // 0x33
    { "4", 0x34 }, // 0x34
    { "5", 0x35 }, // 0x35
    { "6", 0x36 }, // 0x36
    { "7", 0x37 }, // 0x37
    { "8", 0x38 }, // 0x38
    { "9", 0x39 }, // 0x39
                   // 0x3A - 0x40 Undefined
    { "A", 0x41 }, // 0x41
    { "B", 0x42 }, // 0x42
    { "C", 0x43 }, // 0x43
    { "D", 0x44 }, // 0x44
    { "E", 0x45 }, // 0x45
    { "F", 0x46 }, // 0x46
    { "G", 0x47 }, // 0x47
    { "H", 0x48 }, // 0x48
    { "I", 0x49 }, // 0x49
    { "J", 0x4A }, // 0x4A
    { "K", 0x4B }, // 0x4B
    { "L", 0x4C }, // 0x4C
    { "M", 0x4D }, // 0x4D
    { "N", 0x4E }, // 0x4E
    { "O", 0x4F }, // 0x4F
    { "P", 0x50 }, // 0x50
    { "Q", 0x51 }, // 0x51
    { "R", 0x52 }, // 0x52
    { "S", 0x53 }, // 0x53
    { "T", 0x54 }, // 0x54
    { "U", 0x55 }, // 0x55
    { "V", 0x56 }, // 0x56
    { "W", 0x57 }, // 0x57
    { "X", 0x58 }, // 0x58
    { "Y", 0x59 }, // 0x59
    { "Z", 0x5A }, // 0x5A
    { "LWIN", VK_LWIN }, // 0x5B Left Windows key (Microsoft Natural keyboard) 
    { "RWIN", VK_RWIN }, // 0x5C Right Windows key (Natural keyboard)
    { "APPS", VK_APPS }, // 0x5D Applications key (Natural keyboard)
                         // 0x5E Reserverd
    { "SLEEP", VK_SLEEP }, // 0x5F Computer Sleep key
    { "NUMPAD0", VK_NUMPAD0 }, // 0x60
    { "NUMPAD1", VK_NUMPAD1 }, // 0x61
    { "NUMPAD2", VK_NUMPAD2 }, // 0x62
    { "NUMPAD3", VK_NUMPAD3 }, // 0x63
    { "NUMPAD4", VK_NUMPAD4 }, // 0x64
    { "NUMPAD5", VK_NUMPAD5 }, // 0x65
    { "NUMPAD6", VK_NUMPAD6 }, // 0x66
    { "NUMPAD7", VK_NUMPAD7 }, // 0x67
    { "NUMPAD8", VK_NUMPAD8 }, // 0x68
    { "NUMPAD9", VK_NUMPAD9 }, // 0x69
    { "NUMPAD_MULTIPLY", VK_MULTIPLY }, // 0x6A
    { "NUMPAD*", VK_MULTIPLY }, // 0x6A
    { "NUMPAD_ADD", VK_ADD }, // 0x6B
    { "NUMPAD+", VK_ADD }, // 0x6B
    { "NUMPAD_SEPARATOR", VK_SEPARATOR }, // 0x6C
    { "NUMPAD_SUBTRACT", VK_SUBTRACT }, // 0x6D
    { "NUMPAD-", VK_SUBTRACT }, // 0x6D
    { "NUMPAD_DECIMAL", VK_DECIMAL }, // 0x6E
    { "NUMPAD_DIVIDE", VK_DIVIDE }, // 0x6F
    { "NUMPAD/", VK_DIVIDE }, // 0x6F
    { "F1", VK_F1 }, // 0x70
    { "F2", VK_F2 }, // 0x71
    { "F3", VK_F3 }, // 0x72
    { "F4", VK_F4 }, // 0x73
    { "F5", VK_F5 }, // 0x74
    { "F6", VK_F6 }, // 0x75
    { "F7", VK_F7 }, // 0x76
    { "F8", VK_F8 }, // 0x77
    { "F9", VK_F9 }, // 0x78
    { "F10", VK_F10 }, // 0x79
    { "F11", VK_F11 }, // 0x7A
    { "F12", VK_F12 }, // 0x7B
    { "F13", VK_F13 }, // 0x7C
    { "F14", VK_F14 }, // 0x7D
    { "F15", VK_F15 }, // 0x7E
    { "F16", VK_F16 }, // 0x7F
    { "F17", VK_F17 }, // 0x80
    { "F18", VK_F18 }, // 0x81
    { "F19", VK_F19 }, // 0x82
    { "F20", VK_F20 }, // 0x83
    { "F21", VK_F21 }, // 0x84
    { "F22", VK_F22 }, // 0x85
    { "F23", VK_F23 }, // 0x86
    { "F24", VK_F24 }, // 0x87
                       // 0x88 - 0x8F Unassigned
    { "NUMLOCK", VK_NUMLOCK }, // 0x90
    { "SCROLL", VK_SCROLL }, // 0x91
                             // 0x92 - 0x96 OEM specific
                             // 0x97 - 0x9F Unassigned
    { "LSHIFT", VK_LSHIFT }, // 0xA0
    { "RSHIFT", VK_RSHIFT }, // 0xA1
    { "LCONTROL", VK_LCONTROL }, // 0xA2
    { "RCONTROL", VK_RCONTROL }, // 0xA3
    { "LMENU", VK_LMENU }, // 0xA4
    { "LALT", VK_LMENU }, // 0xA4
    { "RMENU", VK_RMENU }, // 0xA5
    { "RALT", VK_RMENU }, // 0xA5
    { "BROWSER_BACK", VK_BROWSER_BACK }, // 0xA6
    { "BROWSER_FORWARD", VK_BROWSER_FORWARD }, // 0xA7
    { "BROWSER_REFRESH", VK_BROWSER_REFRESH }, // 0xA8
    { "BROWSER_STOP", VK_BROWSER_STOP }, // 0xA9
    { "BROWSER_SEARCH", VK_BROWSER_SEARCH }, // 0xAA
    { "BROWSER_FAVORITES", VK_BROWSER_FAVORITES }, // 0xAB
    { "BROWSER_HOME", VK_BROWSER_HOME }, // 0xAC
    { "VOLUME_MUTE", VK_VOLUME_MUTE }, // 0xAD
    { "VOLUME_DOWN", VK_VOLUME_DOWN }, // 0xAE
    { "VOLUME_UP", VK_VOLUME_UP }, // 0xAF
    { "MEDIA_NEXT_TRACK", VK_MEDIA_NEXT_TRACK }, // 0xB0
    { "MEDIA_PREV_TRACK", VK_MEDIA_PREV_TRACK }, // 0xB1
    { "MEDIA_STOP", VK_MEDIA_STOP }, // 0xB2
    { "MEDIA_PLAY_PAUSE", VK_MEDIA_PLAY_PAUSE }, // 0xB3
    { "LAUNCH_MAIL", VK_LAUNCH_MAIL }, // 0xB4
    { "LAUNCH_MEDIA_SELECT", VK_LAUNCH_MEDIA_SELECT }, // 0xB5
    { "LAUNCH_APP1", VK_LAUNCH_APP1 }, // 0xB6
    { "LAUNCH_APP2", VK_LAUNCH_APP2 }, // 0xB7
                                       // 0xB8 - 0xB9 Reserved
    { ";", VK_OEM_1 }, // 0xBA
    { "OEM_1", VK_OEM_1 }, // 0xBA
    { "=", VK_OEM_PLUS }, // 0xBB
    { "OEM_PLUS", VK_OEM_PLUS }, // 0xBB
    { "\\+", VK_OEM_PLUS }, // 0xBB
    { ",", VK_OEM_COMMA }, // 0xBC
    { "OEM_COMMA", VK_OEM_COMMA }, // 0xBC
    { "-", VK_OEM_MINUS }, // 0xBD
    { "OEM_MINUS", VK_OEM_MINUS }, // 0xBD
    { ".", VK_OEM_PERIOD }, // 0xBE
    { "OEM_PERIOD", VK_OEM_PERIOD }, // 0xBE
    { "/", VK_OEM_2 }, // 0xBF
    { "OEM_2", VK_OEM_2 }, // 0xBF
    { "`", VK_OEM_3 }, // 0xC0
    { "OEM_3", VK_OEM_3 }, // 0xC0
                           // 0xC1 - 0xD7 Reserved
                           // 0xD8 - 0xDA Unassigned
    { "[", VK_OEM_4 }, // 0xDB
    { "OEM_4", VK_OEM_4 }, // 0xDB
    { "\\", VK_OEM_5 }, // 0xDC
    { "OEM_5", VK_OEM_5 }, // 0xDC
    { "]", VK_OEM_6 }, // 0xDD
    { "OEM_6", VK_OEM_6 }, // 0xDD
    { "'", VK_OEM_7 }, // 0xDE
    { "OEM_7", VK_OEM_7 }, // 0xDE
    { "OEM_8", VK_OEM_8 }, // 0xDF
                           // 0xE0 Reserved
                           // 0XE1 OEM specific
    { "OEM_102", VK_OEM_102 }, // 0xE2
                               // 0xE3 - 0xE4 OEM specific
    { "PROCESSKEY", VK_PROCESSKEY }, // 0xE5 IME PROCESS key
                                     // Used to pass Unicode characters as if they were keystrokes. The VK_PACKET key is the low word of a 32-bit Virtual Key value used for non-keyboard input methods. For more information, see Remark in KEYBDINPUT, SendInput, WM_KEYDOWN, and WM_KEYUP
    { "PACKET", VK_PACKET }, // 0xE6
                             // 0xE8 Unassigned
                             // 0xE9 - 0xF5 OEM specific
    { "ATTN", VK_ATTN }, // 0xF6
    { "CRSEL", VK_CRSEL }, // 0xF7
    { "EXSEL", VK_EXSEL }, // 0xF8
    { "EREOF", VK_EREOF }, // 0xF9
    { "PLAY", VK_PLAY }, // 0xFA
    { "ZOOM", VK_ZOOM }, // 0xFB
    { "NONAME", VK_NONAME }, // 0xFC
    { "PA1", VK_PA1 }, // 0xFD
    { "OEM_CLEAR", VK_OEM_CLEAR }, // 0xFE
    { "FN", 0xFF }, // Most Laptops use this virtual keycode(0xFF) as Fn key.
};

const int VIRTUAL_KEYCODE_COUNT =
sizeof(VIRTUAL_KEYCODE_TABLE) / sizeof(VIRTUAL_KEYCODE_TABLE[0]);

int __stdcall GetVirtualKeyCode(const char* szKeyName)
{
    int i;
    int nCode;
    int nCount = VIRTUAL_KEYCODE_COUNT;

    // Special Key
    for (i = 0; i<nCount; i++)
    {
        if (_stricmp(szKeyName, VIRTUAL_KEYCODE_TABLE[i].text) == 0)
        {
            return VIRTUAL_KEYCODE_TABLE[i].value;
        }
    }

    nCount = strlen(szKeyName);
    if (nCount != 1)
    {
        return E_FAIL;
    }

    nCode = szKeyName[0];
    if (nCode >= 'a' && nCode <= 'z')
    {
        nCode -= 0x20;
    }

    return nCode;
}

const char* __stdcall GetVirtualKeyName(int nVirtualKeyCode)
{
    int i;
    int nCount = VIRTUAL_KEYCODE_COUNT;

    // Special Key
    for (i = 0; i<nCount; i++)
    {
        if (nVirtualKeyCode == VIRTUAL_KEYCODE_TABLE[i].value)
        {
            return VIRTUAL_KEYCODE_TABLE[i].text;
        }
    }

    return NULL;
}

GRC_NAMESPACE_BEGIN

#define GRC_STRERROR_GEN(name, msg) case GRC_ ## name: return msg;
const char* GetErrorDesc(int err) {
    switch (err) {
        GRC_ERRNO_MAP(GRC_STRERROR_GEN)
    }
    return "Unknown error description";
}
#undef GRC_STRERROR_GEN

#define GRC_ERR_NAME_GEN(name, _) case GRC_ ## name: return #name;
const char* GetErrorName(int err) {
    switch (err) {
        GRC_ERRNO_MAP(GRC_ERR_NAME_GEN)
    }
    return "Unknown error code";
}

#if 0
int __stdcall ParseLine(
    char* szLine, char* szSeparator, char** pszKey, char** pszValue)
{
    char* szText;

    szText = strchr(szLine, szSeparator[0]);
    if (szText == NULL)
    {
        return -1;
    }

    szText[0] = '\0';
    *pszKey = szLine;

    szText++;
    ::StrTrimA(szText, " \t");

    *pszValue = szText;

    return 0;
}
#endif

GRC_NAMESPACE_END