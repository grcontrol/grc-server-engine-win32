#include "SystemExecuter.h"
#include "SystemFunc.h"

SystemExecuter::SystemExecuter()
{
}

SystemExecuter::~SystemExecuter()
{
}

COMMAND_TYPE SystemExecuter::GetType()
{
    return eCT_SYSTEM;
}

int SystemExecuter::Run(const Json::Value & reqJson, Json::Value & resJson)
{
    LOGD_CALL(TAG, "start");

    int ret;
    int subType = reqJson.get("subType", Json::Value(-1)).asInt();

    switch (subType)
    {
    case eSCS_SHUTDOWN:
        ret = ShutdownSystem(false);
        break;

    case eSCS_REBOOT:
        ret = ShutdownSystem(true);
        break;

    case eSCS_LOGOFF:
        ret = LogOff();
        break;

    case eSCS_LOCK_SCREEN:
        ret = LockScreen();
        break;

    case eSCS_HIBERNATE:
        ret = SuspendSystem(true);
        break;

    case eSCS_SUSPEND:
        ret = SuspendSystem(false);
        break;

    case eSCS_MONITOR:
        ret = TurnOffMonitor(NULL);
        break;

    case eSCS_NON_SLEEP_MODE:
        const bool mode = reqJson["nonSleepingMode"].asBool();
        ret = SetNonSleepingMode(mode);
        break;

#if 0
    case eSCS_GET_VOLUME:
        break;
    case eSCS_SET_VOLUME:
        break;
#endif
    }

    LOGD(TAG, "%s(%d) %s: end ret(%d)", __CALL_INFO__, ret);

    return ret;
}
