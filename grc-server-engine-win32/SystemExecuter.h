#pragma once
#ifndef SYSTEM_EXECUTER_H
#define SYSTEM_EXECUTER_H

#include "Executer.h"

class SystemExecuter : public Executer
{
public:
    SystemExecuter();
    virtual ~SystemExecuter();

    // Inherited via Executer
    virtual COMMAND_TYPE GetType() override;
    virtual int Run(const Json::Value & reqJson, Json::Value & resJson) override;
};

#endif