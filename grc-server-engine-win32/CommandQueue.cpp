#include "CommandQueue.h"

CommandQueue::CommandQueue()
{
}

CommandQueue::~CommandQueue()
{
}

void CommandQueue::Push(Command* command)
{
	m_commands.push_back(command);
}

Command* CommandQueue::Pop()
{
	Command* command = NULL;

	COMMAND_LIST::iterator it = m_commands.begin();
	if (it != m_commands.end())
	{
		command = *it;
	}

	return command;
}