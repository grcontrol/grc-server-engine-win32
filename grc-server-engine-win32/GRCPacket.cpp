#include "GRCPacket.h"
#include <gwlog.h>

GRC_NAMESPACE_BEGIN

#if 0
/**
* Create a packet of which memory is consecutive for header and body.
* @param packetType ex) ePT_JSON
* @param data body data
* @param size body data size in byte
*/
static char* CreateGRCPacket(
    const PACKET_TYPE packetType, const char* data, const int size, int* packetSize)
{
    int totalSize = sizeof(GRCPacketHeader) + size;
    char* packet = (char*)malloc(totalSize);

    GRCPacketHeader* header = (GRCPacketHeader*)packet;
    header->type = htonl((uint32_t)packetType);
    header->bodySize = htonl(size);
    header->session = htonl(0);
    memcpy(packet + sizeof(GRCPacketHeader), data, size);

    if (packetSize)
    {
        *packetSize = totalSize;
    }

    return packet;
}
#endif

GRCPacket::GRCPacket() :
    m_data(NULL),
    m_userData(NULL)
{
    memset(&m_header, 0, sizeof(m_header));
}

GRCPacket::~GRCPacket()
{
    if (m_data)
    {
        free(m_data);
    }
}

/**
 * Create a packet of which memory is consecutive for header and body.
 * @param packetType ex) ePT_JSON
 * @param data body data
 * @param size body data size in byte
 */
GRCPacket* GRCPacket::Create(
    const PACKET_TYPE packetType, const char* body, const uint32_t bodySize)
{
    uint32_t packetSize = sizeof(GRCPacket::Header) + bodySize;
    char* packet = (char*)malloc(packetSize);

    // Convert header info in network order.
    GRCPacket::Header* header = (GRCPacket::Header*)packet;
    header->type = htonl((uint32_t)packetType);
    header->bodySize = htonl(bodySize);
    header->session = htonl(0);

    // copy body data into packet.
    memcpy(packet + sizeof(GRCPacket::Header), body, bodySize);

    return GRCPacket::Create(packet, packetSize);
}

GRCPacket* GRCPacket::Create(char* data, uint32_t size)
{
    GRCPacket* packet = new GRCPacket();

    int ret = packet->Set(data, size);
    if (ret != 0)
    {
        delete packet;
        return NULL;
    }

    return packet;
}

GRCPacket* GRCPacket::CreateJsonPacket(const string& json)
{
    return GRCPacket::Create(ePT_JSON, json.data(), json.size());
}

void GRCPacket::Delete(GRCPacket* packet)
{
    if (packet)
    {
        delete packet;
    }
}

/**
 * @param data: it is reused here, so it should be allocated from heap.
 * @param size: the byte size of data
 */
int GRCPacket::Set(char* data, uint32_t size)
{
    Header* header = (Header*)data;

    uint32_t bodySize = htonl(header->bodySize);
    if (size != sizeof(Header) + bodySize)
    {
        // Invalid packet data
        return -1;
    }

    m_header.type = htonl(header->type);
    m_header.bodySize = bodySize;
    m_header.session = htonl(header->session);

    m_data = data;

    return 0;
}

char* GRCPacket::GetBody(uint32_t* size) const
{
    if (size)
    {
        *size = m_header.bodySize;
    }

    return m_data + sizeof(GRCPacket::Header);
}

char* GRCPacket::GetData(uint32_t* size) const
{
    if (size)
    {
        *size = m_header.bodySize + sizeof(m_header);
    }

    return m_data;
}

GRC_NAMESPACE_END