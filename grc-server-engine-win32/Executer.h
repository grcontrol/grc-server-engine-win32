#pragma once
#ifndef COMMAND_EXECUTER_H
#define COMMAND_EXECUTER_H

#include "_grc.h"
#include <json/json.h>

class Command;

/**
 * 수신된 명령들을 실행하는 실행자 클래스 인터페이스
 */
class Executer
{
public:
	virtual COMMAND_TYPE GetType() = 0;
	virtual int Run(const Json::Value& reqJson, Json::Value& resJson) = 0;
};

#endif