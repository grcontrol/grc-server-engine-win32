#include "ServerExecuter.h"
#include <CharsetUtil.h>
#include <Windows.h>

GRC_NAMESPACE_BEGIN

ServerExecuter::ServerExecuter()
{
    m_macAddress = "aaaaaaaaaaaa";
}

ServerExecuter::~ServerExecuter()
{
}

COMMAND_TYPE ServerExecuter::GetType()
{
    return eCT_SERVER;
}

int ServerExecuter::Run(const Json::Value& reqJson, Json::Value& resJson)
{
    const uint32_t subType = reqJson["subType"].asUInt();
    switch (subType)
    {
    case eSCS_AUTHENTICATE:
        return Authenticate(reqJson, resJson);
    case eSCS_DETECTION:
        return Detect(reqJson, resJson);
    }

    return -1;
}

int ServerExecuter::Authenticate(
    const Json::Value& reqJson, Json::Value& resJson)
{
    const string checksum = reqJson["checksum"].asString();

    if (!CompareChecksum(checksum))
    {
        return GRC_AUTHENTICATION_ERROR;
    }

    resJson["session"] = (uint32_t)::GetTickCount();
    resJson["mac"] = m_macAddress;

    return 0;
}

bool ServerExecuter::CompareChecksum(const string& checksum) const
{
    return checksum == "d41d8cd98f00b204e9800998ecf8427e";
}

int ServerExecuter::Detect(
    const Json::Value& reqJson, Json::Value& resJson)
{
    TCHAR computerName[MAX_COMPUTERNAME_LENGTH + 1];
    DWORD size = MAX_COMPUTERNAME_LENGTH + 1;
    if (!::GetComputerName(computerName, &size))
    {
        _tcscmp(computerName, _T("GoldwormRC"));
        size = _tcslen(computerName);
    }

    resJson["name"] = _UTF8(computerName);
    resJson["isLocked"] = IsLocked();
    //resJson["grcVersion"] = GRC_VERSION;

    return 0;
}

GRC_NAMESPACE_END