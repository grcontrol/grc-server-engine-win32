#pragma once
#ifndef BINARY_BODY_CONVERTER_H
#define BINARY_BODY_CONVERTER_H

#include "_grc.h"
#include <json/json.h>

GRC_NAMESPACE_BEGIN

class Packet;

/**
 * Converter between binary body and json body.
 * To support old version protocol in binary format.
 */
class BinaryBodyConverter
{
public:
    BinaryBodyConverter();
    ~BinaryBodyConverter();

    static int BinToJson(Packet* packet, Json::Value& json);

private:
    static int HandleServerCommand(Packet* packet, Json::Value& json);
    static int HandleServerAuthenticateCommand(Packet* packet, Json::Value& json);
};

GRC_NAMESPACE_END

#endif