#include "WindowHandleFinder.h"
#include <PathUtil.h>
#include <VersionHelpers.h>

static BOOL GetExeFilePathFromProcessHandleOnXPOrOlder(
    HANDLE hProcess, DWORD dwFlags, TCHAR* lpExeName, PDWORD lpdwSize)
{
    BOOL bRet = FALSE;
    HMODULE hDLL = ::LoadLibrary(TEXT("Psapi.dll"));

    if (hDLL == NULL)
    {
        LOGE_CALL(TAG, "hDLL is null.");
        return FALSE;
    }

#ifdef _UNICODE
    LPCSTR lpProcName = "GetModuleFileNameExW";
#else
    LPCSTR lpProcName = "GetModuleFileNameExA";
#endif

    typedef DWORD(WINAPI* fpnGetModuleFileNameEx) (
        HANDLE hProcess, HMODULE hModule, LPTSTR lpFilename, DWORD nSize);

    fpnGetModuleFileNameEx _GetModuleFileNameEx =
        (fpnGetModuleFileNameEx) ::GetProcAddress(hDLL, lpProcName);

    if (_GetModuleFileNameEx)
    {
        // It doesn't work in Windows7 x64 and returns 0. error message is ERROR_INVALID_HANDLE(0x06).
        DWORD dwLength = _GetModuleFileNameEx(hProcess, NULL, lpExeName, *lpdwSize);
        bRet = (dwLength > 0) ? TRUE : FALSE;
    }

    ::FreeLibrary(hDLL);

    return bRet;
}

static BOOL GetExeFilePathFromProcessHandleOnNewerThanXP(
    HANDLE hProcess, DWORD dwFlags, TCHAR* lpExeName, PDWORD lpdwSize)
{
    BOOL bRet = FALSE;
    HMODULE hDLL = ::LoadLibrary(TEXT("Kernel32.dll"));

    if (hDLL == NULL)
    {
        LOGE_CALL(TAG, "hDLL is null.");
        return FALSE;
    }

#ifdef _UNICODE
    LPCSTR lpProcName = "QueryFullProcessImageNameW";
#else
    LPCSTR lpProcName = "QueryFullProcessImageNameA";
#endif

    typedef BOOL(WINAPI* fpnQueryFullProcessImageName) (
        HANDLE hProcess, DWORD dwFlags, LPTSTR lpExeName, PDWORD lpdwSize);

    fpnQueryFullProcessImageName _QueryFullProcessImageName =
        (fpnQueryFullProcessImageName) ::GetProcAddress(hDLL, lpProcName);

    if (_QueryFullProcessImageName)
    {
        bRet = _QueryFullProcessImageName(hProcess, 0, lpExeName, lpdwSize);
    }

    ::FreeLibrary(hDLL);

    return bRet;
}

static BOOL GetExeFilePathFromProcessHandle(HANDLE hProcess, DWORD dwFlags, TCHAR* lpExeName, PDWORD lpdwSize)
{
    BOOL bRet = FALSE;

    // If OS is newer than WindowsXP
    if (IsWindowsVistaOrGreater())
    {
        bRet = GetExeFilePathFromProcessHandleOnNewerThanXP(
            hProcess, dwFlags, lpExeName, lpdwSize);
    }
    else
    {
        bRet = GetExeFilePathFromProcessHandleOnXPOrOlder(
            hProcess, dwFlags, lpExeName, lpdwSize);
    }

    return bRet;
}

static BOOL WINAPI IsWindowRelatedToPathName(HWND hWnd, const TCHAR* szPathName)
{
    HANDLE hProcess;
    DWORD dwProcessID;
    DWORD nSize = MAX_PATH;
    TCHAR szBuf[MAX_PATH];
    TCHAR szLongPathName[MAX_PATH];
    DWORD dwLength;

    if (!IsWindow(hWnd))
    {
        return FALSE;
    }

    // Check PATH_NAME
    GetWindowThreadProcessId(hWnd, &dwProcessID);

    hProcess = OpenProcess(
        PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, TRUE, dwProcessID
    );

#if 0

    // "\Device\HarddiskVolume1\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe"
    nSize = GetProcessImageFileName(hProcess, szBuf, nSize);
    if (nSize < 1)
    {
        return FALSE;
    }

#elif 0

    // It doesn't work in Windows7 x64 and returns 0. error message is ERROR_INVALID_HANDLE(0x06).
    dwLength = GetModuleFileNameEx(hProcess, NULL, szBuf, nSize);
    if (dwLength < 1)
    {
        DWORD dwError = GetLastError();
        return FALSE;
    }

#elif 0

    BOOL bRet = QueryFullProcessImageName(hProcess, 0, szBuf, &nSize);
    if (!bRet)
    {
        return FALSE;
    }

#else

    BOOL bRet = GetExeFilePathFromProcessHandle(hProcess, 0, szBuf, &nSize);
    if (!bRet)
    {
        return FALSE;
    }

#endif

    CloseHandle(hProcess);

    dwLength = GetLongPathName(szBuf, szLongPathName, MAX_PATH);
    if (dwLength < 1)
    {
        return FALSE;
    }

#if 0
    if (_tcsicmp(szPathName, szLongPathName) != 0)
    {
        return FALSE;
    }
#else
    const TCHAR chPathDelimiter = L'\\';
    std::wstring fileName = gw::ExtractFileNameT(szLongPathName, chPathDelimiter);
    std::wstring fileNameInFile = gw::ExtractFileNameT(szPathName, chPathDelimiter);

    if (fileName != fileNameInFile)
    {
        return FALSE;
    }
#endif

    return TRUE;
}


// WindowHandleFinder Implementation ==============================
WindowHandleFinder::WindowHandleFinder(void)
{
	Clear();
}

WindowHandleFinder::~WindowHandleFinder(void)
{
}

HRESULT WindowHandleFinder::EnumWindowProc(HWND hWnd)
{
	LOGD(TAG, "%s(%d) %s: WindowHandle: %p", __CALL_INFO__, hWnd);

	BOOL bFound = Check(&hWnd);
	if(bFound)
	{
		// Stop to find.
		m_hWnd = hWnd;
		SetLastError(ERROR_SUCCESS);

		m_hWndList.push_back(hWnd);
	}

	return TRUE;
}

BOOL WindowHandleFinder::Check(HWND* phWnd)
{
	HWND hWnd = *phWnd;

	int nSize = MAX_PATH;

	const TCHAR* szPathName = m_szPathName;
	const TCHAR* szClassName = m_szClassName;

#if 1
	TCHAR szBuf[MAX_PATH];

	if(szClassName != NULL && _tcslen(szClassName) > 0)
	{
		// Check CLASS_NAME
		::GetClassName(hWnd, szBuf, nSize);
		if(_tcsstr(szBuf, szClassName) == NULL)
		{
			return FALSE;
		}
	}
#endif

	WINDOWINFO windowInfo;
	BOOL bRet = ::GetWindowInfo(hWnd, &windowInfo);
	if(bRet == FALSE)
	{
		return FALSE;
	}

	if(m_dwWindowStyle[eWSM_INCLUSIVE] || m_dwWindowStyle[eWSM_EXCLUSIVE])
	{
		const DWORD dwStyle =
			m_dwWindowStyle[eWSM_INCLUSIVE] | m_dwWindowStyle[eWSM_EXCLUSIVE];

		// Check Window's Style.
		if( (windowInfo.dwStyle & dwStyle) != m_dwWindowStyle[eWSM_INCLUSIVE] )
		{
			return FALSE;
		}

		// Check Window's Extension Style.
		const DWORD dwExStyle =
			m_dwWindowExStyle[eWSM_INCLUSIVE] | m_dwWindowExStyle[eWSM_EXCLUSIVE];

		if( (windowInfo.dwExStyle & dwExStyle) != m_dwWindowExStyle[eWSM_INCLUSIVE] )
		{
			return FALSE;
		}
	}

	BOOL bValue = IsWindowRelatedToPathName(hWnd, szPathName);
	if( !bValue )
	{
		return FALSE;
	}

#if 0
	if(szClassName == NULL)
	{
		// Get the Owner window's handle.
		hWnd = ::GetAncestor(hWnd, GA_ROOTOWNER);
		if(hWnd == NULL)
		{
			return FALSE;
		}
	}
#endif

	*phWnd = hWnd;

	return TRUE;
}

void WindowHandleFinder::SetPathName(const TCHAR* szPathName)
{
	m_szPathName = szPathName;
}

void WindowHandleFinder::SetClassName(const TCHAR* szClassName)
{
	m_szClassName = szClassName;
}

void WindowHandleFinder::SetWindowStyle(const DWORD dwStyle, const int nMode)
{
	m_dwWindowStyle[nMode] = dwStyle;
}

void WindowHandleFinder::SetWindowExStyle(const DWORD dwExStyle, const int nMode)
{
	m_dwWindowExStyle[nMode] = dwExStyle;
}

HRESULT WindowHandleFinder::Run(HWND_LIST& hWndList)
{
	if(m_szPathName == NULL || _tcslen(m_szPathName) == 0)
	{
		return E_FAIL;
	}

	m_hWnd = NULL;
	hWndList.clear();

	BOOL bFail = EnumWindows(WindowHandleFinder::EnumWindowsFunc, (LPARAM) this);

	hWndList = m_hWndList;

	return (hWndList.size() > 0) ? S_OK : E_FAIL;
}

BOOL CALLBACK WindowHandleFinder::EnumWindowsFunc(HWND hWnd, LPARAM lParam)
{
    WindowHandleFinder* pFinder = (WindowHandleFinder*)lParam;
    return pFinder->EnumWindowProc(hWnd);
}

void WindowHandleFinder::Clear()
{
	m_szPathName = NULL;
	m_szClassName = NULL;
	memset(m_dwWindowStyle, 0, sizeof(m_dwWindowStyle));
	memset(m_dwWindowExStyle, 0, sizeof(m_dwWindowExStyle));
	m_hWndList.clear();
}