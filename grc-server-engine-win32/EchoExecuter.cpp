#include "EchoExecuter.h"

EchoExecuter::EchoExecuter()
{
}

EchoExecuter::~EchoExecuter()
{
}

COMMAND_TYPE EchoExecuter::GetType()
{
    return eCT_ECHO;
}

int EchoExecuter::Run(const Json::Value& reqJson, Json::Value& resJson)
{
    LOGD_CALL(TAG, "start");

    resJson = reqJson;

    LOGD_CALL(TAG, "end");

    return GRC_OK;
}
