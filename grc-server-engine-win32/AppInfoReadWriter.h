#pragma once
#ifndef APP_INFO_READ_WRITER_H
#define APP_INFO_READ_WRITER_H

#include "_grc.h"
#include <string>

using namespace std;

GRC_NAMESPACE_BEGIN

class AppInfo;

class AppInfoReadWriter
{
public:
    AppInfoReadWriter();
    virtual ~AppInfoReadWriter();

    virtual AppInfo* Load(const wstring& path) = 0;
    virtual int Save(const wstring& path) = 0;

private:

};

GRC_NAMESPACE_END

#endif