#pragma once
#ifndef _GRC_H
#define _GRC_H

#include "uv_func.h"
#include <stdint.h>
#include <memory>
#include <gwlog.h>
#include "grc-errno.h"

#define WIN32_LEAN_AND_MEAN

#if 0
#define GRC_NAMESPACE_NAME grc
#define GRC_NAMESPACE_BEGIN namespace GRC_NAMESPACE_NAME {
#define GRC_NAMESPACE_END }
#define GRC_NAMESPACE_USE using namespace GRC_NAMESPACE_NAME;
#define USE_GRC_NAMESPACE using namespace GRC_NAMESPACE_NAME;
#else
#define GRC_NAMESPACE_NAME
#define GRC_NAMESPACE_BEGIN
#define GRC_NAMESPACE_END
#define GRC_NAMESPACE_USE
#define USE_GRC_NAMESPACE
#endif

extern const char* TAG;

/**
* Maximum number of keys that a shortcut can have.
*/
#define MAX_SHORTCUT_KEY_COUNT 256

/*
* Files which is associated with a new shortcut key addition.
* stdafx.h, stdafx.cpp, AppInfo.cpp, ShortcutHandler.cpp
*/

/**
* MUST NOT change the enumeration's order.
*/
enum REMOTE_CONTROL
{
    eRC_UNKNOWN = -1,
    eRC_JUMP_BACKWARD = 0,
    eRC_JUMP_FORWARD = 1,
    eRC_PREV = 2,
    eRC_NEXT = 3,
    eRC_PLAY = 4,
    eRC_PAUSE = 5,
    eRC_STOP = 6,
    eRC_FULL_SCREEN = 7,
    eRC_DOWN = 8,
    eRC_UP = 9,
    eRC_MUTE = 10,
    eRC_SUBTITLE = 11,
    eRC_LEFT = 12,
    eRC_RIGHT = 13,
    eRC_CLOVER = 14,
    eRC_HEART = 15,
    eRC_DIAMOND = 16,
    eRC_SPADE = 17,
    // subtitle
    eRC_SUBTITLE_BACKWARD = 18,
    eRC_SUBTITLE_RESET = 19,
    eRC_SUBTITLE_FORWARD = 20,
    // audio
    eRC_AUDIO_BACKWARD = 21,
    eRC_AUDIO_RESET = 22,
    eRC_AUDIO_CHANGE = 23,
    eRC_AUDIO_FORWARD = 24,
    // playstation
    eRC_CIRCLE = 25,
    eRC_CROSS = 26,
    eRC_RECTANGLE = 27,
    eRC_TRIANGLE = 28,

    eRC_POWER = 0x0100,
    eRC_FOCUS = 0x0200,
    eRC_SHUTDOWN = 0x0300,
    eRC_RESTART = 0x0400,
    eRC_PING = 0x0500,
    eRC_VOLUME_UP = 0x0600,
    eRC_VOLUME_DOWN = 0x0700,
    eRC_HIDE = 0x0800,

    eRC_SHOW_HIDE = 0x1000,
};

enum COMMAND_TYPE
{
	eCT_NONE = -1,

	eCT_GET_APP_LIST = 0,
	eCT_SET_APP = 1,
	eCT_SHORTCUT = 2,
	eCT_SYSTEM = 3,
	eCT_SERVER = 4,
	eCT_MOUSE = 5,
	eCT_KEYBOARD = 6,
	eCT_TEXT = 7,
	eCT_FILE_BROWSE = 8,

    eCT_LIFECYCLE= 100,

    // Just for test
    eCT_ECHO = 1000,
};

enum SERVER_COMMAND_SUBTYPE
{
	eSCS_AUTHENTICATE = 0,
	eSCS_DETECTION = 1,
};

enum SYSTEM_COMMAND_SUBTYPE
{
	eSCS_SHUTDOWN,
	eSCS_REBOOT,
	eSCS_LOGOFF,
	eSCS_HIBERNATE,
	eSCS_LOCK_SCREEN,
	eSCS_GET_VOLUME,
	eSCS_MONITOR,
	eSCS_SET_VOLUME,
	eSCS_SUSPEND,
	eSCS_NON_SLEEP_MODE,
};

enum FILE_BROWSE_SUBTYPE
{
	eFBS_CD,
	eFBS_DELETE,
	eFBS_LIST,
	eFBS_CWD,
	eFBS_RENAME,
};

enum PACKET_TYPE
{
    ePT_NONE = -1,
    // body in json format
    ePT_JSON = 100,
};

enum FILE_SYSTEM_TYPE
{
    eFST_DIRECTORY,
    eFST_DRIVE,
    eFST_FILE,
};


enum CONFIG_KEY
{
    eAPP_NAME,
    eCLASS_NAME,
    ePATH_NAME,
    eJUMP_BACKWARD,
    eJUMP_FORWARD,
    ePREV,
    eNEXT,
    ePLAY,
    ePAUSE,
    eSTOP,
    eFULL_SCREEN,
    eDOWN,
    eUP,
    eMUTE,
    eSUBTITLE,
    eLEFT,
    eRIGHT,
    eCLOVER,
    eHEART,
    eDIAMOND,
    eSPADE,
    // subtitle
    eSUBTITLE_BACKWARD,
    eSUBTITLE_RESET,
    eSUBTITLE_FORWARD,
    // audio
    eAUDIO_BACKWARD,
    eAUDIO_RESET,
    eAUDIO_CHANGE,
    eAUDIO_FORWARD,
    // shape
    eCIRCLE,
    eCROSS,
    eRECTANGLE,
    eTRIANGLE,
};

extern const int CONFIG_KEY_COMMAND_TABLE[];
extern const char* SZ_CONFIG_KEY_NAME[];
extern const TCHAR* WSZ_CONFIG_KEY_NAME[];
extern const int CONFIG_KEY_COUNT;

int WINAPI GetVirtualKeyCode(const char* szKeyName);
const char* WINAPI GetVirtualKeyName(int nVirtualKeyCode);

GRC_NAMESPACE_BEGIN
const char* GetErrorDesc(int err);
const char* GetErrorName(int err);
#if 0
char* CreateGRCPacket(
    const PACKET_TYPE packetType, const char* data, const int size,
    int* packetSize=NULL);
#endif
GRC_NAMESPACE_END

#endif
