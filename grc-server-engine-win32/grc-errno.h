#pragma once
#ifndef GRC_ERRNO_H
#define GRC_ERRNO_H

enum GRC_ERROR_CODE
{
    GRC_OK = 0,

    GRC_ERR = -1,
    GRC_INVALID_PACKET = -2,
    GRC_INVALID_JSON = -3,
    GRC_UNKNOWN_COMMAND_TYPE = -4,
    GRC_NOT_IMPLEMENTED = -5,
    GRC_AUTHENTICATION_ERROR = -6,
};

#define GRC_ERRNO_MAP(XX)                                                   \
    XX(OK, "ok")                                                            \
    XX(ERR, "error")                                                        \
    XX(INVALID_PACKET, "Invalid packet")                                    \
    XX(INVALID_JSON, "Invalid json")                                        \
    XX(UNKNOWN_COMMAND_TYPE, "Unknown command type")                        \
    XX(NOT_IMPLEMENTED, "Not implemented")                                  \
    XX(AUTHENTICATION_ERROR, "Authentication error")

#endif