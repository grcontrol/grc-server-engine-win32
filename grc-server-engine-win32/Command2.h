#pragma once
#ifndef COMMAND_H
#define COMMAND_H

#include <string>

/**
 * 수행할 명령을 나타내는 추상화 클래스
 */
class Command
{
public:
	int GetType() const { return m_type; }
	int GetSubType() const { return m_subType; }

	/**
	 * returns string property value indicated by key.
	 * @param key key
	 * @return value
	 */
	virtual std::string getString(const std::string& key) = 0;

	/**
	* returns integer property value indicated by key.
	* @param key key
	* @return value
	*/
	virtual int getInt(const std::string& key) = 0;

private:
	Command() : m_type(-1), m_subType(-1) {}
	virtual ~Command() {}

protected:
	/**
	 * Refer to COMMAND_TYPE enumeration in _grc.h.
	 */
	int m_type;

	/**
	* Refer to COMMAND_SUBTYPE enumerations in _grc.h.
	*/
	int m_subType;
};

#endif